﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 1:54 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.TextBoxIn
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class TBI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.OUT = new System.HMI.Symbols.TextField<string>();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.group1 = new NxtControl.GuiFramework.Group();
			// 
			// OUT
			// 
			this.OUT.BeginInit();
			this.OUT.AngleIgnore = false;
			this.OUT.BackColor = System.Drawing.SystemColors.Window;
			this.OUT.DecimalPlacesCount = ((uint)(2u));
			this.OUT.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 245, 167);
			this.OUT.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.OUT.ForeColor = System.Drawing.SystemColors.WindowText;
			this.OUT.Name = "OUT";
			this.OUT.TagName = "OUT";
			this.OUT.Value = null;
			this.OUT.EndInit();
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(203)), ((float)(124)), ((float)(171)), ((float)(106)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalCenter, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(150)), ((byte)(214)), ((byte)(66))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(150)), ((byte)(214)), ((byte)(66)))}, new float[] {
															0F,
															0.32F,
															1F})));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Radius = 20;
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(236, 132);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Send message";
			// 
			// group1
			// 
			this.group1.BeginInit();
			this.group1.Name = "group1";
			this.group1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.OUT,
									this.roundedRectangle1,
									this.freeText1});
			this.group1.EndInit();
			// 
			// TBI
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.group1});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Group group1;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private System.HMI.Symbols.TextField<string> OUT;
		#endregion
	}
}
