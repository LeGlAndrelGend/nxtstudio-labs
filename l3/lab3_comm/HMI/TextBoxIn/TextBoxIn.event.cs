/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 1:54 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #TextBoxIn_HMI;

namespace HMI.Main.Symbols.TextBoxIn
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.String OUT_field = null;
    public System.String OUT
    {
       get { return OUT_field; }
       set { OUT_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.TextBoxIn
{
  partial class TBI
  {
    public bool FireEvent_CNF(System.String OUT)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {OUT});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.TextBoxIn.CNFEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.OUT != null) _values_[0] = ea.OUT;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.String OUT, bool ignore_OUT)
    {
      object[] _values_ = new object[1];
      if (!ignore_OUT) _values_[0] = OUT;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #TextBoxIn_HMI;

#endregion Definitions;
