﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 2:18 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.TextBoxOutWString
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class TBOWStr
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textbox = new System.HMI.Symbols.TextField<bool>();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.group1 = new NxtControl.GuiFramework.Group();
			// 
			// textbox
			// 
			this.textbox.BeginInit();
			this.textbox.AngleIgnore = false;
			this.textbox.BackColor = System.Drawing.SystemColors.Control;
			this.textbox.DecimalPlacesCount = ((uint)(2u));
			this.textbox.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 211, 166);
			this.textbox.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.textbox.ForeColor = System.Drawing.SystemColors.WindowText;
			this.textbox.IsOnlyInput = true;
			this.textbox.Name = "textbox";
			this.textbox.TagName = "textbox";
			this.textbox.Value = false;
			this.textbox.EndInit();
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(186)), ((float)(134)), ((float)(149)), ((float)(87)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalCenter, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(186)), ((byte)(186)), ((byte)(186))),
															new NxtControl.Drawing.Color(((byte)(254)), ((byte)(186)), ((byte)(10))),
															new NxtControl.Drawing.Color(((byte)(186)), ((byte)(186)), ((byte)(186)))}, new float[] {
															0F,
															0.27F,
															1F})));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Radius = 20;
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(230, 143);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Status";
			// 
			// group1
			// 
			this.group1.BeginInit();
			this.group1.Name = "group1";
			this.group1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.textbox,
									this.roundedRectangle1,
									this.freeText1});
			this.group1.EndInit();
			// 
			// TBOWStr
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.group1});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Group group1;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private System.HMI.Symbols.TextField<bool> textbox;
		#endregion
	}
}
