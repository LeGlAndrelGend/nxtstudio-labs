﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 2:05 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.TextBoxOut
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class TBO
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textbox = new System.HMI.Symbols.TextField<string>();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.group1 = new NxtControl.GuiFramework.Group();
			// 
			// textbox
			// 
			this.textbox.BeginInit();
			this.textbox.AngleIgnore = false;
			this.textbox.BackColor = System.Drawing.SystemColors.Control;
			this.textbox.DecimalPlacesCount = ((uint)(2u));
			this.textbox.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 217, 149);
			this.textbox.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.textbox.ForeColor = System.Drawing.SystemColors.WindowText;
			this.textbox.IsOnlyInput = true;
			this.textbox.Name = "textbox";
			this.textbox.TagName = "textbox";
			this.textbox.Value = null;
			this.textbox.EndInit();
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(170)), ((float)(105)), ((float)(193)), ((float)(111)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.InvertedVerticalCenter, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(194)), ((byte)(206)), ((byte)(218))),
															new NxtControl.Drawing.Color(((byte)(186)), ((byte)(186)), ((byte)(186)))}, new float[] {
															0F,
															0.77F,
															1F})));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Radius = 20;
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color("White");
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(199, 112);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Recieved message";
			// 
			// group1
			// 
			this.group1.BeginInit();
			this.group1.Name = "group1";
			this.group1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.textbox,
									this.roundedRectangle1,
									this.freeText1});
			this.group1.EndInit();
			// 
			// TBO
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.group1});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Group group1;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private System.HMI.Symbols.TextField<string> textbox;
		#endregion
	}
}
