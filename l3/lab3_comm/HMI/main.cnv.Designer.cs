﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 2:23 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for main.
	/// </summary>
	partial class main
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbi1 = new HMI.Main.Symbols.TextBoxIn.TBI();
			this.tbo1 = new HMI.Main.Symbols.TextBoxOut.TBO();
			this.tbo2 = new HMI.Main.Symbols.TextBoxOut.TBO();
			this.tbo3 = new HMI.Main.Symbols.TextBoxOut.TBO();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.roundedRectangle2 = new NxtControl.GuiFramework.RoundedRectangle();
			this.roundedRectangle3 = new NxtControl.GuiFramework.RoundedRectangle();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.freeText2 = new NxtControl.GuiFramework.FreeText();
			this.freeText3 = new NxtControl.GuiFramework.FreeText();
			this.freeText4 = new NxtControl.GuiFramework.FreeText();
			this.freeText5 = new NxtControl.GuiFramework.FreeText();
			this.freeText6 = new NxtControl.GuiFramework.FreeText();
			this.TBOStat = new HMI.Main.Symbols.TextBoxOutWString.TBOWStr();
			// 
			// tbi1
			// 
			this.tbi1.BeginInit();
			this.tbi1.AngleIgnore = false;
			this.tbi1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 78, 60);
			this.tbi1.Name = "tbi1";
			this.tbi1.SecurityToken = ((uint)(4294967295u));
			this.tbi1.TagName = "CD3D01E3F99FF1A0";
			this.tbi1.EndInit();
			// 
			// tbo1
			// 
			this.tbo1.BeginInit();
			this.tbo1.AngleIgnore = false;
			this.tbo1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 424, 275);
			this.tbo1.Name = "tbo1";
			this.tbo1.SecurityToken = ((uint)(4294967295u));
			this.tbo1.TagName = "76394882C298E9DA";
			this.tbo1.EndInit();
			// 
			// tbo2
			// 
			this.tbo2.BeginInit();
			this.tbo2.AngleIgnore = false;
			this.tbo2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 106, 278);
			this.tbo2.Name = "tbo2";
			this.tbo2.SecurityToken = ((uint)(4294967295u));
			this.tbo2.TagName = "161F89A8ED4748DA";
			this.tbo2.EndInit();
			// 
			// tbo3
			// 
			this.tbo3.BeginInit();
			this.tbo3.AngleIgnore = false;
			this.tbo3.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 446, 58);
			this.tbo3.Name = "tbo3";
			this.tbo3.SecurityToken = ((uint)(4294967295u));
			this.tbo3.TagName = "1CC8AE7DC5F12329";
			this.tbo3.EndInit();
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(53)), ((float)(37)), ((float)(615)), ((float)(157)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(194)), ((byte)(206)), ((byte)(218))));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Radius = 20;
			// 
			// roundedRectangle2
			// 
			this.roundedRectangle2.Bounds = new NxtControl.Drawing.RectF(((float)(56)), ((float)(242)), ((float)(292)), ((float)(179)));
			this.roundedRectangle2.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(238)), ((byte)(206)), ((byte)(206))));
			this.roundedRectangle2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle2.Name = "roundedRectangle2";
			this.roundedRectangle2.Radius = 20;
			// 
			// roundedRectangle3
			// 
			this.roundedRectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(369)), ((float)(239)), ((float)(296)), ((float)(185)));
			this.roundedRectangle3.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(223)), ((byte)(216)), ((byte)(231))));
			this.roundedRectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle3.Name = "roundedRectangle3";
			this.roundedRectangle3.Radius = 20;
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(60, 41);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Device0";
			// 
			// freeText2
			// 
			this.freeText2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText2.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText2.Location = new NxtControl.Drawing.PointF(63, 249);
			this.freeText2.Name = "freeText2";
			this.freeText2.Text = "Device1";
			// 
			// freeText3
			// 
			this.freeText3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText3.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText3.Location = new NxtControl.Drawing.PointF(380, 244);
			this.freeText3.Name = "freeText3";
			this.freeText3.Text = "Device2";
			// 
			// freeText4
			// 
			this.freeText4.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText4.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText4.Location = new NxtControl.Drawing.PointF(502, 41);
			this.freeText4.Name = "freeText4";
			this.freeText4.Text = "Subscriber3";
			// 
			// freeText5
			// 
			this.freeText5.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText5.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText5.Location = new NxtControl.Drawing.PointF(479, 258);
			this.freeText5.Name = "freeText5";
			this.freeText5.Text = "Subscriber2";
			// 
			// freeText6
			// 
			this.freeText6.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText6.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.freeText6.Location = new NxtControl.Drawing.PointF(157, 259);
			this.freeText6.Name = "freeText6";
			this.freeText6.Text = "Subscriber1";
			// 
			// TBOStat
			// 
			this.TBOStat.BeginInit();
			this.TBOStat.AngleIgnore = false;
			this.TBOStat.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 268, 67);
			this.TBOStat.Name = "TBOStat";
			this.TBOStat.SecurityToken = ((uint)(4294967295u));
			this.TBOStat.TagName = "5DEB712AB9D05272";
			this.TBOStat.EndInit();
			// 
			// main
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1280)), ((float)(900)));
			this.Name = "main";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.roundedRectangle3,
									this.roundedRectangle2,
									this.roundedRectangle1,
									this.tbi1,
									this.tbo1,
									this.tbo2,
									this.tbo3,
									this.freeText1,
									this.freeText2,
									this.freeText3,
									this.freeText4,
									this.freeText5,
									this.freeText6,
									this.TBOStat});
			this.Size = new System.Drawing.Size(1280, 900);
		}
		private HMI.Main.Symbols.TextBoxOutWString.TBOWStr TBOStat;
		private NxtControl.GuiFramework.FreeText freeText6;
		private NxtControl.GuiFramework.FreeText freeText5;
		private NxtControl.GuiFramework.FreeText freeText4;
		private NxtControl.GuiFramework.FreeText freeText3;
		private NxtControl.GuiFramework.FreeText freeText2;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle3;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle2;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private HMI.Main.Symbols.TextBoxOut.TBO tbo3;
		private HMI.Main.Symbols.TextBoxOut.TBO tbo2;
		private HMI.Main.Symbols.TextBoxOut.TBO tbo1;
		private HMI.Main.Symbols.TextBoxIn.TBI tbi1;
		#endregion
	}
}
