/*
 * Created by nxtStudio.
 * User: gzha046
 * Date: 24/01/2011
 * Time: 3:23 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;


namespace NxtStudio.Symbols.UDPSocketServer
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			
	    this.CNF_Fired += new EventHandler<NxtStudio.Symbols.UDPSocketServer.CNFEventArgs>(OnCNF);
	    this.STOPREC_Fired += new EventHandler<NxtStudio.Symbols.UDPSocketServer.STOPRECEventArgs>(OnSTOPREC);
			//
		}
	
		
	  public class StateObject
    {
        // Size of receive buffer.
        public const int BufferSize = 8; //1024;
        // Receive buffer.
        public byte[] receivedData = new byte[8];
        // Received data
        public double receivedValue = 0.0;
        
    }
	  
    public class AsynchronousSocketListener 
    {
        
        private  Socket serverListener; 
        private  StateObject data = new StateObject(); //static
        private  NxtStudio.Symbols.UDPSocketServer.HMI inHMI;//static
           
        public AsynchronousSocketListener()
        {
          
        }

        public void StartListening(string Ipaddress, string portNumber,NxtStudio.Symbols.UDPSocketServer.HMI newHMI)
        {
            // Establish the local endpoint for the socket.
           
           inHMI =newHMI;
           IPAddress ipAddress;
          
           IPAddress.TryParse(Ipaddress,out ipAddress);
           IPEndPoint localEndPoint = new IPEndPoint(ipAddress, int.Parse(portNumber));//11000 is the port IPAddress.Any
           EndPoint epSender = (EndPoint) localEndPoint; 
            // Create a UDP / TCP/IP socket.
            serverListener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); // for UDP - SocketType.Dgram, ProtocolType.Udp
            

            // Bind the socket to the local endpoint and listen for incoming connections.
           try
            {  
              serverListener.Bind(localEndPoint);
              serverListener.BeginReceiveFrom(data.receivedData, 0, data.receivedData.Length, SocketFlags.None, ref epSender, new AsyncCallback(ReadCallback), epSender);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());   
                MessageBox.Show(e.Message.ToString() + " Start listening");
            }

        }
              private void ReadCallback(IAsyncResult ar) //static
        {
            IPEndPoint ipeSender = new IPEndPoint(IPAddress.Any,0);
            EndPoint epSender = (EndPoint)ipeSender;

            // Read data from the client socket.
            this.serverListener.EndReceiveFrom(ar, ref epSender);
            
            
            data.receivedValue = BitConverter.ToDouble(data.receivedData, 0);
            inHMI.FireEvent_REQ(float.Parse(data.receivedValue.ToString()));
            this.serverListener.BeginReceiveFrom(data.receivedData, 0, data.receivedData.Length, SocketFlags.None, ref epSender, new AsyncCallback(ReadCallback), epSender);
            
            
        }    
        public void CloseSocket(){
          this.serverListener.Close(1);
        }
    }
		
		protected void OnCNF(object sender, NxtStudio.Symbols.UDPSocketServer.CNFEventArgs e)
    {
        string IP = "";
        short port  = 0;
        AsynchronousSocketListener mySocket = new AsynchronousSocketListener();
        try 
        {
          if (e.Get_PORT(ref port) != false) 
            if (e.Get_IPAddress(ref IP)!= false)
              { 
                
                mySocket.StartListening(IP,port.ToString(),this);
                                
              } else
              {
                MessageBox.Show("Port is empty string");
              }
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message.ToString() + " OnCNF function");
        }
    
     }
	  
		protected void OnSTOPREC(object sender, NxtStudio.Symbols.UDPSocketServer.STOPRECEventArgs ex){
		  
		  //close socket
		  
		  
		}
		

		

	}
}
