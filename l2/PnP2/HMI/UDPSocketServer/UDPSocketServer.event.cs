/*
 * Created by nxtStudio.
 * User: gzha046
 * Date: 24/01/2011
 * Time: 3:23 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #UDPSocketServer_HMI;

namespace NxtStudio.Symbols.UDPSocketServer
{

  public class CNFEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public CNFEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_IPAddress(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String IPAddress
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_PORT(ref System.Int16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.Int16) var;
      return ret;
    }

    public System.Int16? PORT
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.Int16) var;
    }  }


  }

  public class STOPRECEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public STOPRECEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

}

namespace NxtStudio.Symbols.UDPSocketServer
{

  public class REQEventArgs : System.EventArgs
  {
    public REQEventArgs()
    {
    }
    private System.Single? DataReceived_field = null;
    public System.Single? DataReceived
    {
       get { return DataReceived_field; }
       set { DataReceived_field = value; }
    }

  }

}

namespace NxtStudio.Symbols.UDPSocketServer
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.UDPSocketServer.CNFEventArgs> CNF_Fired;

    private event EventHandler<NxtStudio.Symbols.UDPSocketServer.STOPRECEventArgs> STOPREC_Fired;

    protected override void OnEndInit()
    {
      if (CNF_Fired != null)
        AttachEventInput(0);
      if (STOPREC_Fired != null)
        AttachEventInput(1);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (CNF_Fired != null)
            CNF_Fired(this, new NxtStudio.Symbols.UDPSocketServer.CNFEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (STOPREC_Fired != null)
            STOPREC_Fired(this, new NxtStudio.Symbols.UDPSocketServer.STOPRECEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_REQ(System.Single DataReceived)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {DataReceived});
    }
    public bool FireEvent_REQ(NxtStudio.Symbols.UDPSocketServer.REQEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.DataReceived.HasValue) _values_[0] = ea.DataReceived.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_REQ(System.Single DataReceived, bool ignore_DataReceived)
    {
      object[] _values_ = new object[1];
      if (!ignore_DataReceived) _values_[0] = DataReceived;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #UDPSocketServer_HMI;

#endregion Definitions;
