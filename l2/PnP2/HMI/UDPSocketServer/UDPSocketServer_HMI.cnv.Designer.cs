/*
 * Created by nxtStudio.
 * User: gzha046
 * Date: 24/01/2011
 * Time: 3:23 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.UDPSocketServer
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.freeText2 = new NxtControl.GuiFramework.FreeText();
			this.freeText3 = new NxtControl.GuiFramework.FreeText();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.freeText4 = new NxtControl.GuiFramework.FreeText();
			this.textField_11 = new NxtStudio.Symbols.TextField<string>();
			this.textField_12 = new NxtStudio.Symbols.TextField<short>();
			this.textField_13 = new NxtStudio.Symbols.TextField<float>();
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(9, 29);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "IPAddress:";
			this.freeText1.Visible = false;
			// 
			// freeText2
			// 
			this.freeText2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.freeText2.Location = new NxtControl.Drawing.PointF(32, 52);
			this.freeText2.Name = "freeText2";
			this.freeText2.Text = "PORT:";
			this.freeText2.Visible = false;
			// 
			// freeText3
			// 
			this.freeText3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.freeText3.Location = new NxtControl.Drawing.PointF(3, 72);
			this.freeText3.Name = "freeText3";
			this.freeText3.Text = "Data received:";
			this.freeText3.Visible = false;
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(-1)), ((float)(2)), ((float)(170)), ((float)(110)));
			this.rectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(210)), ((byte)(210)), ((byte)(210))));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			this.rectangle1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.rectangle1.TextColor = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.rectangle1.Visible = false;
			// 
			// freeText4
			// 
			this.freeText4.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText4.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.freeText4.Location = new NxtControl.Drawing.PointF(5, 7);
			this.freeText4.Name = "freeText4";
			this.freeText4.Text = "UDP Receive";
			this.freeText4.Visible = false;
			// 
			// textField_11
			// 
			this.textField_11.BeginInit();
			this.textField_11.AngleIgnore = false;
			this.textField_11.BackColor = System.Drawing.SystemColors.Control;
			this.textField_11.DecimalPlacesCount = ((uint)(2u));
			this.textField_11.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 63, 27);
			this.textField_11.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.textField_11.ForeColor = System.Drawing.SystemColors.WindowText;
			this.textField_11.IsOnlyInput = true;
			this.textField_11.Name = "textField_11";
			this.textField_11.TagName = "IPAddress";
			this.textField_11.Value = null;
			this.textField_11.Visible = false;
			this.textField_11.EndInit();
			// 
			// textField_12
			// 
			this.textField_12.BeginInit();
			this.textField_12.AngleIgnore = false;
			this.textField_12.BackColor = System.Drawing.SystemColors.Control;
			this.textField_12.DecimalPlacesCount = ((uint)(2u));
			this.textField_12.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 63, 49);
			this.textField_12.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.textField_12.ForeColor = System.Drawing.SystemColors.WindowText;
			this.textField_12.IsOnlyInput = true;
			this.textField_12.Name = "textField_12";
			this.textField_12.TagName = "PORT";
			this.textField_12.Value = ((short)(0));
			this.textField_12.Visible = false;
			this.textField_12.EndInit();
			// 
			// textField_13
			// 
			this.textField_13.BeginInit();
			this.textField_13.AngleIgnore = false;
			this.textField_13.BackColor = System.Drawing.SystemColors.Window;
			this.textField_13.DecimalPlacesCount = ((uint)(2u));
			this.textField_13.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 62, 86);
			this.textField_13.Font = new NxtControl.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular);
			this.textField_13.ForeColor = System.Drawing.SystemColors.WindowText;
			this.textField_13.Name = "textField_13";
			this.textField_13.TagName = "DataReceived";
			this.textField_13.Value = 0F;
			this.textField_13.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle1,
									this.freeText1,
									this.freeText2,
									this.freeText3,
									this.freeText4,
									this.textField_11,
									this.textField_12,
									this.textField_13});
		}
		private NxtStudio.Symbols.TextField<float> textField_13;
		private NxtStudio.Symbols.TextField<short> textField_12;
		private NxtStudio.Symbols.TextField<string> textField_11;
		private NxtControl.GuiFramework.FreeText freeText4;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtControl.GuiFramework.FreeText freeText3;
		private NxtControl.GuiFramework.FreeText freeText2;
		private NxtControl.GuiFramework.FreeText freeText1;
		#endregion
	}
}
