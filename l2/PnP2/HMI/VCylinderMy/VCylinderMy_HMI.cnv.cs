﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/24/2017
 * Time: 4:32 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.VCylinderMy
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void PoschangedValueChanged(object sender, ValueChangedEventArgs e)
		{
		  
			NxtControl.Drawing.PointF newPos = grpInnerPart.Location;
		  float position = (float)e.Value;
			double relpos = 115.0d/100.0d * position + 15.0d;
			if (relpos > 220.0d) {
				isMax.Visible = true;
				isMin.Visible = false;
			} else if (relpos < 25.0d) {
				isMax.Visible = false;
				isMin.Visible = true;
			} else {
				isMax.Visible = false;
				isMin.Visible = false;
			}
			newPos.Y = relpos;
			grpInnerPart.Location = newPos;
		}
		
		void LabelValueChanged(object sender, ValueChangedEventArgs e)
		{
		  LabelName.Text = (string)Label.Value;
		}
	}
}
