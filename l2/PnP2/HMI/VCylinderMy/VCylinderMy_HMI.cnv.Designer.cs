﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/24/2017
 * Time: 4:32 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.VCylinderMy
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.rectangle2 = new NxtControl.GuiFramework.Rectangle();
			this.rectangle3 = new NxtControl.GuiFramework.Rectangle();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.roundedRectangle2 = new NxtControl.GuiFramework.RoundedRectangle();
			this.group1 = new NxtControl.GuiFramework.Group();
			this.isMax = new NxtControl.GuiFramework.RoundedRectangle();
			this.isMin = new NxtControl.GuiFramework.RoundedRectangle();
			this.polygon1 = new NxtControl.GuiFramework.Polygon();
			this.LabelName = new NxtControl.GuiFramework.Label();
			this.pipe1 = new NxtControl.GuiFramework.Pipe();
			this.pipe2 = new NxtControl.GuiFramework.Pipe();
			this.pipe3 = new NxtControl.GuiFramework.Pipe();
			this.poschanged = new System.HMI.Symbols.Execute<float>();
			this.Label = new System.HMI.Symbols.Execute<string>();
			this.grpInnerPart = new NxtControl.GuiFramework.Group();
			this.group2 = new NxtControl.GuiFramework.Group();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(31)), ((float)(127)), ((float)(16)), ((float)(136)));
			this.rectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.49F,
															1F})));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			this.rectangle1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// rectangle2
			// 
			this.rectangle2.Bounds = new NxtControl.Drawing.RectF(((float)(24)), ((float)(261)), ((float)(29)), ((float)(15)));
			this.rectangle2.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.49F,
															1F})));
			this.rectangle2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle2.Name = "rectangle2";
			this.rectangle2.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// rectangle3
			// 
			this.rectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(7)), ((float)(114)), ((float)(65)), ((float)(13)));
			this.rectangle3.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.49F,
															1F})));
			this.rectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle3.Name = "rectangle3";
			this.rectangle3.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(1)), ((float)(99)), ((float)(76)), ((float)(158)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Radius = 10;
			// 
			// roundedRectangle2
			// 
			this.roundedRectangle2.Bounds = new NxtControl.Drawing.RectF(((float)(6)), ((float)(107)), ((float)(66)), ((float)(150)));
			this.roundedRectangle2.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(154)), ((byte)(154)), ((byte)(154))),
															new NxtControl.Drawing.Color(((byte)(78)), ((byte)(78)), ((byte)(78))),
															new NxtControl.Drawing.Color(((byte)(154)), ((byte)(154)), ((byte)(154))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.23F,
															0.48F,
															0.75F,
															1F})));
			this.roundedRectangle2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle2.Name = "roundedRectangle2";
			this.roundedRectangle2.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.roundedRectangle2.Radius = 10;
			// 
			// group1
			// 
			this.group1.BeginInit();
			this.group1.Name = "group1";
			this.group1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.roundedRectangle1,
									this.roundedRectangle2});
			this.group1.EndInit();
			// 
			// isMax
			// 
			this.isMax.Bounds = new NxtControl.Drawing.RectF(((float)(6)), ((float)(244)), ((float)(66)), ((float)(9)));
			this.isMax.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.32F,
															0.5F,
															0.68F,
															1F})));
			this.isMax.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.isMax.Name = "isMax";
			this.isMax.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.isMax.Radius = 7;
			this.isMax.Visible = false;
			// 
			// isMin
			// 
			this.isMin.Bounds = new NxtControl.Drawing.RectF(((float)(6)), ((float)(105)), ((float)(66)), ((float)(9)));
			this.isMin.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.32F,
															0.5F,
															0.68F,
															1F})));
			this.isMin.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.isMin.Name = "isMin";
			this.isMin.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.isMin.Radius = 7;
			// 
			// polygon1
			// 
			this.polygon1.Bounds = new NxtControl.Drawing.RectF(((float)(15)), ((float)(265)), ((float)(40)), ((float)(10)));
			this.polygon1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.HorizontalLeft, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(114)), ((byte)(114)), ((byte)(114))),
															new NxtControl.Drawing.Color(((byte)(210)), ((byte)(210)), ((byte)(210))),
															new NxtControl.Drawing.Color(((byte)(114)), ((byte)(114)), ((byte)(114))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.27F,
															0.5F,
															0.72F,
															1F})));
			this.polygon1.Closed = true;
			this.polygon1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.polygon1.Name = "polygon1";
			this.polygon1.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(26.111111111111114, 265),
									new NxtControl.Drawing.PointF(55, 265),
									new NxtControl.Drawing.PointF(55, 275),
									new NxtControl.Drawing.PointF(26.111111111111114, 275),
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(15, 270),
									new NxtControl.Drawing.PointF(15, 270)});
			// 
			// LabelName
			// 
			this.LabelName.AngleIgnore = true;
			this.LabelName.Bounds = new NxtControl.Drawing.RectF(((float)(65)), ((float)(260)), ((float)(60)), ((float)(20)));
			this.LabelName.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.LabelName.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.LabelName.Name = "LabelName";
			this.LabelName.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.LabelName.Text = "Name";
			// 
			// pipe1
			// 
			this.pipe1.Bounds = new NxtControl.Drawing.RectF(((float)(26)), ((float)(44)), ((float)(0)), ((float)(56)));
			this.pipe1.InnerColor = new NxtControl.Drawing.Color(((byte)(210)), ((byte)(222)), ((byte)(243)));
			this.pipe1.Name = "pipe1";
			this.pipe1.OuterColor = new NxtControl.Drawing.Color(((byte)(82)), ((byte)(122)), ((byte)(174)));
			this.pipe1.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(26, 44),
									new NxtControl.Drawing.PointF(26, 100)});
			this.pipe1.Width = 8;
			// 
			// pipe2
			// 
			this.pipe2.Bounds = new NxtControl.Drawing.RectF(((float)(136)), ((float)(45)), ((float)(0)), ((float)(211)));
			this.pipe2.InnerColor = new NxtControl.Drawing.Color(((byte)(210)), ((byte)(222)), ((byte)(243)));
			this.pipe2.Name = "pipe2";
			this.pipe2.OuterColor = new NxtControl.Drawing.Color(((byte)(82)), ((byte)(122)), ((byte)(174)));
			this.pipe2.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(136, 45),
									new NxtControl.Drawing.PointF(136, 256)});
			this.pipe2.Width = 8;
			// 
			// pipe3
			// 
			this.pipe3.Bounds = new NxtControl.Drawing.RectF(((float)(77)), ((float)(252)), ((float)(60)), ((float)(0)));
			this.pipe3.InnerColor = new NxtControl.Drawing.Color(((byte)(210)), ((byte)(222)), ((byte)(243)));
			this.pipe3.Name = "pipe3";
			this.pipe3.OuterColor = new NxtControl.Drawing.Color(((byte)(82)), ((byte)(122)), ((byte)(174)));
			this.pipe3.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(137, 252),
									new NxtControl.Drawing.PointF(77, 252)});
			this.pipe3.Width = 8;
			// 
			// poschanged
			// 
			this.poschanged.BeginInit();
			this.poschanged.AngleIgnore = false;
			this.poschanged.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 11, 293);
			this.poschanged.IsOnlyInput = true;
			this.poschanged.Name = "poschanged";
			this.poschanged.TagName = "pos";
			this.poschanged.Value = 0F;
			this.poschanged.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PoschangedValueChanged);
			this.poschanged.EndInit();
			// 
			// Label
			// 
			this.Label.BeginInit();
			this.Label.AngleIgnore = false;
			this.Label.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 11, 313);
			this.Label.IsOnlyInput = true;
			this.Label.Name = "Label";
			this.Label.TagName = "Label";
			this.Label.Value = null;
			this.Label.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.LabelValueChanged);
			this.Label.EndInit();
			// 
			// grpInnerPart
			// 
			this.grpInnerPart.BeginInit();
			this.grpInnerPart.Name = "grpInnerPart";
			this.grpInnerPart.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle1,
									this.rectangle2,
									this.rectangle3,
									this.polygon1});
			this.grpInnerPart.EndInit();
			// 
			// group2
			// 
			this.group2.BeginInit();
			this.group2.Name = "group2";
			this.group2.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.group1,
									this.isMax,
									this.isMin});
			this.group2.EndInit();
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.freeText1.Location = new NxtControl.Drawing.PointF(54, 50);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Text";
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.pipe1,
									this.LabelName,
									this.pipe3,
									this.pipe2,
									this.poschanged,
									this.Label,
									this.group2,
									this.grpInnerPart,
									this.freeText1});
			this.SymbolSize = new System.Drawing.Size(161, 437);
		}
		private NxtControl.GuiFramework.FreeText freeText1;
		private System.HMI.Symbols.Execute<float> poschanged;
		private NxtControl.GuiFramework.Group group2;
		private NxtControl.GuiFramework.Group grpInnerPart;
		private System.HMI.Symbols.Execute<string> Label;
		private NxtControl.GuiFramework.Pipe pipe3;
		private NxtControl.GuiFramework.Pipe pipe2;
		private NxtControl.GuiFramework.Pipe pipe1;
		private NxtControl.GuiFramework.Label LabelName;
		private NxtControl.GuiFramework.Polygon polygon1;
		private NxtControl.GuiFramework.RoundedRectangle isMin;
		private NxtControl.GuiFramework.RoundedRectangle isMax;
		private NxtControl.GuiFramework.Group group1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle2;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private NxtControl.GuiFramework.Rectangle rectangle3;
		private NxtControl.GuiFramework.Rectangle rectangle2;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		#endregion
	}
}
