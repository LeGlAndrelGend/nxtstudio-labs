/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/24/2017
 * Time: 3:13 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #VCylinderMy_HMI;

namespace NxtStudio.Symbols.VCylinderMy
{

  public class INDEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public INDEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_pos(ref System.Single value)
    {
      if (accessorService == null)
        return false;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.Single) var;
      return ret;
    }

    public System.Single? pos
    { get {
      if (accessorService == null)
        return null;
      float var = 0;
      bool ret = accessorService.GetFloatValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.Single) var;
    }  }

    public bool Get_Label(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String Label
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }


  }

}

namespace NxtStudio.Symbols.VCylinderMy
{

  public class INITO2EventArgs : System.EventArgs
  {
    public INITO2EventArgs()
    {
    }
    private System.Boolean? OUT_field = null;
    public System.Boolean? OUT
    {
       get { return OUT_field; }
       set { OUT_field = value; }
    }

  }

}

namespace NxtStudio.Symbols.VCylinderMy
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.VCylinderMy.INDEventArgs> IND_Fired;

    protected override void OnEndInit()
    {
      if (IND_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (IND_Fired != null)
            IND_Fired(this, new NxtStudio.Symbols.VCylinderMy.INDEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }

  }
}

namespace NxtStudio.Symbols.VCylinderMy
{}
#endregion #VCylinderMy_HMI;

#endregion Definitions;
