/*
 * Created by nxtStudio.
 * User: gzha046
 * Date: 24/01/2011
 * Time: 3:24 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace NxtStudio.Symbols.UDPSocketClientDouble
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	/// 
	// SEND data object
	
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			this.REQ_Fired += new EventHandler<NxtStudio.Symbols.UDPSocketClientDouble.REQEventArgs>(OnREQ);
						
		}
		
		

	
	  public class ObjectToSend{ 
       public byte[] dataToSend = new byte[8];
       public double valueToSend = 0.0;
     }
		
		public class AsynchronousClientSocket{ 

      private  Socket clientSocket; 
      private  ObjectToSend toSend = new ObjectToSend();
      private NxtStudio.Symbols.UDPSocketClientDouble.HMI inHMI;// = new NxtStudio.Symbols.UDPSocketClientDouble.HMI();
      public AsynchronousClientSocket(){
      }
  
      public  void SendUdp(string ipAd, int sendPort, string messageToSend, NxtStudio.Symbols.UDPSocketClientDouble.HMI newHMI)
      {
          
          inHMI = newHMI;
          toSend.valueToSend = double.Parse(messageToSend);
          toSend.dataToSend = BitConverter.GetBytes(toSend.valueToSend); 
          IPEndPoint ipeSender = new IPEndPoint(IPAddress.Parse(ipAd), sendPort);
          EndPoint epSender = (EndPoint)ipeSender;
          clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
          clientSocket.BeginSendTo(toSend.dataToSend, 0, toSend.dataToSend.Length, SocketFlags.None, epSender, 
                                new AsyncCallback(OnSend), epSender); 
      }
  
      private void OnSend(IAsyncResult ar)
      {
        
          try
          {
              clientSocket.EndSendTo(ar);
              inHMI.FireEvent_CNF(); 
          }
          catch (Exception ex)
          {
              System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
          }
         
      }
    }
	
	private void OnREQ(object sender, NxtStudio.Symbols.UDPSocketClientDouble.REQEventArgs e){
		  
		  AsynchronousClientSocket myClientSocket = new AsynchronousClientSocket();
		  string IpAddress = "";
		  short SendPORT = 0;
		  string DataToSend = "";
		  
		  try{
		    if (e.Get_IPAddress(ref IpAddress)!= false){
		      if (e.Get_PORT(ref SendPORT)!= false){
		        if (e.Get_DataToSend(ref DataToSend)!=false){
		          myClientSocket.SendUdp(IpAddress, SendPORT, DataToSend, this);
		        }
		      }
		    }
		    
		  }
		  catch (Exception ex){
		    MessageBox.Show(ex.Message.ToString());
		  }
		  
		  
		  
		}
		
		
		
 }	
}
