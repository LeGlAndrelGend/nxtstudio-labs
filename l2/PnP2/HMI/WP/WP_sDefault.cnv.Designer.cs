﻿/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 12/15/2012
 * Time: 10:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.WP
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ellipse1 = new NxtControl.GuiFramework.Ellipse();
			this.pos = new System.HMI.Symbols.Execute<float>();
			// 
			// ellipse1
			// 
			this.ellipse1.Bounds = new NxtControl.Drawing.RectF(((float)(1)), ((float)(3)), ((float)(34)), ((float)(34)));
			this.ellipse1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))));
			this.ellipse1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ellipse1.Name = "ellipse1";
			// 
			// pos
			// 
			this.pos.BeginInit();
			this.pos.AngleIgnore = false;
			this.pos.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 20, 45);
			this.pos.IsOnlyInput = true;
			this.pos.Name = "pos";
			this.pos.TagName = "pos";
			this.pos.Value = 0F;
			this.pos.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PosValueChanged);
			this.pos.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.ellipse1,
									this.pos});
			this.SymbolSize = new System.Drawing.Size(127, 70);
		}
		private System.HMI.Symbols.Execute<float> pos;
		private NxtControl.GuiFramework.Ellipse ellipse1;
		#endregion
	}
}
