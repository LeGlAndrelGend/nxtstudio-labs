﻿/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 12/15/2012
 * Time: 10:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.WP
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void PosValueChanged(object sender, ValueChangedEventArgs e)
		{
		  //this.ellipse1.Location.X= (double)pos+1.0d;
		  
		  NxtControl.Drawing.PointF newPos = ellipse1.Location;
		  float position = (float)e.Value;
			double relpos = position + 1.0d;
			newPos.X = relpos;
			ellipse1.Location = newPos;
		}
	}
}
