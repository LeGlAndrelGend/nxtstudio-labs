﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/24/2017
 * Time: 9:15 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.VCylinderMySA
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Cylinder = new NxtStudio.Symbols.VCylinderMy.HMI();
			this.TopSns = new NxtStudio.Symbols.Sensor.HMI();
			this.BottomSns = new NxtStudio.Symbols.Sensor.HMI();
			this.ValvePush = new NxtStudio.Symbols.Valve.HMI();
			this.ValvePop = new NxtStudio.Symbols.Valve.HMI();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			// 
			// Cylinder
			// 
			this.Cylinder.BeginInit();
			this.Cylinder.AngleIgnore = false;
			this.Cylinder.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 0, 39);
			this.Cylinder.Name = "Cylinder";
			this.Cylinder.SecurityToken = ((uint)(4294967295u));
			this.Cylinder.TagName = "Cylinder";
			this.Cylinder.EndInit();
			// 
			// TopSns
			// 
			this.TopSns.BeginInit();
			this.TopSns.AngleIgnore = false;
			this.TopSns.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 80, 114);
			this.TopSns.Name = "TopSns";
			this.TopSns.SecurityToken = ((uint)(4294967295u));
			this.TopSns.TagName = "TopSns";
			this.TopSns.EndInit();
			// 
			// BottomSns
			// 
			this.BottomSns.BeginInit();
			this.BottomSns.AngleIgnore = false;
			this.BottomSns.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 80, 227);
			this.BottomSns.Name = "BottomSns";
			this.BottomSns.SecurityToken = ((uint)(4294967295u));
			this.BottomSns.TagName = "BottomSns";
			this.BottomSns.EndInit();
			// 
			// ValvePush
			// 
			this.ValvePush.BeginInit();
			this.ValvePush.AngleIgnore = false;
			this.ValvePush.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 5, 10);
			this.ValvePush.Name = "ValvePush";
			this.ValvePush.SecurityToken = ((uint)(4294967295u));
			this.ValvePush.TagName = "ValvePush";
			this.ValvePush.EndInit();
			// 
			// ValvePop
			// 
			this.ValvePop.BeginInit();
			this.ValvePop.AngleIgnore = false;
			this.ValvePop.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 118, 10);
			this.ValvePop.Name = "ValvePop";
			this.ValvePop.SecurityToken = ((uint)(4294967295u));
			this.ValvePop.TagName = "ValvePop";
			this.ValvePop.EndInit();
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(9)), ((float)(223)), ((float)(283)));
			this.rectangle1.Brush = new NxtControl.Drawing.Brush();
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			this.rectangle1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(26)), ((byte)(62)), ((byte)(114))), 1F, NxtControl.Drawing.DashStyle.Dash);
			// 
			// HMI
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.Cylinder,
									this.TopSns,
									this.BottomSns,
									this.ValvePush,
									this.ValvePop,
									this.rectangle1});
			this.SymbolSize = new System.Drawing.Size(228, 441);
		}
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtStudio.Symbols.Valve.HMI ValvePop;
		private NxtStudio.Symbols.Valve.HMI ValvePush;
		private NxtStudio.Symbols.Sensor.HMI BottomSns;
		private NxtStudio.Symbols.Sensor.HMI TopSns;
		private NxtStudio.Symbols.VCylinderMy.HMI Cylinder;
		#endregion
	}
}
