using System;
using NxtControl.GuiFramework;
using NxtControl.Services;


#region Definitions;
#region VCylinderSA_HMI;

namespace NxtStudio.Symbols.VCylinderSA
{
  partial class HMI
  {

    private NxtStudio.Controllers.VCylinderSA.FacePlate01 FacePlate01
    {
      get
      { 
        if (IsOpenFaceplateSecure() == false)
          return null;

        NxtStudio.Controllers.VCylinderSA.FacePlate01 faceplate = null; 
        IHMIManagementService hmiManagementService = (IHMIManagementService)ServiceProvider.GetService(typeof(IHMIManagementService));
        if (hmiManagementService != null)
          faceplate = (NxtStudio.Controllers.VCylinderSA.FacePlate01)hmiManagementService.GetRegisteredHMIFaceplate(MapPath, typeof(NxtStudio.Controllers.VCylinderSA.FacePlate01));

        if (faceplate == null)
        {
          faceplate = new NxtStudio.Controllers.VCylinderSA.FacePlate01();
          faceplate.SetConnectionInfo(this.TagName, this.SymbolPath, this.ChannelId, GetType());
          if (hmiManagementService != null)
            hmiManagementService.RegisterHMISymbolController(faceplate);
        }
        return faceplate;
      }
    }
     
    protected override void DoOpenFaceplate(OpenFaceplate openFaceplate)
    {
      NxtControl.GuiFramework.HMIFaceplate hmiFaceplate = null;

      if (hmiFaceplate != null)
      {
        if (hmiFaceplate.Initialized == true)
          hmiFaceplate.Activate();
        else
        {
          OnInitializeFaceplate(hmiFaceplate);
          hmiFaceplate.Show(this);
        }
      }
    }

    public override void DoOpenFaceplate(string openFaceplate)
    {
      NxtControl.GuiFramework.HMIFaceplate hmiFaceplate = null;

      if (hmiFaceplate != null)
      {
        if (hmiFaceplate.Initialized == true)
          hmiFaceplate.Activate();
        else
        {
          OnInitializeFaceplate(hmiFaceplate);
          hmiFaceplate.Show(this);
        }
      }
    }

  }
}
#endregion VCylinderSA_HMI;

#endregion Definitions;

