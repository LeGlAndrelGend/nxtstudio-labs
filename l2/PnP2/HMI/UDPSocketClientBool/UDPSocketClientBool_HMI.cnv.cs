/*
 * Created by nxtStudio.
 * User: gzha046
 * Date: 24/01/2011
 * Time: 3:24 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace NxtStudio.Symbols.UDPSocketClientBool
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	/// 
	// SEND data object
	
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			this.REQ_Fired += new EventHandler<NxtStudio.Symbols.UDPSocketClientBool.REQEventArgs>(OnREQ);
						
		}
		
		

	
	  public class ObjectToSend{ 
		  public byte[] dataToSend = new byte[1];// new byte[8];
       public bool valueToSend = false;
     }
		
		public class AsynchronousClientSocket{ 

      private  Socket clientSocket; 
      private  ObjectToSend toSend = new ObjectToSend();
      private NxtStudio.Symbols.UDPSocketClientBool.HMI inHMI;// = new NxtStudio.Symbols.UDPSocketClientBool.HMI();
      public AsynchronousClientSocket(){
      }
  
      public  void SendUdp(string ipAd, int sendPort, bool messageToSend, NxtStudio.Symbols.UDPSocketClientBool.HMI newHMI)
      {
          
          inHMI = newHMI;
          //toSend.valueToSend = double.Parse(messageToSend);
          toSend.valueToSend = messageToSend;
          toSend.dataToSend = BitConverter.GetBytes(toSend.valueToSend); //BitConverter.GetBytes(toSend.valueToSend);
          IPEndPoint ipeSender = new IPEndPoint(IPAddress.Parse(ipAd), sendPort);
          EndPoint epSender = (EndPoint)ipeSender;
          clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
          clientSocket.BeginSendTo(toSend.dataToSend, 0, toSend.dataToSend.Length, SocketFlags.None, epSender, 
                                new AsyncCallback(OnSend), epSender); 
      }
  
      private void OnSend(IAsyncResult ar)
      {
        
          try
          {
              clientSocket.EndSendTo(ar);
              inHMI.FireEvent_CNF(); 
          }
          catch (Exception ex)
          {
              System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
          }
         
      }
    }
	
	private void OnREQ(object sender, NxtStudio.Symbols.UDPSocketClientBool.REQEventArgs e){
		  
		  AsynchronousClientSocket myClientSocket = new AsynchronousClientSocket();
		  string IpAddress = "";
		  short SendPORT = 0;
		  bool DataToSend = false;
		  
		  try{
		    if (e.Get_IPAddress(ref IpAddress)!= false){
		      if (e.Get_PORT(ref SendPORT)!= false){
		        if (e.Get_DataToSend(ref DataToSend)!=false){
		          myClientSocket.SendUdp(IpAddress, SendPORT, DataToSend, this);
		        }
		      }
		    }
		    
		  }
		  catch (Exception ex){
		    MessageBox.Show(ex.Message.ToString());
		  }
		  
		  
		  
		}
		
		
		
 }	
}
