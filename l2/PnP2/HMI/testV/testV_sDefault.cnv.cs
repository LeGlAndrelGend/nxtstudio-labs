﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/27/2017
 * Time: 9:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.testV
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void PosValueChanged(object sender, ValueChangedEventArgs e)
		{
			NxtControl.Drawing.PointF newPos = grpInnerPart.Location;
		  float position = (float)e.Value;
			double relpos = 115.0d/100.0d * position + 15.0d;
			if (relpos > 120.0d) {
				isMax.Visible = true;
				isMin.Visible = false;
			} else if (relpos < 25.0d) {
				isMax.Visible = false;
				isMin.Visible = true;
			} else {
				isMax.Visible = false;
				isMin.Visible = false;
			}
			newPos.X = relpos;
			grpInnerPart.Location = newPos;
		}
		
		void LabelValueChanged(object sender, ValueChangedEventArgs e)
		{
			labelName.Text=(string)Label.Value;
		}
	}
}
