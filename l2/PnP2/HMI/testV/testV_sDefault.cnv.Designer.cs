﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/27/2017
 * Time: 9:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.testV
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.isMax = new NxtControl.GuiFramework.RoundedRectangle();
			this.polygon3 = new NxtControl.GuiFramework.Polygon();
			this.rectangle7 = new NxtControl.GuiFramework.Rectangle();
			this.rectangle4 = new NxtControl.GuiFramework.Rectangle();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.isMin = new NxtControl.GuiFramework.RoundedRectangle();
			this.roundedRectangle3 = new NxtControl.GuiFramework.RoundedRectangle();
			this.roundedRectangle4 = new NxtControl.GuiFramework.RoundedRectangle();
			this.pipe1 = new NxtControl.GuiFramework.Pipe();
			this.pipe2 = new NxtControl.GuiFramework.Pipe();
			this.group2 = new NxtControl.GuiFramework.Group();
			this.grpInnerPart = new NxtControl.GuiFramework.Group();
			this.pos = new System.HMI.Symbols.Execute<float>();
			this.Label = new System.HMI.Symbols.Execute<string>();
			this.labelName = new NxtControl.GuiFramework.Label();
			// 
			// isMax
			// 
			this.isMax.Bounds = new NxtControl.Drawing.RectF(((float)(165)), ((float)(90)), ((float)(9)), ((float)(65.9277114868164)));
			this.isMax.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.24F,
															0.51F,
															0.76F,
															1F})));
			this.isMax.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.isMax.Name = "isMax";
			this.isMax.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.isMax.Radius = 7;
			this.isMax.Visible = false;
			// 
			// polygon3
			// 
			this.polygon3.Bounds = new NxtControl.Drawing.RectF(((float)(187)), ((float)(107)), ((float)(9)), ((float)(42)));
			this.polygon3.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(114)), ((byte)(114)), ((byte)(114))),
															new NxtControl.Drawing.Color(((byte)(210)), ((byte)(210)), ((byte)(210))),
															new NxtControl.Drawing.Color(((byte)(114)), ((byte)(114)), ((byte)(114))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.25F,
															0.5F,
															0.74F,
															1F})));
			this.polygon3.Closed = true;
			this.polygon3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.polygon3.Name = "polygon3";
			this.polygon3.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(187, 142.53846153846155),
									new NxtControl.Drawing.PointF(187, 107),
									new NxtControl.Drawing.PointF(196, 107),
									new NxtControl.Drawing.PointF(196, 142.53846153846155),
									new NxtControl.Drawing.PointF(191.52662721893495, 149),
									new NxtControl.Drawing.PointF(187, 142.53846153846155),
									new NxtControl.Drawing.PointF(187, 142.53846153846155),
									new NxtControl.Drawing.PointF(187, 142.53846153846155),
									new NxtControl.Drawing.PointF(187, 142.53846153846155),
									new NxtControl.Drawing.PointF(187, 142.53846153846155)});
			// 
			// rectangle7
			// 
			this.rectangle7.Bounds = new NxtControl.Drawing.RectF(((float)(36)), ((float)(92)), ((float)(13)), ((float)(65)));
			this.rectangle7.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.48F,
															1F})));
			this.rectangle7.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle7.Name = "rectangle7";
			this.rectangle7.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.rectangle7.ShadowOffSet = 2;
			// 
			// rectangle4
			// 
			this.rectangle4.Bounds = new NxtControl.Drawing.RectF(((float)(184)), ((float)(111)), ((float)(15)), ((float)(29)));
			this.rectangle4.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.48F,
															1F})));
			this.rectangle4.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle4.Name = "rectangle4";
			this.rectangle4.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(49)), ((float)(117)), ((float)(136)), ((float)(16)));
			this.rectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(150)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)))}, new float[] {
															0F,
															0.48F,
															1F})));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			this.rectangle1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(30)), ((byte)(30)), ((byte)(30))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.rectangle1.ShadowOffSet = 2;
			// 
			// isMin
			// 
			this.isMin.Bounds = new NxtControl.Drawing.RectF(((float)(25)), ((float)(90)), ((float)(9)), ((float)(65.9277114868164)));
			this.isMin.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18))),
															new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.24F,
															0.51F,
															0.76F,
															1F})));
			this.isMin.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.isMin.Name = "isMin";
			this.isMin.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.isMin.Radius = 7;
			this.isMin.Visible = false;
			// 
			// roundedRectangle3
			// 
			this.roundedRectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(19)), ((float)(85)), ((float)(158)), ((float)(76)));
			this.roundedRectangle3.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))));
			this.roundedRectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle3.Name = "roundedRectangle3";
			this.roundedRectangle3.Radius = 10;
			// 
			// roundedRectangle4
			// 
			this.roundedRectangle4.Bounds = new NxtControl.Drawing.RectF(((float)(27)), ((float)(90.49395751953125)), ((float)(150)), ((float)(65.9277114868164)));
			this.roundedRectangle4.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.VerticalBottom, new NxtControl.Drawing.GradientFillColorBlend(new NxtControl.Drawing.Color[] {
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))),
															new NxtControl.Drawing.Color(((byte)(154)), ((byte)(154)), ((byte)(154))),
															new NxtControl.Drawing.Color(((byte)(78)), ((byte)(78)), ((byte)(78))),
															new NxtControl.Drawing.Color(((byte)(154)), ((byte)(154)), ((byte)(154))),
															new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255)))}, new float[] {
															0F,
															0.24F,
															0.51F,
															0.76F,
															1F})));
			this.roundedRectangle4.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle4.Name = "roundedRectangle4";
			this.roundedRectangle4.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.roundedRectangle4.Radius = 7;
			// 
			// pipe1
			// 
			this.pipe1.Bounds = new NxtControl.Drawing.RectF(((float)(29)), ((float)(35)), ((float)(0)), ((float)(51)));
			this.pipe1.InnerColor = new NxtControl.Drawing.Color(((byte)(210)), ((byte)(222)), ((byte)(243)));
			this.pipe1.Name = "pipe1";
			this.pipe1.OuterColor = new NxtControl.Drawing.Color(((byte)(82)), ((byte)(122)), ((byte)(174)));
			this.pipe1.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(29, 35),
									new NxtControl.Drawing.PointF(29, 86)});
			this.pipe1.Width = 8;
			// 
			// pipe2
			// 
			this.pipe2.Bounds = new NxtControl.Drawing.RectF(((float)(172)), ((float)(35)), ((float)(0)), ((float)(51)));
			this.pipe2.InnerColor = new NxtControl.Drawing.Color(((byte)(210)), ((byte)(222)), ((byte)(243)));
			this.pipe2.Name = "pipe2";
			this.pipe2.OuterColor = new NxtControl.Drawing.Color(((byte)(82)), ((byte)(122)), ((byte)(174)));
			this.pipe2.Points.AddRange(new NxtControl.Drawing.PointF[] {
									new NxtControl.Drawing.PointF(172, 35),
									new NxtControl.Drawing.PointF(172, 86)});
			this.pipe2.Width = 8;
			// 
			// group2
			// 
			this.group2.BeginInit();
			this.group2.Name = "group2";
			this.group2.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.roundedRectangle3,
									this.roundedRectangle4,
									this.isMin,
									this.isMax});
			this.group2.EndInit();
			// 
			// grpInnerPart
			// 
			this.grpInnerPart.BeginInit();
			this.grpInnerPart.Name = "grpInnerPart";
			this.grpInnerPart.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle7,
									this.rectangle1,
									this.rectangle4,
									this.polygon3});
			this.grpInnerPart.EndInit();
			// 
			// pos
			// 
			this.pos.BeginInit();
			this.pos.AngleIgnore = false;
			this.pos.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 58, 204);
			this.pos.IsOnlyInput = true;
			this.pos.Name = "pos";
			this.pos.TagName = "pos";
			this.pos.Value = 0F;
			this.pos.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PosValueChanged);
			this.pos.EndInit();
			// 
			// Label
			// 
			this.Label.BeginInit();
			this.Label.AngleIgnore = false;
			this.Label.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 56, 231);
			this.Label.IsOnlyInput = true;
			this.Label.Name = "Label";
			this.Label.TagName = "Label";
			this.Label.Value = null;
			this.Label.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.LabelValueChanged);
			this.Label.EndInit();
			// 
			// labelName
			// 
			this.labelName.AngleIgnore = true;
			this.labelName.Bounds = new NxtControl.Drawing.RectF(((float)(77)), ((float)(170)), ((float)(49)), ((float)(17)));
			this.labelName.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.labelName.Font = new NxtControl.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
			this.labelName.Name = "labelName";
			this.labelName.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.labelName.Text = "Label";
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.pipe1,
									this.pipe2,
									this.group2,
									this.grpInnerPart,
									this.pos,
									this.Label,
									this.labelName});
			this.SymbolSize = new System.Drawing.Size(600, 400);
		}
		private NxtControl.GuiFramework.Label labelName;
		private System.HMI.Symbols.Execute<string> Label;
		private System.HMI.Symbols.Execute<float> pos;
		private NxtControl.GuiFramework.Group grpInnerPart;
		private NxtControl.GuiFramework.Group group2;
		private NxtControl.GuiFramework.Pipe pipe2;
		private NxtControl.GuiFramework.Pipe pipe1;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle4;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle3;
		private NxtControl.GuiFramework.RoundedRectangle isMin;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtControl.GuiFramework.Rectangle rectangle4;
		private NxtControl.GuiFramework.Rectangle rectangle7;
		private NxtControl.GuiFramework.Polygon polygon3;
		private NxtControl.GuiFramework.RoundedRectangle isMax;
		#endregion
	}
}
