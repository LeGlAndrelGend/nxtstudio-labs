﻿/*
 * Created by nxtStudio.
 * User: Horst Mayer
 * Date: 9/14/2008
 * Time: 10:47 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.Switch
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HMI));
			this.openLEFT = new NxtControl.GuiFramework.Rectangle();
			this.closedLEFT = new NxtControl.GuiFramework.Rectangle();
			this.openRIGHT = new NxtControl.GuiFramework.Rectangle();
			this.closedRIGHT = new NxtControl.GuiFramework.Rectangle();
			this.posLEFTchanged = new NxtStudio.Symbols.Execute<float>();
			this.posRIGHTChanged = new NxtStudio.Symbols.Execute<float>();
			this.posChanged = new NxtStudio.Symbols.Execute<float>();
			this.swL = new NxtStudio.Symbols.Execute<bool>();
			this.swR = new NxtStudio.Symbols.Execute<bool>();
			this.ledSquare_11 = new NxtStudio.Symbols.LedSquare<bool>();
			this.ledSquare_12 = new NxtStudio.Symbols.LedSquare<bool>();
			// 
			// openLEFT
			// 
			this.openLEFT.Bounds = new NxtControl.Drawing.RectF(((float)(15)), ((float)(15)), ((float)(25)), ((float)(65)));
			this.openLEFT.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.openLEFT.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.openLEFT.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("openLEFT.ImageStream")));
			this.openLEFT.Name = "openLEFT";
			this.openLEFT.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.openLEFT.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// closedLEFT
			// 
			this.closedLEFT.Bounds = new NxtControl.Drawing.RectF(((float)(15)), ((float)(15)), ((float)(25)), ((float)(65)));
			this.closedLEFT.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.closedLEFT.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.closedLEFT.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("closedLEFT.ImageStream")));
			this.closedLEFT.Name = "closedLEFT";
			this.closedLEFT.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.closedLEFT.TextColor = new NxtControl.Drawing.Color("Black");
			this.closedLEFT.Visible = false;
			// 
			// openRIGHT
			// 
			this.openRIGHT.Bounds = new NxtControl.Drawing.RectF(((float)(149)), ((float)(14)), ((float)(25)), ((float)(65)));
			this.openRIGHT.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.openRIGHT.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.openRIGHT.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("openRIGHT.ImageStream")));
			this.openRIGHT.Name = "openRIGHT";
			this.openRIGHT.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.openRIGHT.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// closedRIGHT
			// 
			this.closedRIGHT.Bounds = new NxtControl.Drawing.RectF(((float)(150)), ((float)(15)), ((float)(25)), ((float)(65)));
			this.closedRIGHT.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.closedRIGHT.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.closedRIGHT.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("closedRIGHT.ImageStream")));
			this.closedRIGHT.Name = "closedRIGHT";
			this.closedRIGHT.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.closedRIGHT.TextColor = new NxtControl.Drawing.Color("Black");
			this.closedRIGHT.Visible = false;
			// 
			// posLEFTchanged
			// 
			this.posLEFTchanged.BeginInit();
			this.posLEFTchanged.AngleIgnore = false;
			this.posLEFTchanged.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 41, 45);
			this.posLEFTchanged.IsOnlyInput = true;
			this.posLEFTchanged.Name = "posLEFTchanged";
			this.posLEFTchanged.TagName = "posLEFT";
			this.posLEFTchanged.Value = 0F;
			this.posLEFTchanged.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PosLEFTchangedValueChanged);
			this.posLEFTchanged.EndInit();
			// 
			// posRIGHTChanged
			// 
			this.posRIGHTChanged.BeginInit();
			this.posRIGHTChanged.AngleIgnore = false;
			this.posRIGHTChanged.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 35, 20);
			this.posRIGHTChanged.IsOnlyInput = true;
			this.posRIGHTChanged.Name = "posRIGHTChanged";
			this.posRIGHTChanged.TagName = "posRIGHT";
			this.posRIGHTChanged.Value = 0F;
			this.posRIGHTChanged.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PosRIGHTChangedValueChanged);
			this.posRIGHTChanged.EndInit();
			// 
			// posChanged
			// 
			this.posChanged.BeginInit();
			this.posChanged.AngleIgnore = false;
			this.posChanged.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 38, 55);
			this.posChanged.IsOnlyInput = true;
			this.posChanged.Name = "posChanged";
			this.posChanged.TagName = "pos";
			this.posChanged.Value = 0F;
			this.posChanged.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.PosChangedValueChanged);
			this.posChanged.EndInit();
			// 
			// swL
			// 
			this.swL.BeginInit();
			this.swL.AngleIgnore = false;
			this.swL.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 35, 70);
			this.swL.IsOnlyInput = true;
			this.swL.Name = "swL";
			this.swL.TagName = "swLeft";
			this.swL.Value = false;
			this.swL.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.SwLValueChanged);
			this.swL.EndInit();
			// 
			// swR
			// 
			this.swR.BeginInit();
			this.swR.AngleIgnore = false;
			this.swR.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 30, 80);
			this.swR.IsOnlyInput = true;
			this.swR.Name = "swR";
			this.swR.TagName = "swRight";
			this.swR.Value = false;
			this.swR.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.SwRValueChanged);
			this.swR.EndInit();
			// 
			// ledSquare_11
			// 
			this.ledSquare_11.BeginInit();
			this.ledSquare_11.AngleIgnore = false;
			this.ledSquare_11.ColorFrame = new NxtControl.Drawing.Color("Black");
			this.ledSquare_11.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.ledSquare_11.ColorOn = new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30)));
			this.ledSquare_11.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 20, 90);
			this.ledSquare_11.Frame = 2;
			this.ledSquare_11.Name = "ledSquare_11";
			this.ledSquare_11.TagName = "swLeft";
			this.ledSquare_11.EndInit();
			// 
			// ledSquare_12
			// 
			this.ledSquare_12.BeginInit();
			this.ledSquare_12.AngleIgnore = false;
			this.ledSquare_12.ColorFrame = new NxtControl.Drawing.Color("Black");
			this.ledSquare_12.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.ledSquare_12.ColorOn = new NxtControl.Drawing.Color(((byte)(234)), ((byte)(22)), ((byte)(30)));
			this.ledSquare_12.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 154, 89);
			this.ledSquare_12.Frame = 2;
			this.ledSquare_12.Name = "ledSquare_12";
			this.ledSquare_12.TagName = "swRight";
			this.ledSquare_12.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.openLEFT,
									this.closedLEFT,
									this.openRIGHT,
									this.closedRIGHT,
									this.posLEFTchanged,
									this.posRIGHTChanged,
									this.posChanged,
									this.swL,
									this.swR,
									this.ledSquare_11,
									this.ledSquare_12});
			this.SymbolSize = new System.Drawing.Size(184, 105);
		}
		private NxtStudio.Symbols.LedSquare<bool> ledSquare_12;
		private NxtStudio.Symbols.LedSquare<bool> ledSquare_11;
		private NxtStudio.Symbols.Execute<bool> swR;
		private NxtStudio.Symbols.Execute<bool> swL;
		private NxtControl.GuiFramework.Rectangle closedLEFT;
		private NxtControl.GuiFramework.Rectangle closedRIGHT;
		private NxtStudio.Symbols.Execute<float> posChanged;
		private NxtStudio.Symbols.Execute<float> posRIGHTChanged;
		private NxtStudio.Symbols.Execute<float> posLEFTchanged;
		private NxtControl.GuiFramework.Rectangle openRIGHT;
		private NxtControl.GuiFramework.Rectangle openLEFT;
		#endregion
	}
}
