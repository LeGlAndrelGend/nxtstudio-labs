﻿/*
 * Created by nxtStudio.
 * User: Horst Mayer
 * Date: 9/14/2008
 * Time: 10:47 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;


namespace NxtStudio.Symbols.Switch
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		float posLEFT;
		float posRIGHT;
		//float range;
		
		void PosLEFTchangedValueChanged(object sender, ValueChangedEventArgs e)
		{
			posLEFT = (float)e.Value;
			openLEFT.X = posLEFT + (openLEFT.Width / 2);
			closedLEFT.X = posLEFT + (closedLEFT.Width / 2);
			//forcer.X = posLEFT + (forcer.Width / 2) ;
		}
		
		
		void PosRIGHTChangedValueChanged(object sender, ValueChangedEventArgs e)
		{
			posRIGHT = (float)e.Value;
			openRIGHT.X = posRIGHT; //- (openRIGHT.Width / 2 );
			closedRIGHT.X = posRIGHT;//- (closedRIGHT.Width / 2 );
		}
		
		void PosChangedValueChanged(object sender, ValueChangedEventArgs e)
		{
			float relpos;
			relpos = (float)e.Value;
			//openLEFT.X = relpos + posLEFT + (openLEFT.Width / 2);
			//openRIGHT.X = relpos + posRIGHT + + (openRIGHT.Width / 2);
			//forcer.X = relpos + (forcer.Width / 2);
		}

		
		void SwRValueChanged(object sender, ValueChangedEventArgs e)
		{
			closedRIGHT.Visible = (bool)e.Value;
			openRIGHT.Visible = !((bool)e.Value);
		
		}
		
		void SwLValueChanged(object sender, ValueChangedEventArgs e)
		{
			closedLEFT.Visible = (bool)e.Value;
			openLEFT.Visible = !((bool)e.Value);
		}
	}
}
