﻿/*
 * Created by nxtStudio.
 * User: Horst Mayer
 * Date: 9/14/2008
 * Time: 12:21 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.iValve
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Valve1 = new NxtStudio.Symbols.Valve.HMI();
			// 
			// Valve1
			// 
			this.Valve1.BeginInit();
			this.Valve1.AngleIgnore = false;
			this.Valve1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 4, 3);
			this.Valve1.Name = "Valve1";
			this.Valve1.SecurityToken = ((uint)(4294967295u));
			this.Valve1.TagName = "Valve1";
			this.Valve1.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.Valve1});
			this.SymbolSize = new System.Drawing.Size(112, 47);
		}
		private NxtStudio.Symbols.Valve.HMI Valve1;
		#endregion
	}
}
