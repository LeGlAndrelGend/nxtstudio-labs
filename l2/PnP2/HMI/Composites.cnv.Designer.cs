﻿/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 12/25/2012
 * Time: 10:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace NxtStudio.Canvases
{
	/// <summary>
	/// Summary description for TwoHorCyl.
	/// </summary>
	partial class Composites
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.inputXY = new NxtStudio.Symbols.ManControl.HMI();
			this.compcyl1 = new NxtStudio.Symbols.HCylinderSA.HMI();
			this.VerCylinderMy = new NxtStudio.Symbols.VCylinderMySA.HMI();
			// 
			// inputXY
			// 
			this.inputXY.BeginInit();
			this.inputXY.AngleIgnore = false;
			this.inputXY.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 34, 222);
			this.inputXY.Name = "inputXY";
			this.inputXY.SecurityToken = ((uint)(4294967295u));
			this.inputXY.TagName = "CBF22C00599345E2";
			this.inputXY.EndInit();
			// 
			// compcyl1
			// 
			this.compcyl1.BeginInit();
			this.compcyl1.AngleIgnore = false;
			this.compcyl1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 1, 3);
			this.compcyl1.Name = "compcyl1";
			this.compcyl1.SecurityToken = ((uint)(4294967295u));
			this.compcyl1.TagName = "2E2721C1DD923968";
			this.compcyl1.EndInit();
			// 
			// VerCylinderMy
			// 
			this.VerCylinderMy.BeginInit();
			this.VerCylinderMy.AngleIgnore = false;
			this.VerCylinderMy.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 356, 3);
			this.VerCylinderMy.Name = "VerCylinderMy";
			this.VerCylinderMy.SecurityToken = ((uint)(4294967295u));
			this.VerCylinderMy.TagName = "5FF402C53450A6CA";
			this.VerCylinderMy.EndInit();
			// 
			// Composites
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(800)), ((float)(520)));
			this.Name = "Big";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.inputXY,
									this.compcyl1,
									this.VerCylinderMy});
			this.Size = new System.Drawing.Size(800, 520);
		}
		private NxtStudio.Symbols.VCylinderMySA.HMI VerCylinderMy;
		private NxtStudio.Symbols.HCylinderSA.HMI compcyl1;
		private NxtStudio.Symbols.ManControl.HMI inputXY;
		#endregion
	}
}
