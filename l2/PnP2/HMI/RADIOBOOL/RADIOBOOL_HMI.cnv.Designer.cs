/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 24/03/2012
 * Time: 7:55 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.RADIOBOOL
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btStart = new NxtControl.GuiFramework.RadioButton();
			this.btStop = new NxtControl.GuiFramework.RadioButton();
			this.LABEL1 = new NxtStudio.Symbols.Execute<string>();
			this.LABEL0 = new NxtStudio.Symbols.Execute<string>();
			this.INV = new NxtStudio.Symbols.Execute<bool>();
			this.OUT = new NxtStudio.Symbols.Execute<bool>();
			// 
			// btStart
			// 
			this.btStart.Location = new System.Drawing.Point(13, 67);
			this.btStart.Name = "btStart";
			this.btStart.Size = new System.Drawing.Size(50, 18);
			this.btStart.TabIndex = 0;
			this.btStart.CheckedChanged += new System.EventHandler(this.BtStartCheckedChanged);
			// 
			// btStop
			// 
			this.btStop.Location = new System.Drawing.Point(88, 67);
			this.btStop.Name = "btStop";
			this.btStop.Size = new System.Drawing.Size(60, 18);
			this.btStop.TabIndex = 1;
			this.btStop.CheckedChanged += new System.EventHandler(this.BtStopCheckedChanged);
			// 
			// LABEL1
			// 
			this.LABEL1.BeginInit();
			this.LABEL1.AngleIgnore = false;
			this.LABEL1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 113, 119);
			this.LABEL1.IsOnlyInput = true;
			this.LABEL1.Name = "LABEL1";
			this.LABEL1.TagName = "LABEL1";
			this.LABEL1.Value = null;
			this.LABEL1.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.LABEL1ValueChanged);
			this.LABEL1.EndInit();
			// 
			// LABEL0
			// 
			this.LABEL0.BeginInit();
			this.LABEL0.AngleIgnore = false;
			this.LABEL0.DesignTransformation = new NxtControl.Drawing.Matrix(0.76, 0, 0, 1, 112, 102);
			this.LABEL0.IsOnlyInput = true;
			this.LABEL0.Name = "LABEL0";
			this.LABEL0.TagName = "LABEL0";
			this.LABEL0.Value = null;
			this.LABEL0.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.LABEL0ValueChanged);
			this.LABEL0.EndInit();
			// 
			// INV
			// 
			this.INV.BeginInit();
			this.INV.AngleIgnore = false;
			this.INV.DesignTransformation = new NxtControl.Drawing.Matrix(0.29, 0, 0, 1, 59, 114);
			this.INV.IsOnlyInput = true;
			this.INV.Name = "INV";
			this.INV.TagName = "INV";
			this.INV.Value = false;
			this.INV.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.INVValueChanged);
			this.INV.EndInit();
			// 
			// OUT
			// 
			this.OUT.BeginInit();
			this.OUT.AngleIgnore = false;
			this.OUT.DesignTransformation = new NxtControl.Drawing.Matrix(0.44, 0, 0, 1, 61, 145);
			this.OUT.Name = "OUT";
			this.OUT.TagName = "OUT";
			this.OUT.Value = false;
			this.OUT.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.btStart,
									this.btStop,
									this.LABEL1,
									this.LABEL0,
									this.INV,
									this.OUT});
			this.SymbolSize = new System.Drawing.Size(156, 180);
		}
		private NxtStudio.Symbols.Execute<bool> OUT;
		private NxtStudio.Symbols.Execute<bool> INV;
		private NxtControl.GuiFramework.RadioButton btStart;
		private NxtControl.GuiFramework.RadioButton btStop;
		private NxtStudio.Symbols.Execute<string> LABEL0;
		private NxtStudio.Symbols.Execute<string> LABEL1;
		#endregion
	}
}
