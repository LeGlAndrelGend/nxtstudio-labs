/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 24/03/2012
 * Time: 7:55 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #RADIOBOOL_HMI;

namespace NxtStudio.Symbols.RADIOBOOL
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_LABEL1(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String LABEL1
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_LABEL0(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String LABEL0
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_INV(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,2, ref var);
      if (ret) value = (System.Boolean) var;
      return ret;
    }

    public System.Boolean? INV
    { get {
      if (accessorService == null)
        return null;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,2, ref var);
      if (!ret) return null;
      return (System.Boolean) var;
    }  }


  }

}

namespace NxtStudio.Symbols.RADIOBOOL
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.Boolean? OUT_field = null;
    public System.Boolean? OUT
    {
       get { return OUT_field; }
       set { OUT_field = value; }
    }

  }

  public class INDEventArgs : System.EventArgs
  {
    public INDEventArgs()
    {
    }
    private System.Boolean? OUT_field = null;
    public System.Boolean? OUT
    {
       get { return OUT_field; }
       set { OUT_field = value; }
    }

  }

}

namespace NxtStudio.Symbols.RADIOBOOL
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.RADIOBOOL.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new NxtStudio.Symbols.RADIOBOOL.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_IND(System.Boolean OUT)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {OUT});
    }
    public bool FireEvent_IND(NxtStudio.Symbols.RADIOBOOL.INDEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.OUT.HasValue) _values_[0] = ea.OUT.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_IND(System.Boolean OUT, bool ignore_OUT)
    {
      object[] _values_ = new object[1];
      if (!ignore_OUT) _values_[0] = OUT;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }

  }
}
#endregion #RADIOBOOL_HMI;

#endregion Definitions;
