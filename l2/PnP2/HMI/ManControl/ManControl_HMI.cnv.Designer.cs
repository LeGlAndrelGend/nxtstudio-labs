/*
 * Created by nxtStudio.
 * User: Horst Mayer
 * Date: 9/14/2008
 * Time: 11:10 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.ManControl
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.joyStick_11 = new NxtStudio.Symbols.JoyStick<ushort>();
			this.roundedRectangle1 = new NxtControl.GuiFramework.RoundedRectangle();
			this.ellipse10 = new NxtControl.GuiFramework.Ellipse();
			this.line10 = new NxtControl.GuiFramework.Line();
			this.ellipse11 = new NxtControl.GuiFramework.Ellipse();
			this.line11 = new NxtControl.GuiFramework.Line();
			this.ellipse12 = new NxtControl.GuiFramework.Ellipse();
			this.line12 = new NxtControl.GuiFramework.Line();
			this.ellipse13 = new NxtControl.GuiFramework.Ellipse();
			this.line13 = new NxtControl.GuiFramework.Line();
			// 
			// joyStick_11
			// 
			this.joyStick_11.BeginInit();
			this.joyStick_11.AngleIgnore = false;
			this.joyStick_11.Name = "joyStick_11";
			this.joyStick_11.TagName = "coord";
			this.joyStick_11.Transformation = new NxtControl.Drawing.Matrix(0.7018716577540105, 0, 0, 1.0603957293851569, 10, 15.191234596605437);
			this.joyStick_11.Value = ((ushort)(0));
			this.joyStick_11.EndInit();
			// 
			// roundedRectangle1
			// 
			this.roundedRectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(5)), ((float)(10)), ((float)(150)), ((float)(150)));
			this.roundedRectangle1.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(134)), ((byte)(154)), ((byte)(182)), ((byte)(255))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.DiagonalLeftTop, NxtControl.Drawing.GradientFillBrightness.Dark));
			this.roundedRectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.roundedRectangle1.Name = "roundedRectangle1";
			this.roundedRectangle1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.roundedRectangle1.Radius = 20;
			this.roundedRectangle1.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// ellipse10
			// 
			this.ellipse10.Bounds = new NxtControl.Drawing.RectF(((float)(14)), ((float)(15)), ((float)(14)), ((float)(12)));
			this.ellipse10.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("White"), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, NxtControl.Drawing.GradientFillBrightness.Dark));
			this.ellipse10.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ellipse10.Name = "ellipse10";
			this.ellipse10.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.ellipse10.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// line10
			// 
			this.line10.EndPoint = new NxtControl.Drawing.PointF(24, 19);
			this.line10.Name = "line10";
			this.line10.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line10.StartPoint = new NxtControl.Drawing.PointF(15, 25);
			// 
			// ellipse11
			// 
			this.ellipse11.Bounds = new NxtControl.Drawing.RectF(((float)(136)), ((float)(16)), ((float)(14)), ((float)(12)));
			this.ellipse11.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("White"), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, NxtControl.Drawing.GradientFillBrightness.Dark));
			this.ellipse11.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ellipse11.Name = "ellipse11";
			this.ellipse11.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.ellipse11.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// line11
			// 
			this.line11.EndPoint = new NxtControl.Drawing.PointF(147, 19);
			this.line11.Name = "line11";
			this.line11.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line11.StartPoint = new NxtControl.Drawing.PointF(138, 25);
			// 
			// ellipse12
			// 
			this.ellipse12.Bounds = new NxtControl.Drawing.RectF(((float)(136)), ((float)(142)), ((float)(14)), ((float)(12)));
			this.ellipse12.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("White"), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, NxtControl.Drawing.GradientFillBrightness.Dark));
			this.ellipse12.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ellipse12.Name = "ellipse12";
			this.ellipse12.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.ellipse12.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// line12
			// 
			this.line12.EndPoint = new NxtControl.Drawing.PointF(147, 146);
			this.line12.Name = "line12";
			this.line12.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line12.StartPoint = new NxtControl.Drawing.PointF(138, 152);
			// 
			// ellipse13
			// 
			this.ellipse13.Bounds = new NxtControl.Drawing.RectF(((float)(14)), ((float)(142)), ((float)(14)), ((float)(12)));
			this.ellipse13.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("White"), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center, NxtControl.Drawing.GradientFillBrightness.Dark));
			this.ellipse13.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.ellipse13.Name = "ellipse13";
			this.ellipse13.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.ellipse13.TextColor = new NxtControl.Drawing.Color("Black");
			// 
			// line13
			// 
			this.line13.EndPoint = new NxtControl.Drawing.PointF(25, 145);
			this.line13.Name = "line13";
			this.line13.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Black"), 2F, NxtControl.Drawing.DashStyle.Solid);
			this.line13.StartPoint = new NxtControl.Drawing.PointF(16, 151);
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.roundedRectangle1,
									this.joyStick_11,
									this.ellipse10,
									this.line10,
									this.ellipse11,
									this.line11,
									this.ellipse12,
									this.line12,
									this.ellipse13,
									this.line13});
		}
		private NxtControl.GuiFramework.Line line13;
		private NxtControl.GuiFramework.Ellipse ellipse13;
		private NxtControl.GuiFramework.Line line12;
		private NxtControl.GuiFramework.Ellipse ellipse12;
		private NxtControl.GuiFramework.Line line11;
		private NxtControl.GuiFramework.Ellipse ellipse11;
		private NxtControl.GuiFramework.Line line10;
		private NxtControl.GuiFramework.Ellipse ellipse10;
		private NxtControl.GuiFramework.RoundedRectangle roundedRectangle1;
		private NxtStudio.Symbols.JoyStick<ushort> joyStick_11;
		#endregion
	}
}
