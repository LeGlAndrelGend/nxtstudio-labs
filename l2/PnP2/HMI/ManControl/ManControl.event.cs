using System;
using NxtControl.GuiFramework;
using NxtControl.Services;


#region Definitions;
#region #ManControl_HMI;

namespace NxtStudio.Symbols.ManControl
{
  partial class HMI
  {
    public bool FireEvent_REQ(System.UInt16 coord)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {coord});
    }
    public bool FireEvent_REQ(System.UInt16 coord, bool ignore_coord)
    {
      object[] _values_ = new object[1];
      if (!ignore_coord) _values_[0] = coord;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #ManControl_HMI;

#endregion Definitions;

