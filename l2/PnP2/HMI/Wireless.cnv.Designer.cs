﻿/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 12/23/2012
 * Time: 5:48 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace NxtStudio.Canvases
{
	/// <summary>
	/// Summary description for Wireless.
	/// </summary>
	partial class Wireless
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.joystick = new NxtStudio.Symbols.ManControl.HMI();
			this.PushH = new NxtStudio.Symbols.iValve.HMI();
			this.PopH = new NxtStudio.Symbols.iValve.HMI();
			this.SensLeft = new NxtStudio.Symbols.Sensor.HMI();
			this.SensRight = new NxtStudio.Symbols.Sensor.HMI();
			this.PopV = new NxtStudio.Symbols.iValve.HMI();
			this.PushV = new NxtStudio.Symbols.iValve.HMI();
			this.VSensTop = new NxtStudio.Symbols.Sensor.HMI();
			this.VSensBtn = new NxtStudio.Symbols.Sensor.HMI();
			this.cyl1 = new NxtStudio.Symbols.HCylinder.HMI();
			this.cyl2 = new NxtStudio.Symbols.VCylinder.HMI();
			// 
			// joystick
			// 
			this.joystick.BeginInit();
			this.joystick.AngleIgnore = false;
			this.joystick.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 38, 272);
			this.joystick.Name = "joystick";
			this.joystick.SecurityToken = ((uint)(4294967295u));
			this.joystick.TagName = "DD25996347FEE8C6";
			this.joystick.EndInit();
			// 
			// PushH
			// 
			this.PushH.BeginInit();
			this.PushH.AngleIgnore = false;
			this.PushH.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 24, 72);
			this.PushH.Name = "PushH";
			this.PushH.SecurityToken = ((uint)(4294967295u));
			this.PushH.TagName = "969940A576A707E0";
			this.PushH.EndInit();
			// 
			// PopH
			// 
			this.PopH.BeginInit();
			this.PopH.AngleIgnore = false;
			this.PopH.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 167, 72);
			this.PopH.Name = "PopH";
			this.PopH.SecurityToken = ((uint)(4294967295u));
			this.PopH.TagName = "CF555866403C0811";
			this.PopH.EndInit();
			// 
			// SensLeft
			// 
			this.SensLeft.BeginInit();
			this.SensLeft.AngleIgnore = false;
			this.SensLeft.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 54, 233);
			this.SensLeft.Name = "SensLeft";
			this.SensLeft.SecurityToken = ((uint)(4294967295u));
			this.SensLeft.TagName = "6E49B7E94EDBD6B0";
			this.SensLeft.EndInit();
			// 
			// SensRight
			// 
			this.SensRight.BeginInit();
			this.SensRight.AngleIgnore = false;
			this.SensRight.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 168, 233);
			this.SensRight.Name = "SensRight";
			this.SensRight.SecurityToken = ((uint)(4294967295u));
			this.SensRight.TagName = "1A10F87B9BE278CB";
			this.SensRight.EndInit();
			// 
			// PopV
			// 
			this.PopV.BeginInit();
			this.PopV.AngleIgnore = false;
			this.PopV.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 554, 72);
			this.PopV.Name = "PopV";
			this.PopV.SecurityToken = ((uint)(4294967295u));
			this.PopV.TagName = "BFA679F8C82B60E0";
			this.PopV.EndInit();
			// 
			// PushV
			// 
			this.PushV.BeginInit();
			this.PushV.AngleIgnore = false;
			this.PushV.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 442, 72);
			this.PushV.Name = "PushV";
			this.PushV.SecurityToken = ((uint)(4294967295u));
			this.PushV.TagName = "83C8FF0A6DBA72FB";
			this.PushV.EndInit();
			// 
			// VSensTop
			// 
			this.VSensTop.BeginInit();
			this.VSensTop.AngleIgnore = false;
			this.VSensTop.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 412, 167);
			this.VSensTop.Name = "VSensTop";
			this.VSensTop.SecurityToken = ((uint)(4294967295u));
			this.VSensTop.TagName = "1BE7CBDCB31A08EE";
			this.VSensTop.EndInit();
			// 
			// VSensBtn
			// 
			this.VSensBtn.BeginInit();
			this.VSensBtn.AngleIgnore = false;
			this.VSensBtn.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 412, 281);
			this.VSensBtn.Name = "VSensBtn";
			this.VSensBtn.SecurityToken = ((uint)(4294967295u));
			this.VSensBtn.TagName = "CE209ED62FE04C81";
			this.VSensBtn.EndInit();
			// 
			// cyl1
			// 
			this.cyl1.BeginInit();
			this.cyl1.AngleIgnore = false;
			this.cyl1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 34, 101);
			this.cyl1.LabelName = "Name";
			this.cyl1.Name = "cyl1";
			this.cyl1.SecurityToken = ((uint)(4294967295u));
			this.cyl1.TagName = "F2CA7CE8D5AC8B3F";
			this.cyl1.EndInit();
			// 
			// cyl2
			// 
			this.cyl2.BeginInit();
			this.cyl2.AngleIgnore = false;
			this.cyl2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 437, 102);
			this.cyl2.LabelName = "Name";
			this.cyl2.Name = "cyl2";
			this.cyl2.SecurityToken = ((uint)(4294967295u));
			this.cyl2.TagName = "C9A92F3A07AFF169";
			this.cyl2.EndInit();
			// 
			// Wireless
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(800)), ((float)(520)));
			this.Name = "Wireless";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.joystick,
									this.PushH,
									this.PopH,
									this.SensLeft,
									this.SensRight,
									this.VSensBtn,
									this.VSensTop,
									this.PushV,
									this.PopV,
									this.cyl1,
									this.cyl2});
			this.Size = new System.Drawing.Size(800, 520);
		}
		private NxtStudio.Symbols.VCylinder.HMI cyl2;
		private NxtStudio.Symbols.HCylinder.HMI cyl1;
		private NxtStudio.Symbols.iValve.HMI PopV;
		private NxtStudio.Symbols.iValve.HMI PushV;
		private NxtStudio.Symbols.Sensor.HMI VSensTop;
		private NxtStudio.Symbols.Sensor.HMI VSensBtn;
		private NxtStudio.Symbols.Sensor.HMI SensRight;
		private NxtStudio.Symbols.Sensor.HMI SensLeft;
		private NxtStudio.Symbols.iValve.HMI PushH;
		private NxtStudio.Symbols.iValve.HMI PopH;
		private NxtStudio.Symbols.ManControl.HMI joystick;
		#endregion
	}
}
