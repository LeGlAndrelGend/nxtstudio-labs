﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/24/2017
 * Time: 11:18 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace NxtStudio.Canvases
{
	/// <summary>
	/// Summary description for Canvas1.
	/// </summary>
	partial class Canvas1
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.VerCylinderMy = new NxtStudio.Symbols.VCylinderMySA.HMI();
			this.VerCylinderMy_1 = new NxtStudio.Symbols.VCylinderMySA.HMI();
			this.cyl1 = new NxtStudio.Symbols.HCylinder.HMI();
			this.joystick = new NxtStudio.Symbols.ManControl.HMI();
			this.VerCylinderMy_2 = new NxtStudio.Symbols.VCylinderMySA.HMI();
			this.SensRight = new NxtStudio.Symbols.Sensor.HMI();
			this.VSensTop = new NxtStudio.Symbols.Sensor.HMI();
			this.WPX = new NxtStudio.Symbols.BUTTON.HMI();
			this.WPY = new NxtStudio.Symbols.BUTTON.HMI();
			// 
			// VerCylinderMy
			// 
			this.VerCylinderMy.BeginInit();
			this.VerCylinderMy.AngleIgnore = false;
			this.VerCylinderMy.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 675, 395);
			this.VerCylinderMy.Name = "VerCylinderMy";
			this.VerCylinderMy.SecurityToken = ((uint)(4294967295u));
			this.VerCylinderMy.TagName = "5FF402C53450A6CA";
			this.VerCylinderMy.EndInit();
			// 
			// VerCylinderMy_1
			// 
			this.VerCylinderMy_1.BeginInit();
			this.VerCylinderMy_1.AngleIgnore = false;
			this.VerCylinderMy_1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 600, 402);
			this.VerCylinderMy_1.Name = "VerCylinderMy_1";
			this.VerCylinderMy_1.SecurityToken = ((uint)(4294967295u));
			this.VerCylinderMy_1.TagName = "5FF402C53450A6CA";
			this.VerCylinderMy_1.EndInit();
			// 
			// cyl1
			// 
			this.cyl1.BeginInit();
			this.cyl1.AngleIgnore = false;
			this.cyl1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 630, 250);
			this.cyl1.LabelName = "Name";
			this.cyl1.Name = "cyl1";
			this.cyl1.SecurityToken = ((uint)(4294967295u));
			this.cyl1.TagName = "F2CA7CE8D5AC8B3F";
			this.cyl1.EndInit();
			// 
			// joystick
			// 
			this.joystick.BeginInit();
			this.joystick.AngleIgnore = false;
			this.joystick.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 708, 510);
			this.joystick.Name = "joystick";
			this.joystick.SecurityToken = ((uint)(4294967295u));
			this.joystick.TagName = "DD25996347FEE8C6";
			this.joystick.EndInit();
			// 
			// VerCylinderMy_2
			// 
			this.VerCylinderMy_2.BeginInit();
			this.VerCylinderMy_2.AngleIgnore = false;
			this.VerCylinderMy_2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 497, 530);
			this.VerCylinderMy_2.Name = "VerCylinderMy_2";
			this.VerCylinderMy_2.SecurityToken = ((uint)(4294967295u));
			this.VerCylinderMy_2.TagName = "5FF402C53450A6CA";
			this.VerCylinderMy_2.EndInit();
			// 
			// SensRight
			// 
			this.SensRight.BeginInit();
			this.SensRight.AngleIgnore = false;
			this.SensRight.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 478, 422);
			this.SensRight.Name = "SensRight";
			this.SensRight.SecurityToken = ((uint)(4294967295u));
			this.SensRight.TagName = "1A10F87B9BE278CB";
			this.SensRight.EndInit();
			// 
			// VSensTop
			// 
			this.VSensTop.BeginInit();
			this.VSensTop.AngleIgnore = false;
			this.VSensTop.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 502, 510);
			this.VSensTop.Name = "VSensTop";
			this.VSensTop.SecurityToken = ((uint)(4294967295u));
			this.VSensTop.TagName = "1BE7CBDCB31A08EE";
			this.VSensTop.EndInit();
			// 
			// WPX
			// 
			this.WPX.BeginInit();
			this.WPX.AngleIgnore = false;
			this.WPX.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 475, 595);
			this.WPX.Name = "WPX";
			this.WPX.SecurityToken = ((uint)(4294967295u));
			this.WPX.TagName = "E45CC0D3A0FF2129";
			this.WPX.EndInit();
			// 
			// WPY
			// 
			this.WPY.BeginInit();
			this.WPY.AngleIgnore = false;
			this.WPY.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 1003, 285);
			this.WPY.Name = "WPY";
			this.WPY.SecurityToken = ((uint)(4294967295u));
			this.WPY.TagName = "61930F36E0F7A7DE";
			this.WPY.EndInit();
			// 
			// Canvas1
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1280)), ((float)(900)));
			this.Name = "Canvas1";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.VerCylinderMy,
									this.VerCylinderMy_1,
									this.cyl1,
									this.joystick,
									this.VerCylinderMy_2,
									this.SensRight,
									this.VSensTop,
									this.WPX,
									this.WPY});
			this.Size = new System.Drawing.Size(1280, 900);
		}
		private NxtStudio.Symbols.BUTTON.HMI WPY;
		private NxtStudio.Symbols.BUTTON.HMI WPX;
		private NxtStudio.Symbols.Sensor.HMI VSensTop;
		private NxtStudio.Symbols.Sensor.HMI SensRight;
		private NxtStudio.Symbols.VCylinderMySA.HMI VerCylinderMy_2;
		private NxtStudio.Symbols.ManControl.HMI joystick;
		private NxtStudio.Symbols.HCylinder.HMI cyl1;
		private NxtStudio.Symbols.VCylinderMySA.HMI VerCylinderMy_1;
		private NxtStudio.Symbols.VCylinderMySA.HMI VerCylinderMy;
		#endregion
	}
}
