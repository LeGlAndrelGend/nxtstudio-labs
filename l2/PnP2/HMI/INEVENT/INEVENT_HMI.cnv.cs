/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 16/11/2012
 * Time: 4:54 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INEVENT
{
	//private bool Pressed; 
  /// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
	  public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}

		
		void LABELValueChanged(object sender, ValueChangedEventArgs e)
		{
		  button1.Text=(string)LABEL.Value;			
		  FireEvent_IND();		  
		  
		}

		
		void Button1EnabledChanged(object sender, EventArgs e)
		{
		  //this.FireEvent_IND(1);
		}
		
		void Button1Click(object sender, EventArgs e)
		{
		  this.FireEvent_IND();
		}
	}
}
