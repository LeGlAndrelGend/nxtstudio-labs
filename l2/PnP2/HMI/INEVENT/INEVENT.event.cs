/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 16/11/2012
 * Time: 4:54 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #INEVENT_HMI;

namespace NxtStudio.Symbols.INEVENT
{

  public class INDEventArgs : System.EventArgs
  {
    public INDEventArgs()
    {
    }

  }

}

namespace NxtStudio.Symbols.INEVENT
{
  partial class HMI
  {
    public bool FireEvent_IND()
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {});
    }
    public bool FireEvent_IND(NxtStudio.Symbols.INEVENT.INDEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #INEVENT_HMI;

#endregion Definitions;
