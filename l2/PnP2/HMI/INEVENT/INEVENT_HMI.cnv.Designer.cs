/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 16/11/2012
 * Time: 4:54 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INEVENT
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new NxtControl.GuiFramework.Button();
			this.LABEL = new NxtStudio.Symbols.Execute<string>();
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.No;
			this.button1.Location = new System.Drawing.Point(30, 57);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(59, 23);
			this.button1.TabIndex = 0;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// LABEL
			// 
			this.LABEL.BeginInit();
			this.LABEL.AngleIgnore = false;
			this.LABEL.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 62, 67);
			this.LABEL.IsOnlyInput = true;
			this.LABEL.Name = "LABEL";
			this.LABEL.TagName = "LABEL";
			this.LABEL.Value = null;
			this.LABEL.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.LABELValueChanged);
			this.LABEL.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.button1,
									this.LABEL});
			this.SymbolSize = new System.Drawing.Size(127, 153);
		}
		private NxtStudio.Symbols.Execute<string> LABEL;
		private NxtControl.GuiFramework.Button button1;
		#endregion
	}
}
