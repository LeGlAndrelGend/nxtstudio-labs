<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE AdapterType SYSTEM "../LibraryElement.dtd">
<AdapterType Name="IO" Comment="Adapter Interface" Namespace="Main">
  <Identification Standard="61499-1" />
  <VersionInfo Organization="nxtControl GmbH" Version="0.0" Author="valvya" Date="2/19/2013" />
  <InterfaceList>
    <EventInputs>
      <Event Name="REQ" Comment="Request from Socket">
        <With Var="SENS1" />
      </Event>
      <Event Name="RSP" Comment="Response from Socket">
        <With Var="SENS2" />
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Name="CNF" Comment="Confirmation from Plug">
        <With Var="ACT1" />
      </Event>
      <Event Name="IND" Comment="Indication from Plug">
        <With Var="ACT2" />
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Name="SENS1" Type="BOOL" Comment="Request Data from Socket" />
      <VarDeclaration Name="SENS2" Type="BOOL" Comment="Response Data from Socket" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="ACT1" Type="BOOL" Comment="Confirmation Data from Plug" />
      <VarDeclaration Name="ACT2" Type="BOOL" Comment="Indication Data from Plug" />
    </OutputVars>
  </InterfaceList>
  <Service RightInterface="RESOURCE" LeftInterface="SOCKET">
    <ServiceSequence Name="request_confirm">
      <ServiceTransaction>
        <InputPrimitive Interface="PLUG" Event="CNF" Parameters="CNFD" />
        <OutputPrimitive Interface="SOCKET" Event="CNF" Parameters="CNFD" />
      </ServiceTransaction>
      <ServiceTransaction>
        <InputPrimitive Interface="SOCKET" Event="REQ" Parameters="REQD" />
        <OutputPrimitive Interface="PLUG" Event="REQ" Parameters="REQD" />
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="indication_response">
      <ServiceTransaction>
        <InputPrimitive Interface="SOCKET" Event="RSP" Parameters="RSPD" />
        <OutputPrimitive Interface="PLUG" Event="RSP" Parameters="RSPD" />
      </ServiceTransaction>
      <ServiceTransaction>
        <InputPrimitive Interface="PLUG" Event="IND" Parameters="INDD" />
        <OutputPrimitive Interface="SOCKET" Event="IND" Parameters="INDD" />
      </ServiceTransaction>
    </ServiceSequence>
  </Service>
</AdapterType>