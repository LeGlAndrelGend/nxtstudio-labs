﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/27/2017
 * Time: 7:27 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for LFSerRoomHMI.
	/// </summary>
	partial class LFSerRoomHMI
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LFSerRoomInst = new HMI.Main.Symbols.LFSerRoomFB.sDefault();
			// 
			// LFSerRoomInst
			// 
			this.LFSerRoomInst.BeginInit();
			this.LFSerRoomInst.AngleIgnore = false;
			this.LFSerRoomInst.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 1, 0);
			this.LFSerRoomInst.Name = "LFSerRoomInst";
			this.LFSerRoomInst.SecurityToken = ((uint)(4294967295u));
			this.LFSerRoomInst.TagName = "AABEA623FED4D995";
			this.LFSerRoomInst.EndInit();
			// 
			// LFSerRoomHMI
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1280)), ((float)(900)));
			this.Name = "LFSerRoomHMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.LFSerRoomInst});
			this.Size = new System.Drawing.Size(1280, 900);
			this.Title = "LFSerRoomHMI";
		}
		private HMI.Main.Symbols.LFSerRoomFB.sDefault LFSerRoomInst;
		#endregion
	}
}
