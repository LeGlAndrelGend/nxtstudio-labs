﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/27/2017
 * Time: 1:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for SR_HMI_new.
	/// </summary>
	partial class SR_HMI_new
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SRFBInst = new HMI.Main.Symbols.ServerRoomFBMy.sDefault();
			// 
			// SRFBInst
			// 
			this.SRFBInst.BeginInit();
			this.SRFBInst.AngleIgnore = false;
			this.SRFBInst.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, -1, 0);
			this.SRFBInst.Name = "SRFBInst";
			this.SRFBInst.SecurityToken = ((uint)(4294967295u));
			this.SRFBInst.TagName = "59ACF8C400B61B7F";
			this.SRFBInst.EndInit();
			// 
			// SR_HMI_new
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1000)), ((float)(550)));
			this.Name = "SR_HMI_new";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.SRFBInst});
			this.Size = new System.Drawing.Size(1000, 550);
			this.Title = "SR_HMI_new";
		}
		private HMI.Main.Symbols.ServerRoomFBMy.sDefault SRFBInst;
		#endregion
	}
}
