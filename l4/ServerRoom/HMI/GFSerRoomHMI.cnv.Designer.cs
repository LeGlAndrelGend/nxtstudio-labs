﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/27/2017
 * Time: 7:37 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for GFSerRoomHMI.
	/// </summary>
	partial class GFSerRoomHMI
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.GFSerRoomInst = new HMI.Main.Symbols.GFSerRoomFB.sDefault();
			// 
			// GFSerRoomInst
			// 
			this.GFSerRoomInst.BeginInit();
			this.GFSerRoomInst.AngleIgnore = false;
			this.GFSerRoomInst.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, -1, -1);
			this.GFSerRoomInst.Name = "GFSerRoomInst";
			this.GFSerRoomInst.SecurityToken = ((uint)(4294967295u));
			this.GFSerRoomInst.TagName = "48DB9D442F657E41";
			this.GFSerRoomInst.EndInit();
			// 
			// GFSerRoomHMI
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1280)), ((float)(900)));
			this.Name = "GFSerRoomHMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.GFSerRoomInst});
			this.Size = new System.Drawing.Size(1280, 900);
			this.Title = "GFSerRoomHMI";
		}
		private HMI.Main.Symbols.GFSerRoomFB.sDefault GFSerRoomInst;
		#endregion
	}
}
