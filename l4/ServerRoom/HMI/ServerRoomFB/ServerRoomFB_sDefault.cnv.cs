﻿/*
 * Created by nxtSTUDIO.
 * User: arader
 * Date: 2/6/2014
 * Time: 3:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace HMI.Main.Symbols.ServerRoomFB
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
	  
	  int LF_State = 0;
	  int GF_State = 0;
	  
	  int LF_EC_Total = 0;
	  int GF_EC_Total = 0;
	  int C1 = 3;
    int C2 = 5;    	
    
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
      this.InitServerRoom_Fired +=new EventHandler <ServerRoomFB.InitServerRoomEventArgs>(InitServerRoom);
			this.Receive_Fired += new EventHandler <ServerRoomFB.ReceiveEventArgs> (Receive);
			
			
		}
		
		public void InitServerRoom(object Sender, ServerRoomFB.InitServerRoomEventArgs sr){
  
		}		
		
		
  public void Receive(object Sender, ServerRoomFB.ReceiveEventArgs r){	
		  
	}

		
/*-----------------ON and OFF Buttons for Local and Global Fans -----------------------*/
/*-------------------------------------------------------------------------------------*/		
		
/* Function to Turn the Local Fan On*/		
	void LF_ON_BClick(object sender, EventArgs e)
		{
		    this.FireEvent_LF_ON("LF_ON");
		    LF_State = 1;
		    rect_LF.Visible = false;
		    rect_LFMove.Visible = true;
		    LF_ON_OFF.Text = "ON";
        LF_LightON.Visible = true;
        LF_LightOFF.Visible = false;
        LF_ON_B.Enabled = false;
        LF_OFF_B.Enabled = true;
		}
/* Function to Turn the Global Fan Off*/			
		void LF_OFF_BClick(object sender, EventArgs e)
		{
         this.FireEvent_LF_OFF("LF_OFF");
		     LF_State = 0;
		     rect_LF.Visible = true;
		     rect_LFMove.Visible = false;
		     LF_ON_OFF.Text = "OFF";
         LF_LightON.Visible = false;
         LF_LightOFF.Visible = true;        	     
         LF_ON_B.Enabled = true;
         LF_OFF_B.Enabled = false;		  
		}

/* Function to Turn the Global Fan On*/	
		void GF_ON_BClick(object sender, EventArgs e)
		{
        this.FireEvent_LF_ON("GF_ON");
		    GF_State = 1;			
		    rect_GF.Visible = false;
		    rect_GFMove.Visible = true;
		    GF_ON_OFF.Text = "ON";
		    GF_LightON.Visible = true; 
		    GF_LightOFF.Visible = false; 
		    GF_ON_B.Enabled = false;
        GF_OFF_B.Enabled = true;
		}
		
/* Function to Turn the Global Fan OFF*/			
		void GF_OFF_BClick(object sender, EventArgs e)
		{
         this.FireEvent_LF_OFF("GF_OFF");
		     GF_State = 0;
		     rect_GF.Visible = true;
		     rect_GFMove.Visible = false;
		     GF_ON_OFF.Text = "OFF";
		     GF_LightON.Visible = false;
		     GF_LightOFF.Visible = true;     
         GF_ON_B.Enabled = true;
         GF_OFF_B.Enabled = false;		      
		}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/		
		
/*-----------------Working with Tracker------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
				
		
/* THIS METHOD RESPONDS TO CHANGES ON TRACKER WHICH IS ´CONNECTED TO A CPU
 WHEN THE TRACKER IS SLIDE TO THE RIGHT AND EXCEEDS THE MAXIMUM LIMIT OF THE CPU TEMPERATURE
 ITS CORRESPONDING LOCAL FAN WILL BE ON TO REDUCE ITS TEMPERATURE
 AND TRACKER WILL SLIDE TO LEFT UNTIL CPU TEMPERATURE REACHES ITS MINIMUM LIMIT	*/
		void T_CPULoadValueChanged(object sender, EventArgs e)
		{

		  CPULoad_Text.Text = t_CPULoad.Value.ToString();
		  
		  if (t_CPULoad.Value > double.Parse(CPUTMax.Text) && LF_State.Equals(0)){
		    
		    // Task 4.2 complete 
		    // This is extension for add constant value "C1" to global variable "LF_EC_Total"
		    // when local fan in turned on by LF tracker 
		    LF_EC_Total += C1;
		    
		    this.FireEvent_LF_ON("LF_ON");
		    LF_State = 1;
		    rect_LF.Visible = false;
		    rect_LFMove.Visible = true;
		    LF_ON_OFF.Text = "ON";
        LF_LightON.Visible = true;
        LF_LightOFF.Visible = false;
        Thread threadCPU = new Thread(new ThreadStart(moveCPULoad));
        threadCPU.Start();
	    } 
		  if (t_CPULoad.Value == double.Parse(CPUTMin.Text) && LF_State.Equals(1)){
         this.FireEvent_LF_OFF("LF_OFF");
		     LF_State = 0;
		     rect_LF.Visible = true;
		     rect_LFMove.Visible = false;
		     LF_ON_OFF.Text = "OFF";
         LF_LightON.Visible = false;
         LF_LightOFF.Visible = true;        	     
 
		   }
		}
/* THIS METHOD RESPONDS TO CHANGES ON TRACKER WHICH IS ´CONNECTED TO THE ROOM
 WHEN THE TRACKER IS SLIDE TO THE RIGHT AND EXCEEDS THE MAXIMUM LIMIT OF THE ROOM TEMPERATURE
 CORRESPONDING TO THE GLOBAL FAN WILL BE ON TO REDUCE ITS TEMPERATURE
 AND TRACKER WILL SLIDE TO LEFT UNTIL ROOM TEMPERATURE REACHES ITS MINIMUM LIMIT	*/			
		void T_RoomTempValueChanged(object sender, EventArgs e)
		{ 
		  RoomTemp_Text.Text = t_RoomTemp.Value.ToString();
		  
		  if (t_RoomTemp.Value > double.Parse(RTMax.Text) && GF_State.Equals(0)){
		    
		    // Task 4.3 complete 
		    // This is extension for add constant value "C2" to global variable "GF_EC_Total"
		    // when global fan in turned on by GF tracker 
		    GF_EC_Total += C2;
		    
		    this.FireEvent_GF_ON("GF_ON");
		    GF_State = 1;
		    rect_GF.Visible = false;
		    rect_GFMove.Visible = true;
		    GF_ON_OFF.Text = "ON";
		    GF_LightON.Visible = true;
		    GF_LightOFF.Visible = false; 
        Thread threadRoom = new Thread(new ThreadStart(moveRoomTemp));
        threadRoom.Start();
	    } 
		  if (t_RoomTemp.Value == double.Parse(RTMin.Text) && GF_State.Equals(1)){
         this.FireEvent_GF_OFF("GF_OFF");
		     GF_State = 0;
		     rect_GF.Visible = true;
		     rect_GFMove.Visible = false;
		     GF_ON_OFF.Text = "OFF";
		     GF_LightON.Visible = false;
		     GF_LightOFF.Visible = true;            
		   }		    
		}
		
// Function for Run Button			
		void RunClick(object sender, EventArgs e)
		{
			t_RoomTemp.Value =  double.Parse(RoomTemp_Text.Text);
		  t_CPULoad.Value = double.Parse (CPULoad_Text.Text);
		}
		

		public void moveCPULoad ()
		{
		  try
		  {	  
			  string[] parts = CPULoad_Text.Text.Split('.'); 
        int y = int.Parse(parts[0]);
        		  
			  t_CPULoad.Value = Convert.ToDouble(y);
        Thread.Sleep(500);
			  for (int i=y;i>=Convert.ToInt32(double.Parse(CPUTMin.Text));i--)
			  {
			    t_CPULoad.Value = Convert.ToDouble(i);       
          Thread.Sleep(100);
			  }
		  }catch (Exception ex){
		    
		  }
		}

		public void moveRoomTemp ()
		{
		  try
		  {
		  
			  string[] parts = RoomTemp_Text.Text.Split('.'); 
        int y = int.Parse(parts[0]);
        Thread.Sleep(500);
		
			  for (int i=y;i>=Convert.ToInt32(double.Parse(RTMin.Text));i--)
			  {
			    t_RoomTemp.Value = Convert.ToDouble(i);
          Thread.Sleep(300);
			  }
		  }catch (Exception ex){
		    
		  }
		}	
		
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/		

/*------------ Reseting the Control Strategy Setup to its Default Value ----------- ---*/			
/*-------------------------------------------------------------------------------------*/
		void ReSetToDefaultClick(object sender, EventArgs e)
		{
			GFSpeed.Text = "2";
			LFSpeed.Text = "2";
			RTMin.Text = "10";
			RTMax.Text = "30";
			CPUTMin.Text = "10";
			CPUTMax.Text = "50";			
		}		

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/	
	
/*------------ Creating Energy Report and Reseting the Text Boxes ---------------------*/			
/*-------------------------------------------------------------------------------------*/	
		void ReportClick(object sender, EventArgs e)
		{
			//Task4.4 complete : THIS METHOD is DISPLAY ENERGY CONSUMPTIONS IN APPROPRIATE TEXT BOX
      LF_Total_EC.Text = LF_EC_Total.ToString();
      GF_Total_EC.Text = GF_EC_Total.ToString();
      Total_EC_All.Text = (LF_EC_Total + GF_EC_Total).ToString();
		}
		void ResetClick(object sender, EventArgs e)
		{
		  t_RoomTemp.Value =  double.Parse(RTMin.Text);
		  RoomTemp_Text.Text = RTMin.Text;
		  t_CPULoad.Value = double.Parse (CPUTMin.Text);
		  CPULoad_Text.Text = CPUTMin.Text;
		  //ReportLog_listBox.Items.Clear();
		  LF_Total_EC.Clear();
		  GF_Total_EC.Clear();
		  Total_EC_All.Clear();
		}				  

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/	

	}
}
