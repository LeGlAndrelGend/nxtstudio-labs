﻿/*
 * Created by nxtSTUDIO.
 * User: arader
 * Date: 2/6/2014
 * Time: 3:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.ServerRoomFB
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.t_CPULoad = new NxtControl.GuiFramework.Tracker();
			this.CPULoad_Text = new NxtControl.GuiFramework.TextBox();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.freeText2 = new NxtControl.GuiFramework.FreeText();
			this.t_RoomTemp = new NxtControl.GuiFramework.Tracker();
			this.freeText3 = new NxtControl.GuiFramework.FreeText();
			this.RoomTemp_Text = new NxtControl.GuiFramework.TextBox();
			this.freeText4 = new NxtControl.GuiFramework.FreeText();
			this.rect_GF = new NxtControl.GuiFramework.Rectangle();
			this.rect_LF = new NxtControl.GuiFramework.Rectangle();
			this.rect_LFMove = new NxtControl.GuiFramework.Rectangle();
			this.rect_GFMove = new NxtControl.GuiFramework.Rectangle();
			this.freeText5 = new NxtControl.GuiFramework.FreeText();
			this.freeText6 = new NxtControl.GuiFramework.FreeText();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.rect_CPU = new NxtControl.GuiFramework.Rectangle();
			this.freeText8 = new NxtControl.GuiFramework.FreeText();
			this.LF_LightOFF = new NxtControl.GuiFramework.Ellipse();
			this.LF_ON_OFF = new NxtControl.GuiFramework.FreeText();
			this.line1 = new NxtControl.GuiFramework.Line();
			this.freeText9 = new NxtControl.GuiFramework.FreeText();
			this.freeText10 = new NxtControl.GuiFramework.FreeText();
			this.freeText11 = new NxtControl.GuiFramework.FreeText();
			this.freeText12 = new NxtControl.GuiFramework.FreeText();
			this.rectangle2 = new NxtControl.GuiFramework.Rectangle();
			this.GF_LightOFF = new NxtControl.GuiFramework.Ellipse();
			this.GF_ON_OFF = new NxtControl.GuiFramework.FreeText();
			this.freeText14 = new NxtControl.GuiFramework.FreeText();
			this.rectangle3 = new NxtControl.GuiFramework.Rectangle();
			this.LF_LightON = new NxtControl.GuiFramework.Ellipse();
			this.GF_LightON = new NxtControl.GuiFramework.Ellipse();
			this.freeText13 = new NxtControl.GuiFramework.FreeText();
			this.LF_Total_EC = new NxtControl.GuiFramework.TextBox();
			this.freeText15 = new NxtControl.GuiFramework.FreeText();
			this.GF_Total_EC = new NxtControl.GuiFramework.TextBox();
			this.freeText16 = new NxtControl.GuiFramework.FreeText();
			this.Total_EC_All = new NxtControl.GuiFramework.TextBox();
			this.freeText17 = new NxtControl.GuiFramework.FreeText();
			this.Reset = new NxtControl.GuiFramework.DrawnButton();
			this.Report = new NxtControl.GuiFramework.DrawnButton();
			this.freeText18 = new NxtControl.GuiFramework.FreeText();
			this.rectangle6 = new NxtControl.GuiFramework.Rectangle();
			this.freeText19 = new NxtControl.GuiFramework.FreeText();
			this.freeText20 = new NxtControl.GuiFramework.FreeText();
			this.freeText21 = new NxtControl.GuiFramework.FreeText();
			this.freeText22 = new NxtControl.GuiFramework.FreeText();
			this.RTMin = new NxtControl.GuiFramework.TextBox();
			this.freeText23 = new NxtControl.GuiFramework.FreeText();
			this.RTMax = new NxtControl.GuiFramework.TextBox();
			this.freeText24 = new NxtControl.GuiFramework.FreeText();
			this.CPUTMin = new NxtControl.GuiFramework.TextBox();
			this.freeText25 = new NxtControl.GuiFramework.FreeText();
			this.CPUTMax = new NxtControl.GuiFramework.TextBox();
			this.freeText26 = new NxtControl.GuiFramework.FreeText();
			this.radioButton1 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton2 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton3 = new NxtControl.GuiFramework.RadioButton();
			this.reSetToDefault = new NxtControl.GuiFramework.DrawnButton();
			this.freeText27 = new NxtControl.GuiFramework.FreeText();
			this.freeText28 = new NxtControl.GuiFramework.FreeText();
			this.freeText29 = new NxtControl.GuiFramework.FreeText();
			this.freeText30 = new NxtControl.GuiFramework.FreeText();
			this.LFSpeed = new NxtControl.GuiFramework.ComboBox();
			this.GFSpeed = new NxtControl.GuiFramework.ComboBox();
			this.Run = new NxtControl.GuiFramework.DrawnButton();
			this.Total_EC = new NxtControl.GuiFramework.Rectangle();
			this.LF_ON_B = new NxtControl.GuiFramework.DrawnButton();
			this.LF_OFF_B = new NxtControl.GuiFramework.DrawnButton();
			this.GF_ON_B = new NxtControl.GuiFramework.DrawnButton();
			this.GF_OFF_B = new NxtControl.GuiFramework.DrawnButton();
			this.freeText7 = new NxtControl.GuiFramework.FreeText();
			// 
			// t_CPULoad
			// 
			this.t_CPULoad.BeginInit();
			this.t_CPULoad.Bounds = new NxtControl.Drawing.RectF(((float)(47)), ((float)(515)), ((float)(240)), ((float)(65)));
			this.t_CPULoad.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.t_CPULoad.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_CPULoad.Maximum = 100;
			this.t_CPULoad.Minimum = 0;
			this.t_CPULoad.Name = "t_CPULoad";
			this.t_CPULoad.Orientation = System.Windows.Forms.Orientation.Horizontal;
			this.t_CPULoad.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.TrackHandleBrush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(139))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center));
			this.t_CPULoad.TrackHandlePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.TrackLinePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.ValueFont = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_CPULoad.ValueChanged += new System.EventHandler(this.T_CPULoadValueChanged);
			this.t_CPULoad.EndInit();
			// 
			// CPULoad_Text
			// 
			this.CPULoad_Text.Location = new System.Drawing.Point(306, 535);
			this.CPULoad_Text.Name = "CPULoad_Text";
			this.CPULoad_Text.Size = new System.Drawing.Size(87, 20);
			this.CPULoad_Text.TabIndex = 0;
			this.CPULoad_Text.Text = "10";
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText1.Location = new NxtControl.Drawing.PointF(50, 491);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Change CPU Temprature";
			// 
			// freeText2
			// 
			this.freeText2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText2.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText2.Location = new NxtControl.Drawing.PointF(301, 514);
			this.freeText2.Name = "freeText2";
			this.freeText2.Text = "CPU Temp";
			// 
			// t_RoomTemp
			// 
			this.t_RoomTemp.BeginInit();
			this.t_RoomTemp.Bounds = new NxtControl.Drawing.RectF(((float)(48)), ((float)(628)), ((float)(238)), ((float)(59)));
			this.t_RoomTemp.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.t_RoomTemp.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_RoomTemp.Maximum = 50;
			this.t_RoomTemp.Minimum = 0;
			this.t_RoomTemp.Name = "t_RoomTemp";
			this.t_RoomTemp.Orientation = System.Windows.Forms.Orientation.Horizontal;
			this.t_RoomTemp.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.TrackHandleBrush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(139))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center));
			this.t_RoomTemp.TrackHandlePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.TrackLinePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.ValueFont = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_RoomTemp.ValueChanged += new System.EventHandler(this.T_RoomTempValueChanged);
			this.t_RoomTemp.EndInit();
			// 
			// freeText3
			// 
			this.freeText3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText3.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText3.Location = new NxtControl.Drawing.PointF(46, 600);
			this.freeText3.Name = "freeText3";
			this.freeText3.Text = "Change Room Temprature";
			// 
			// RoomTemp_Text
			// 
			this.RoomTemp_Text.Location = new System.Drawing.Point(305, 648);
			this.RoomTemp_Text.Name = "RoomTemp_Text";
			this.RoomTemp_Text.Size = new System.Drawing.Size(88, 20);
			this.RoomTemp_Text.TabIndex = 0;
			this.RoomTemp_Text.Text = "10";
			// 
			// freeText4
			// 
			this.freeText4.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText4.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText4.Location = new NxtControl.Drawing.PointF(300, 628);
			this.freeText4.Name = "freeText4";
			this.freeText4.Text = "Room Temp";
			// 
			// rect_GF
			// 
			this.rect_GF.Bounds = new NxtControl.Drawing.RectF(((float)(438)), ((float)(79)), ((float)(149)), ((float)(161)));
			this.rect_GF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_GF.ImageName = "HMI:Images.GF";
			this.rect_GF.Name = "rect_GF";
			// 
			// rect_LF
			// 
			this.rect_LF.Bounds = new NxtControl.Drawing.RectF(((float)(51)), ((float)(110)), ((float)(84)), ((float)(65)));
			this.rect_LF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_LF.ImageName = "HMI:Images.LF";
			this.rect_LF.Name = "rect_LF";
			// 
			// rect_LFMove
			// 
			this.rect_LFMove.Bounds = new NxtControl.Drawing.RectF(((float)(50)), ((float)(110)), ((float)(84)), ((float)(65)));
			this.rect_LFMove.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_LFMove.ImageName = "HMI:Images.LF-Move";
			this.rect_LFMove.Name = "rect_LFMove";
			this.rect_LFMove.Visible = false;
			// 
			// rect_GFMove
			// 
			this.rect_GFMove.Bounds = new NxtControl.Drawing.RectF(((float)(438)), ((float)(79)), ((float)(149)), ((float)(161)));
			this.rect_GFMove.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_GFMove.ImageName = "HMI:Images.GF-Move";
			this.rect_GFMove.Name = "rect_GFMove";
			this.rect_GFMove.Visible = false;
			// 
			// freeText5
			// 
			this.freeText5.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText5.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText5.Location = new NxtControl.Drawing.PointF(47, 86);
			this.freeText5.Name = "freeText5";
			this.freeText5.Text = "Local Fan";
			// 
			// freeText6
			// 
			this.freeText6.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText6.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText6.Location = new NxtControl.Drawing.PointF(466, 56);
			this.freeText6.Name = "freeText6";
			this.freeText6.Text = "Global Fan";
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(35)), ((float)(79)), ((float)(371)), ((float)(160)));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			// 
			// rect_CPU
			// 
			this.rect_CPU.Bounds = new NxtControl.Drawing.RectF(((float)(171)), ((float)(110)), ((float)(224)), ((float)(60)));
			this.rect_CPU.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_CPU.ImageName = "HMI:Images.CPU";
			this.rect_CPU.Name = "rect_CPU";
			// 
			// freeText8
			// 
			this.freeText8.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText8.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText8.Location = new NxtControl.Drawing.PointF(244, 85);
			this.freeText8.Name = "freeText8";
			this.freeText8.Text = "CPU";
			// 
			// LF_LightOFF
			// 
			this.LF_LightOFF.Bounds = new NxtControl.Drawing.RectF(((float)(112)), ((float)(179)), ((float)(22)), ((float)(20)));
			this.LF_LightOFF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.LF_LightOFF.ImageName = "HMI:Images.Light-OFF";
			this.LF_LightOFF.Name = "LF_LightOFF";
			// 
			// LF_ON_OFF
			// 
			this.LF_ON_OFF.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.LF_ON_OFF.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_ON_OFF.Location = new NxtControl.Drawing.PointF(45, 181);
			this.LF_ON_OFF.Name = "LF_ON_OFF";
			this.LF_ON_OFF.Text = "OFF";
			// 
			// line1
			// 
			this.line1.EndPoint = new NxtControl.Drawing.PointF(150, 237);
			this.line1.Name = "line1";
			this.line1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.line1.StartPoint = new NxtControl.Drawing.PointF(150, 79);
			// 
			// freeText9
			// 
			this.freeText9.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText9.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText9.Location = new NxtControl.Drawing.PointF(46, 203);
			this.freeText9.Name = "freeText9";
			this.freeText9.Text = "Speed:";
			// 
			// freeText10
			// 
			this.freeText10.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText10.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText10.Location = new NxtControl.Drawing.PointF(166, 180);
			this.freeText10.Name = "freeText10";
			this.freeText10.Text = "Load:";
			// 
			// freeText11
			// 
			this.freeText11.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText11.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText11.Location = new NxtControl.Drawing.PointF(166, 199);
			this.freeText11.Name = "freeText11";
			this.freeText11.Text = "Heat:";
			// 
			// freeText12
			// 
			this.freeText12.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText12.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText12.Location = new NxtControl.Drawing.PointF(165, 216);
			this.freeText12.Name = "freeText12";
			this.freeText12.Text = "Temp:";
			// 
			// rectangle2
			// 
			this.rectangle2.Bounds = new NxtControl.Drawing.RectF(((float)(429)), ((float)(75)), ((float)(169)), ((float)(219)));
			this.rectangle2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle2.Name = "rectangle2";
			// 
			// GF_LightOFF
			// 
			this.GF_LightOFF.Bounds = new NxtControl.Drawing.RectF(((float)(519)), ((float)(245)), ((float)(22)), ((float)(20)));
			this.GF_LightOFF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.GF_LightOFF.ImageName = "HMI:Images.Light-OFF";
			this.GF_LightOFF.Name = "GF_LightOFF";
			// 
			// GF_ON_OFF
			// 
			this.GF_ON_OFF.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.GF_ON_OFF.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_ON_OFF.Location = new NxtControl.Drawing.PointF(452, 247);
			this.GF_ON_OFF.Name = "GF_ON_OFF";
			this.GF_ON_OFF.Text = "OFF";
			// 
			// freeText14
			// 
			this.freeText14.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText14.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText14.Location = new NxtControl.Drawing.PointF(453, 269);
			this.freeText14.Name = "freeText14";
			this.freeText14.Text = "Speed:";
			// 
			// rectangle3
			// 
			this.rectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(35)), ((float)(373)), ((float)(369)), ((float)(330)));
			this.rectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle3.Name = "rectangle3";
			// 
			// LF_LightON
			// 
			this.LF_LightON.Bounds = new NxtControl.Drawing.RectF(((float)(112)), ((float)(179)), ((float)(22)), ((float)(20)));
			this.LF_LightON.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.LF_LightON.ImageName = "HMI:Images.Light-ON";
			this.LF_LightON.Name = "LF_LightON";
			this.LF_LightON.Visible = false;
			// 
			// GF_LightON
			// 
			this.GF_LightON.Bounds = new NxtControl.Drawing.RectF(((float)(519)), ((float)(244)), ((float)(22)), ((float)(20)));
			this.GF_LightON.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.GF_LightON.ImageName = "HMI:Images.Light-ON";
			this.GF_LightON.Name = "GF_LightON";
			this.GF_LightON.Visible = false;
			// 
			// freeText13
			// 
			this.freeText13.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText13.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText13.Location = new NxtControl.Drawing.PointF(716, 426);
			this.freeText13.Name = "freeText13";
			this.freeText13.Text = "Energy Related Info";
			// 
			// LF_Total_EC
			// 
			this.LF_Total_EC.Location = new System.Drawing.Point(648, 497);
			this.LF_Total_EC.Name = "LF_Total_EC";
			this.LF_Total_EC.Size = new System.Drawing.Size(287, 20);
			this.LF_Total_EC.TabIndex = 0;
			// 
			// freeText15
			// 
			this.freeText15.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText15.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText15.Location = new NxtControl.Drawing.PointF(643, 476);
			this.freeText15.Name = "freeText15";
			this.freeText15.Text = "Local Fans\' Total Energy Consumption";
			// 
			// GF_Total_EC
			// 
			this.GF_Total_EC.Location = new System.Drawing.Point(652, 553);
			this.GF_Total_EC.Name = "GF_Total_EC";
			this.GF_Total_EC.Size = new System.Drawing.Size(287, 20);
			this.GF_Total_EC.TabIndex = 0;
			// 
			// freeText16
			// 
			this.freeText16.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText16.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText16.Location = new NxtControl.Drawing.PointF(647, 532);
			this.freeText16.Name = "freeText16";
			this.freeText16.Text = "Global Fans\' Total Energy Consumption";
			// 
			// Total_EC_All
			// 
			this.Total_EC_All.Location = new System.Drawing.Point(655, 610);
			this.Total_EC_All.Name = "Total_EC_All";
			this.Total_EC_All.Size = new System.Drawing.Size(287, 20);
			this.Total_EC_All.TabIndex = 0;
			// 
			// freeText17
			// 
			this.freeText17.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText17.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText17.Location = new NxtControl.Drawing.PointF(650, 589);
			this.freeText17.Name = "freeText17";
			this.freeText17.Text = "Total Energy Consumption";
			// 
			// Reset
			// 
			this.Reset.Bounds = new NxtControl.Drawing.RectF(((float)(661)), ((float)(640)), ((float)(69)), ((float)(29)));
			this.Reset.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Reset.Name = "Reset";
			this.Reset.Radius = 20;
			this.Reset.Text = "Reset";
			this.Reset.Click += new System.EventHandler(this.ResetClick);
			// 
			// Report
			// 
			this.Report.Bounds = new NxtControl.Drawing.RectF(((float)(750)), ((float)(640)), ((float)(69)), ((float)(29)));
			this.Report.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Report.Name = "Report";
			this.Report.Radius = 20;
			this.Report.Text = "Report";
			this.Report.Click += new System.EventHandler(this.ReportClick);
			// 
			// freeText18
			// 
			this.freeText18.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText18.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText18.Location = new NxtControl.Drawing.PointF(717, 55);
			this.freeText18.Name = "freeText18";
			this.freeText18.Text = "Control Strategy Setup";
			// 
			// rectangle6
			// 
			this.rectangle6.Bounds = new NxtControl.Drawing.RectF(((float)(626)), ((float)(80)), ((float)(347)), ((float)(304)));
			this.rectangle6.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle6.Name = "rectangle6";
			// 
			// freeText19
			// 
			this.freeText19.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText19.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText19.Location = new NxtControl.Drawing.PointF(636, 92);
			this.freeText19.Name = "freeText19";
			this.freeText19.Text = "1- GF Speed";
			// 
			// freeText20
			// 
			this.freeText20.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText20.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText20.Location = new NxtControl.Drawing.PointF(638, 151);
			this.freeText20.Name = "freeText20";
			this.freeText20.Text = "3- Room Temp";
			// 
			// freeText21
			// 
			this.freeText21.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText21.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText21.Location = new NxtControl.Drawing.PointF(639, 179);
			this.freeText21.Name = "freeText21";
			this.freeText21.Text = "4- CPU Temp";
			// 
			// freeText22
			// 
			this.freeText22.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText22.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText22.Location = new NxtControl.Drawing.PointF(639, 204);
			this.freeText22.Name = "freeText22";
			this.freeText22.Text = "5- Controller Type";
			// 
			// RTMin
			// 
			this.RTMin.Location = new System.Drawing.Point(821, 148);
			this.RTMin.Name = "RTMin";
			this.RTMin.Size = new System.Drawing.Size(41, 20);
			this.RTMin.TabIndex = 0;
			this.RTMin.Text = "10";
			// 
			// freeText23
			// 
			this.freeText23.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText23.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText23.Location = new NxtControl.Drawing.PointF(782, 149);
			this.freeText23.Name = "freeText23";
			this.freeText23.Text = "Min:";
			// 
			// RTMax
			// 
			this.RTMax.Location = new System.Drawing.Point(916, 148);
			this.RTMax.Name = "RTMax";
			this.RTMax.Size = new System.Drawing.Size(41, 20);
			this.RTMax.TabIndex = 0;
			this.RTMax.Text = "30";
			// 
			// freeText24
			// 
			this.freeText24.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText24.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText24.Location = new NxtControl.Drawing.PointF(875, 149);
			this.freeText24.Name = "freeText24";
			this.freeText24.Text = "Max:";
			// 
			// CPUTMin
			// 
			this.CPUTMin.Location = new System.Drawing.Point(822, 178);
			this.CPUTMin.Name = "CPUTMin";
			this.CPUTMin.Size = new System.Drawing.Size(41, 20);
			this.CPUTMin.TabIndex = 0;
			this.CPUTMin.Text = "10";
			// 
			// freeText25
			// 
			this.freeText25.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText25.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText25.Location = new NxtControl.Drawing.PointF(783, 179);
			this.freeText25.Name = "freeText25";
			this.freeText25.Text = "Min:";
			// 
			// CPUTMax
			// 
			this.CPUTMax.Location = new System.Drawing.Point(917, 178);
			this.CPUTMax.Name = "CPUTMax";
			this.CPUTMax.Size = new System.Drawing.Size(41, 20);
			this.CPUTMax.TabIndex = 0;
			this.CPUTMax.Text = "50";
			// 
			// freeText26
			// 
			this.freeText26.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText26.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText26.Location = new NxtControl.Drawing.PointF(875, 179);
			this.freeText26.Name = "freeText26";
			this.freeText26.Text = "Max:";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton1.Location = new System.Drawing.Point(669, 232);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(77, 24);
			this.radioButton1.TabIndex = 3;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "PID \r";
			// 
			// radioButton2
			// 
			this.radioButton2.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton2.Location = new System.Drawing.Point(669, 259);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(78, 24);
			this.radioButton2.TabIndex = 3;
			this.radioButton2.Text = "MPC";
			// 
			// radioButton3
			// 
			this.radioButton3.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton3.Location = new System.Drawing.Point(668, 287);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(77, 24);
			this.radioButton3.TabIndex = 3;
			this.radioButton3.Text = "VFD";
			// 
			// reSetToDefault
			// 
			this.reSetToDefault.Bounds = new NxtControl.Drawing.RectF(((float)(733)), ((float)(337)), ((float)(137)), ((float)(29)));
			this.reSetToDefault.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.reSetToDefault.Name = "reSetToDefault";
			this.reSetToDefault.Radius = 20;
			this.reSetToDefault.Text = "RESET To Default";
			this.reSetToDefault.Click += new System.EventHandler(this.ReSetToDefaultClick);
			// 
			// freeText27
			// 
			this.freeText27.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText27.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText27.Location = new NxtControl.Drawing.PointF(764, 236);
			this.freeText27.Name = "freeText27";
			this.freeText27.Text = "(Proportional Integral Derivative)";
			// 
			// freeText28
			// 
			this.freeText28.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText28.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText28.Location = new NxtControl.Drawing.PointF(765, 262);
			this.freeText28.Name = "freeText28";
			this.freeText28.Text = "(Model Predictive Control)";
			// 
			// freeText29
			// 
			this.freeText29.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText29.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText29.Location = new NxtControl.Drawing.PointF(765, 291);
			this.freeText29.Name = "freeText29";
			this.freeText29.Text = "(Variable Frequency Drive)";
			// 
			// freeText30
			// 
			this.freeText30.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText30.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText30.Location = new NxtControl.Drawing.PointF(636, 120);
			this.freeText30.Name = "freeText30";
			this.freeText30.Text = "2- LF Speed";
			// 
			// LFSpeed
			// 
			this.LFSpeed.Items.Add("1");
			this.LFSpeed.Items.Add("2");
			this.LFSpeed.Items.Add("3");
			this.LFSpeed.Location = new System.Drawing.Point(820, 119);
			this.LFSpeed.Name = "LFSpeed";
			this.LFSpeed.Size = new System.Drawing.Size(47, 21);
			this.LFSpeed.TabIndex = 4;
			this.LFSpeed.Text = "2";
			// 
			// GFSpeed
			// 
			this.GFSpeed.Items.Add("1");
			this.GFSpeed.Items.Add("2");
			this.GFSpeed.Items.Add("3");
			this.GFSpeed.Location = new System.Drawing.Point(819, 91);
			this.GFSpeed.Name = "GFSpeed";
			this.GFSpeed.Size = new System.Drawing.Size(47, 21);
			this.GFSpeed.TabIndex = 4;
			this.GFSpeed.Text = "2";
			// 
			// Run
			// 
			this.Run.Bounds = new NxtControl.Drawing.RectF(((float)(305)), ((float)(576)), ((float)(88)), ((float)(38)));
			this.Run.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Run.Name = "Run";
			this.Run.Radius = 20;
			this.Run.Text = "Run";
			this.Run.Click += new System.EventHandler(this.RunClick);
			// 
			// Total_EC
			// 
			this.Total_EC.Bounds = new NxtControl.Drawing.RectF(((float)(618)), ((float)(462)), ((float)(372)), ((float)(213)));
			this.Total_EC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Total_EC.Name = "Total_EC";
			// 
			// LF_ON_B
			// 
			this.LF_ON_B.Bounds = new NxtControl.Drawing.RectF(((float)(49)), ((float)(384)), ((float)(147)), ((float)(29)));
			this.LF_ON_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_ON_B.Name = "LF_ON_B";
			this.LF_ON_B.Radius = 20;
			this.LF_ON_B.Text = "Local Fan ON";
			this.LF_ON_B.Click += new System.EventHandler(this.LF_ON_BClick);
			// 
			// LF_OFF_B
			// 
			this.LF_OFF_B.Bounds = new NxtControl.Drawing.RectF(((float)(243)), ((float)(383)), ((float)(147)), ((float)(29)));
			this.LF_OFF_B.Enabled = false;
			this.LF_OFF_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_OFF_B.Name = "LF_OFF_B";
			this.LF_OFF_B.Radius = 20;
			this.LF_OFF_B.Text = "Local Fan OFF";
			this.LF_OFF_B.Click += new System.EventHandler(this.LF_OFF_BClick);
			// 
			// GF_ON_B
			// 
			this.GF_ON_B.Bounds = new NxtControl.Drawing.RectF(((float)(48)), ((float)(432)), ((float)(147)), ((float)(29)));
			this.GF_ON_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_ON_B.Name = "GF_ON_B";
			this.GF_ON_B.Radius = 20;
			this.GF_ON_B.Text = "Global Fan ON";
			this.GF_ON_B.Click += new System.EventHandler(this.GF_ON_BClick);
			// 
			// GF_OFF_B
			// 
			this.GF_OFF_B.Bounds = new NxtControl.Drawing.RectF(((float)(241)), ((float)(436)), ((float)(147)), ((float)(29)));
			this.GF_OFF_B.Enabled = false;
			this.GF_OFF_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_OFF_B.Name = "GF_OFF_B";
			this.GF_OFF_B.Radius = 20;
			this.GF_OFF_B.Text = "Global Fan OFF";
			this.GF_OFF_B.Click += new System.EventHandler(this.GF_OFF_BClick);
			// 
			// freeText7
			// 
			this.freeText7.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText7.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText7.Location = new NxtControl.Drawing.PointF(178, 56);
			this.freeText7.Name = "freeText7";
			this.freeText7.Text = "Server#1";
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle3,
									this.rectangle2,
									this.rectangle1,
									this.t_CPULoad,
									this.CPULoad_Text,
									this.freeText1,
									this.freeText2,
									this.t_RoomTemp,
									this.freeText3,
									this.RoomTemp_Text,
									this.freeText4,
									this.rect_GF,
									this.rect_LF,
									this.rect_LFMove,
									this.rect_GFMove,
									this.freeText5,
									this.freeText6,
									this.freeText7,
									this.rect_CPU,
									this.freeText8,
									this.LF_LightOFF,
									this.LF_ON_OFF,
									this.line1,
									this.freeText9,
									this.freeText10,
									this.freeText11,
									this.freeText12,
									this.GF_LightOFF,
									this.GF_ON_OFF,
									this.freeText14,
									this.LF_LightON,
									this.GF_LightON,
									this.Total_EC,
									this.freeText13,
									this.LF_Total_EC,
									this.freeText15,
									this.GF_Total_EC,
									this.freeText16,
									this.Total_EC_All,
									this.freeText17,
									this.Reset,
									this.Report,
									this.freeText18,
									this.rectangle6,
									this.freeText19,
									this.freeText20,
									this.freeText21,
									this.freeText22,
									this.RTMin,
									this.freeText23,
									this.RTMax,
									this.freeText24,
									this.CPUTMin,
									this.freeText25,
									this.CPUTMax,
									this.freeText26,
									this.radioButton1,
									this.radioButton2,
									this.radioButton3,
									this.reSetToDefault,
									this.freeText27,
									this.freeText28,
									this.freeText29,
									this.freeText30,
									this.LFSpeed,
									this.GFSpeed,
									this.Run,
									this.LF_ON_B,
									this.LF_OFF_B,
									this.GF_ON_B,
									this.GF_OFF_B});
			this.SymbolSize = new System.Drawing.Size(1005, 714);
		}
		private NxtControl.GuiFramework.DrawnButton reSetToDefault;
		private NxtControl.GuiFramework.DrawnButton GF_OFF_B;
		private NxtControl.GuiFramework.DrawnButton GF_ON_B;
		private NxtControl.GuiFramework.DrawnButton LF_OFF_B;
		private NxtControl.GuiFramework.DrawnButton LF_ON_B;
		private NxtControl.GuiFramework.TextBox Total_EC_All;
		private NxtControl.GuiFramework.Rectangle Total_EC;
		private NxtControl.GuiFramework.TextBox LF_Total_EC;
		private NxtControl.GuiFramework.TextBox GF_Total_EC;
		private NxtControl.GuiFramework.DrawnButton Run;
		private NxtControl.GuiFramework.ComboBox GFSpeed;
		private NxtControl.GuiFramework.TextBox RTMin;
		private NxtControl.GuiFramework.TextBox RTMax;
		private NxtControl.GuiFramework.TextBox CPUTMin;
		private NxtControl.GuiFramework.TextBox CPUTMax;
		private NxtControl.GuiFramework.ComboBox LFSpeed;
		private NxtControl.GuiFramework.FreeText freeText30;
		private NxtControl.GuiFramework.FreeText freeText29;
		private NxtControl.GuiFramework.FreeText freeText28;
		private NxtControl.GuiFramework.FreeText freeText27;
		private NxtControl.GuiFramework.RadioButton radioButton3;
		private NxtControl.GuiFramework.RadioButton radioButton2;
		private NxtControl.GuiFramework.RadioButton radioButton1;
		private NxtControl.GuiFramework.FreeText freeText26;
		private NxtControl.GuiFramework.FreeText freeText25;
		private NxtControl.GuiFramework.FreeText freeText24;
		private NxtControl.GuiFramework.FreeText freeText23;
		private NxtControl.GuiFramework.FreeText freeText22;
		private NxtControl.GuiFramework.FreeText freeText21;
		private NxtControl.GuiFramework.FreeText freeText20;
		private NxtControl.GuiFramework.FreeText freeText19;
		private NxtControl.GuiFramework.Rectangle rectangle6;
		private NxtControl.GuiFramework.FreeText freeText18;
		private NxtControl.GuiFramework.DrawnButton Report;
		private NxtControl.GuiFramework.DrawnButton Reset;
		private NxtControl.GuiFramework.FreeText freeText17;
		private NxtControl.GuiFramework.FreeText freeText16;
		private NxtControl.GuiFramework.FreeText freeText15;
		private NxtControl.GuiFramework.FreeText freeText13;
		private NxtControl.GuiFramework.Ellipse GF_LightON;
		private NxtControl.GuiFramework.Ellipse GF_LightOFF;
		private NxtControl.GuiFramework.Ellipse LF_LightON;
		private NxtControl.GuiFramework.Ellipse LF_LightOFF;
		private NxtControl.GuiFramework.Rectangle rectangle3;
		private NxtControl.GuiFramework.FreeText freeText14;
		private NxtControl.GuiFramework.FreeText GF_ON_OFF;
		private NxtControl.GuiFramework.Rectangle rectangle2;
		private NxtControl.GuiFramework.FreeText freeText12;
		private NxtControl.GuiFramework.FreeText freeText11;
		private NxtControl.GuiFramework.FreeText freeText10;
		private NxtControl.GuiFramework.FreeText freeText9;
		private NxtControl.GuiFramework.Line line1;
		private NxtControl.GuiFramework.FreeText LF_ON_OFF;
		private NxtControl.GuiFramework.FreeText freeText8;
		private NxtControl.GuiFramework.Rectangle rect_CPU;
		private NxtControl.GuiFramework.FreeText freeText7;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtControl.GuiFramework.FreeText freeText6;
		private NxtControl.GuiFramework.FreeText freeText5;
		private NxtControl.GuiFramework.Rectangle rect_GFMove;
		private NxtControl.GuiFramework.Rectangle rect_LFMove;
		private NxtControl.GuiFramework.Rectangle rect_GF;
		private NxtControl.GuiFramework.Rectangle rect_LF;
		private NxtControl.GuiFramework.FreeText freeText4;
		private NxtControl.GuiFramework.TextBox RoomTemp_Text;
		private NxtControl.GuiFramework.FreeText freeText3;
		private NxtControl.GuiFramework.Tracker t_RoomTemp;
		private NxtControl.GuiFramework.FreeText freeText2;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.Tracker t_CPULoad;
		private NxtControl.GuiFramework.TextBox CPULoad_Text;
		#endregion
	}
}
