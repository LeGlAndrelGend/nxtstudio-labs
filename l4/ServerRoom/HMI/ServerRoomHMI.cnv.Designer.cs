﻿/*
 * Created by nxtSTUDIO.
 * User: Bob
 * Date: 9/26/2017
 * Time: 5:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for ServerRoomHMI.
	/// </summary>
	partial class ServerRoomHMI
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SRFBAPP = new HMI.Main.Symbols.ServerRoomFB.sDefault();
			// 
			// SRFBAPP
			// 
			this.SRFBAPP.BeginInit();
			this.SRFBAPP.AngleIgnore = false;
			this.SRFBAPP.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 0, 1);
			this.SRFBAPP.Name = "SRFBAPP";
			this.SRFBAPP.SecurityToken = ((uint)(4294967295u));
			this.SRFBAPP.TagName = "3F310A6257D84926";
			this.SRFBAPP.EndInit();
			// 
			// ServerRoomHMI
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1000)), ((float)(680)));
			this.Name = "ServerRoomHMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.SRFBAPP});
			this.Size = new System.Drawing.Size(1000, 680);
			this.Title = "ServerRoomHMI";
		}
		private HMI.Main.Symbols.ServerRoomFB.sDefault SRFBAPP;
		#endregion
	}
}
