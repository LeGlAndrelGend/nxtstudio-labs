﻿/*
 * Created by nxtSTUDIO.
 * User: Alto
 * Date: 09/26/2017
 * Time: 8:15 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace HMI.Main.Symbols.GFSerRoomFB
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
	  /*--declare global variables--*/
	  bool GF_State_ON = false;
	  int GF_EC_Total = 0;
    int C2 = 5; 
		public sDefault()
		{
			// The InitializeComponent() call is required for Windows Forms designer support.
			InitializeComponent();
      this.InitServerRoom_Fired +=new EventHandler <GFSerRoomFB.InitServerRoomEventArgs>(InitServerRoom);
			this.Receive_Fired += new EventHandler <GFSerRoomFB.ReceiveEventArgs> (Receive);
		}
		public void InitServerRoom(object Sender, GFSerRoomFB.InitServerRoomEventArgs sr){}		
    public void Receive(object Sender, GFSerRoomFB.ReceiveEventArgs r){}

    /*--Run button--*/
    void RunClick(object sender, EventArgs e)
		{
			t_RoomTemp.Value = double.Parse(RoomTemp_Text.Text);
		}
    /*--Change image static/animated--*/
    /*--Light on/off when fun is turned on/off--*/
    
    void GF_ON_BClick(object sender, EventArgs e)
		{
      GFTurnOn();
      GF_ON_B.Enabled = false;
      GF_OFF_B.Enabled = true;
		}
    public void GFTurnOn()
    {
      this.FireEvent_GF_ON("GF_ON");  
      GF_State_ON = true;
      rect_GF.Visible = false;
      rect_GFMove.Visible = true;
      GF_ON_OFF.Text = "ON";
      GF_LightON.Visible = true;
      GF_LightOFF.Visible = false;
    }
    
		void GF_OFF_BClick(object sender, EventArgs e)
		{
		  GFTurnOff();
      GF_ON_B.Enabled = true;
      GF_OFF_B.Enabled = false;			
		}
		public void GFTurnOff()
		{
		  this.FireEvent_GF_OFF("GF_OFF"); 
      GF_State_ON = false;
      rect_GF.Visible = true;
      rect_GFMove.Visible = false;
      GF_ON_OFF.Text = "OFF";
      GF_LightON.Visible = false;
      GF_LightOFF.Visible = true;
		}

    /*--Working with Tracker--*/
    /*--Change temperature by using tracker, turn on fan if not default temp range--*/
    /*--Count electricity consumption LF and GF when turn funs on by trackers--*/
    
    public void moveTrackerDefault(ref NxtControl.GuiFramework.Tracker tracker, ref NxtControl.GuiFramework.TextBox trackerText, ref NxtControl.GuiFramework.TextBox tempMin)
    {
      try 
      {
        string[] parts = trackerText.Text.Split('.');
        int textValue = int.Parse(parts[0]);
        tracker.Value = Convert.ToDouble(textValue);
        Thread.Sleep(500);
        for (int curVal = textValue; curVal >= Convert.ToInt32(double.Parse(tempMin.Text)); curVal--)
        {
          tracker.Value = Convert.ToDouble(curVal);
          Thread.Sleep(100);
        }
      } catch( Exception ex) {}
    }
    
    void T_RoomTempValueChanged(object sender, EventArgs e)
		{
      RoomTemp_Text.Text = t_RoomTemp.Value.ToString();
      
      if (t_RoomTemp.Value > double.Parse(RTMax.Text) && !GF_State_ON)
      {
        GFTurnOn();
        Thread threadRoom = new Thread( new ThreadStart( moveRoomTemp ));
        threadRoom.Start();
        GF_EC_Total += C2;   //increase total EC consume by GF
      } else if (t_RoomTemp.Value == double.Parse(RTMin.Text) && GF_State_ON)
        {
          GFTurnOff();
        }
    }

	  public void moveRoomTemp()
    {
	    moveTrackerDefault(ref t_RoomTemp, ref RoomTemp_Text, ref RTMin);
    }
	  
	  /*--Reset trackers parameters to default--*/
	  void ReSetToDefaultClick(object sender, EventArgs e)
		{
			GFSpeed.Text = "2";
			RTMin.Text = "10";
			RTMax.Text = "30";
		}

    void ResetClick(object sender, EventArgs e)
		{
      t_RoomTemp.Value =  double.Parse(RTMin.Text);
		  RoomTemp_Text.Text = RTMin.Text;
      GF_EC_Total = 0;
      GF_Total_EC.Clear();
      Total_EC_All.Clear();
		}
    
    /*--Count total electricity consumption LF and GF--*/
    /*--Output info about consumption electricity by click to Report button--*/
    void ReportClick(object sender, EventArgs e)
		{
			GF_Total_EC.Text = GF_EC_Total.ToString();
			Total_EC_All.Text = (GF_EC_Total).ToString();
		}
	}
}
