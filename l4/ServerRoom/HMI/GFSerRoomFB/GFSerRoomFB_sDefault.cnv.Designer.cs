﻿/*
 * Created by nxtSTUDIO.
 * User: arader
 * Date: 2/6/2014
 * Time: 3:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.GFSerRoomFB
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.t_RoomTemp = new NxtControl.GuiFramework.Tracker();
			this.freeText3 = new NxtControl.GuiFramework.FreeText();
			this.RoomTemp_Text = new NxtControl.GuiFramework.TextBox();
			this.freeText4 = new NxtControl.GuiFramework.FreeText();
			this.rect_GF = new NxtControl.GuiFramework.Rectangle();
			this.rect_GFMove = new NxtControl.GuiFramework.Rectangle();
			this.freeText6 = new NxtControl.GuiFramework.FreeText();
			this.rectangle2 = new NxtControl.GuiFramework.Rectangle();
			this.GF_LightOFF = new NxtControl.GuiFramework.Ellipse();
			this.GF_ON_OFF = new NxtControl.GuiFramework.FreeText();
			this.freeText14 = new NxtControl.GuiFramework.FreeText();
			this.rectangle3 = new NxtControl.GuiFramework.Rectangle();
			this.GF_LightON = new NxtControl.GuiFramework.Ellipse();
			this.freeText13 = new NxtControl.GuiFramework.FreeText();
			this.GF_Total_EC = new NxtControl.GuiFramework.TextBox();
			this.freeText16 = new NxtControl.GuiFramework.FreeText();
			this.Total_EC_All = new NxtControl.GuiFramework.TextBox();
			this.freeText17 = new NxtControl.GuiFramework.FreeText();
			this.Reset = new NxtControl.GuiFramework.DrawnButton();
			this.Report = new NxtControl.GuiFramework.DrawnButton();
			this.freeText18 = new NxtControl.GuiFramework.FreeText();
			this.rectangle6 = new NxtControl.GuiFramework.Rectangle();
			this.freeText19 = new NxtControl.GuiFramework.FreeText();
			this.freeText20 = new NxtControl.GuiFramework.FreeText();
			this.freeText22 = new NxtControl.GuiFramework.FreeText();
			this.RTMin = new NxtControl.GuiFramework.TextBox();
			this.freeText23 = new NxtControl.GuiFramework.FreeText();
			this.RTMax = new NxtControl.GuiFramework.TextBox();
			this.freeText24 = new NxtControl.GuiFramework.FreeText();
			this.radioButton1 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton2 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton3 = new NxtControl.GuiFramework.RadioButton();
			this.reSetToDefault = new NxtControl.GuiFramework.DrawnButton();
			this.freeText27 = new NxtControl.GuiFramework.FreeText();
			this.freeText28 = new NxtControl.GuiFramework.FreeText();
			this.freeText29 = new NxtControl.GuiFramework.FreeText();
			this.GFSpeed = new NxtControl.GuiFramework.ComboBox();
			this.Run = new NxtControl.GuiFramework.DrawnButton();
			this.Total_EC = new NxtControl.GuiFramework.Rectangle();
			this.GF_ON_B = new NxtControl.GuiFramework.DrawnButton();
			this.GF_OFF_B = new NxtControl.GuiFramework.DrawnButton();
			// 
			// t_RoomTemp
			// 
			this.t_RoomTemp.BeginInit();
			this.t_RoomTemp.Bounds = new NxtControl.Drawing.RectF(((float)(49)), ((float)(501)), ((float)(238)), ((float)(59)));
			this.t_RoomTemp.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.t_RoomTemp.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_RoomTemp.Maximum = 50;
			this.t_RoomTemp.Minimum = 0;
			this.t_RoomTemp.Name = "t_RoomTemp";
			this.t_RoomTemp.Orientation = System.Windows.Forms.Orientation.Horizontal;
			this.t_RoomTemp.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.TrackHandleBrush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(139))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center));
			this.t_RoomTemp.TrackHandlePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.TrackLinePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_RoomTemp.ValueFont = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_RoomTemp.ValueChanged += new System.EventHandler(this.T_RoomTempValueChanged);
			this.t_RoomTemp.EndInit();
			// 
			// freeText3
			// 
			this.freeText3.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText3.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText3.Location = new NxtControl.Drawing.PointF(47, 473);
			this.freeText3.Name = "freeText3";
			this.freeText3.Text = "Change Room Temprature";
			// 
			// RoomTemp_Text
			// 
			this.RoomTemp_Text.Location = new System.Drawing.Point(306, 521);
			this.RoomTemp_Text.Name = "RoomTemp_Text";
			this.RoomTemp_Text.Size = new System.Drawing.Size(88, 20);
			this.RoomTemp_Text.TabIndex = 0;
			this.RoomTemp_Text.Text = "10";
			// 
			// freeText4
			// 
			this.freeText4.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText4.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText4.Location = new NxtControl.Drawing.PointF(301, 501);
			this.freeText4.Name = "freeText4";
			this.freeText4.Text = "Room Temp";
			// 
			// rect_GF
			// 
			this.rect_GF.Bounds = new NxtControl.Drawing.RectF(((float)(431)), ((float)(108)), ((float)(166)), ((float)(126)));
			this.rect_GF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_GF.ImageName = "HMI:imagesnew.GF";
			this.rect_GF.Name = "rect_GF";
			// 
			// rect_GFMove
			// 
			this.rect_GFMove.Bounds = new NxtControl.Drawing.RectF(((float)(431)), ((float)(108)), ((float)(166)), ((float)(126)));
			this.rect_GFMove.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_GFMove.ImageName = "HMI:imagesnew.GF-move";
			this.rect_GFMove.Name = "rect_GFMove";
			this.rect_GFMove.Visible = false;
			// 
			// freeText6
			// 
			this.freeText6.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText6.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText6.Location = new NxtControl.Drawing.PointF(472, 87);
			this.freeText6.Name = "freeText6";
			this.freeText6.Text = "Global Fan";
			// 
			// rectangle2
			// 
			this.rectangle2.Bounds = new NxtControl.Drawing.RectF(((float)(429)), ((float)(75)), ((float)(169)), ((float)(219)));
			this.rectangle2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle2.Name = "rectangle2";
			// 
			// GF_LightOFF
			// 
			this.GF_LightOFF.Bounds = new NxtControl.Drawing.RectF(((float)(519)), ((float)(245)), ((float)(22)), ((float)(20)));
			this.GF_LightOFF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.GF_LightOFF.ImageName = "HMI:imagesnew.light-off";
			this.GF_LightOFF.Name = "GF_LightOFF";
			// 
			// GF_ON_OFF
			// 
			this.GF_ON_OFF.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.GF_ON_OFF.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_ON_OFF.Location = new NxtControl.Drawing.PointF(452, 247);
			this.GF_ON_OFF.Name = "GF_ON_OFF";
			this.GF_ON_OFF.Text = "OFF";
			// 
			// freeText14
			// 
			this.freeText14.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText14.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText14.Location = new NxtControl.Drawing.PointF(453, 269);
			this.freeText14.Name = "freeText14";
			this.freeText14.Text = "Speed:";
			// 
			// rectangle3
			// 
			this.rectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(36)), ((float)(246)), ((float)(369)), ((float)(330)));
			this.rectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle3.Name = "rectangle3";
			// 
			// GF_LightON
			// 
			this.GF_LightON.Bounds = new NxtControl.Drawing.RectF(((float)(519)), ((float)(245)), ((float)(22)), ((float)(20)));
			this.GF_LightON.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.GF_LightON.ImageName = "HMI:imagesnew.light-on";
			this.GF_LightON.Name = "GF_LightON";
			this.GF_LightON.Visible = false;
			// 
			// freeText13
			// 
			this.freeText13.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText13.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText13.Location = new NxtControl.Drawing.PointF(693, 391);
			this.freeText13.Name = "freeText13";
			this.freeText13.Text = "Energy Related Info";
			// 
			// GF_Total_EC
			// 
			this.GF_Total_EC.Location = new System.Drawing.Point(667, 500);
			this.GF_Total_EC.Name = "GF_Total_EC";
			this.GF_Total_EC.Size = new System.Drawing.Size(287, 20);
			this.GF_Total_EC.TabIndex = 0;
			// 
			// freeText16
			// 
			this.freeText16.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText16.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText16.Location = new NxtControl.Drawing.PointF(653, 479);
			this.freeText16.Name = "freeText16";
			this.freeText16.Text = "Global Fans\' Total Energy Consumption";
			// 
			// Total_EC_All
			// 
			this.Total_EC_All.Location = new System.Drawing.Point(670, 552);
			this.Total_EC_All.Name = "Total_EC_All";
			this.Total_EC_All.Size = new System.Drawing.Size(287, 20);
			this.Total_EC_All.TabIndex = 0;
			// 
			// freeText17
			// 
			this.freeText17.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText17.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText17.Location = new NxtControl.Drawing.PointF(709, 529);
			this.freeText17.Name = "freeText17";
			this.freeText17.Text = "Total Energy Consumption";
			// 
			// Reset
			// 
			this.Reset.Bounds = new NxtControl.Drawing.RectF(((float)(573)), ((float)(510)), ((float)(69)), ((float)(29)));
			this.Reset.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Reset.Name = "Reset";
			this.Reset.Radius = 20;
			this.Reset.Text = "Reset";
			this.Reset.Click += new System.EventHandler(this.ResetClick);
			// 
			// Report
			// 
			this.Report.Bounds = new NxtControl.Drawing.RectF(((float)(573)), ((float)(467)), ((float)(69)), ((float)(29)));
			this.Report.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Report.Name = "Report";
			this.Report.Radius = 20;
			this.Report.Text = "Report";
			this.Report.Click += new System.EventHandler(this.ReportClick);
			// 
			// freeText18
			// 
			this.freeText18.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText18.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText18.Location = new NxtControl.Drawing.PointF(717, 55);
			this.freeText18.Name = "freeText18";
			this.freeText18.Text = "Control Strategy Setup";
			// 
			// rectangle6
			// 
			this.rectangle6.Bounds = new NxtControl.Drawing.RectF(((float)(626)), ((float)(80)), ((float)(347)), ((float)(304)));
			this.rectangle6.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle6.Name = "rectangle6";
			// 
			// freeText19
			// 
			this.freeText19.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText19.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText19.Location = new NxtControl.Drawing.PointF(636, 92);
			this.freeText19.Name = "freeText19";
			this.freeText19.Text = "1- GF Speed";
			// 
			// freeText20
			// 
			this.freeText20.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText20.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText20.Location = new NxtControl.Drawing.PointF(638, 151);
			this.freeText20.Name = "freeText20";
			this.freeText20.Text = "3- Room Temp";
			// 
			// freeText22
			// 
			this.freeText22.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText22.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText22.Location = new NxtControl.Drawing.PointF(639, 204);
			this.freeText22.Name = "freeText22";
			this.freeText22.Text = "5- Controller Type";
			// 
			// RTMin
			// 
			this.RTMin.Location = new System.Drawing.Point(821, 148);
			this.RTMin.Name = "RTMin";
			this.RTMin.Size = new System.Drawing.Size(41, 20);
			this.RTMin.TabIndex = 0;
			this.RTMin.Text = "10";
			// 
			// freeText23
			// 
			this.freeText23.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText23.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText23.Location = new NxtControl.Drawing.PointF(782, 149);
			this.freeText23.Name = "freeText23";
			this.freeText23.Text = "Min:";
			// 
			// RTMax
			// 
			this.RTMax.Location = new System.Drawing.Point(916, 148);
			this.RTMax.Name = "RTMax";
			this.RTMax.Size = new System.Drawing.Size(41, 20);
			this.RTMax.TabIndex = 0;
			this.RTMax.Text = "30";
			// 
			// freeText24
			// 
			this.freeText24.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText24.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText24.Location = new NxtControl.Drawing.PointF(875, 149);
			this.freeText24.Name = "freeText24";
			this.freeText24.Text = "Max:";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton1.Location = new System.Drawing.Point(669, 232);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(77, 24);
			this.radioButton1.TabIndex = 3;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "PID \r";
			// 
			// radioButton2
			// 
			this.radioButton2.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton2.Location = new System.Drawing.Point(669, 259);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(78, 24);
			this.radioButton2.TabIndex = 3;
			this.radioButton2.Text = "MPC";
			// 
			// radioButton3
			// 
			this.radioButton3.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton3.Location = new System.Drawing.Point(668, 287);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(77, 24);
			this.radioButton3.TabIndex = 3;
			this.radioButton3.Text = "VFD";
			// 
			// reSetToDefault
			// 
			this.reSetToDefault.Bounds = new NxtControl.Drawing.RectF(((float)(733)), ((float)(337)), ((float)(137)), ((float)(29)));
			this.reSetToDefault.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.reSetToDefault.Name = "reSetToDefault";
			this.reSetToDefault.Radius = 20;
			this.reSetToDefault.Text = "RESET To Default";
			this.reSetToDefault.Click += new System.EventHandler(this.ReSetToDefaultClick);
			// 
			// freeText27
			// 
			this.freeText27.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText27.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText27.Location = new NxtControl.Drawing.PointF(764, 236);
			this.freeText27.Name = "freeText27";
			this.freeText27.Text = "(Proportional Integral Derivative)";
			// 
			// freeText28
			// 
			this.freeText28.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText28.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText28.Location = new NxtControl.Drawing.PointF(765, 262);
			this.freeText28.Name = "freeText28";
			this.freeText28.Text = "(Model Predictive Control)";
			// 
			// freeText29
			// 
			this.freeText29.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText29.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText29.Location = new NxtControl.Drawing.PointF(765, 291);
			this.freeText29.Name = "freeText29";
			this.freeText29.Text = "(Variable Frequency Drive)";
			// 
			// GFSpeed
			// 
			this.GFSpeed.Items.Add("1");
			this.GFSpeed.Items.Add("2");
			this.GFSpeed.Items.Add("3");
			this.GFSpeed.Location = new System.Drawing.Point(819, 91);
			this.GFSpeed.Name = "GFSpeed";
			this.GFSpeed.Size = new System.Drawing.Size(47, 21);
			this.GFSpeed.TabIndex = 4;
			this.GFSpeed.Text = "2";
			// 
			// Run
			// 
			this.Run.Bounds = new NxtControl.Drawing.RectF(((float)(306)), ((float)(449)), ((float)(88)), ((float)(38)));
			this.Run.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Run.Name = "Run";
			this.Run.Radius = 20;
			this.Run.Text = "Run";
			this.Run.Click += new System.EventHandler(this.RunClick);
			// 
			// Total_EC
			// 
			this.Total_EC.Bounds = new NxtControl.Drawing.RectF(((float)(564)), ((float)(420)), ((float)(411)), ((float)(167)));
			this.Total_EC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Total_EC.Name = "Total_EC";
			// 
			// GF_ON_B
			// 
			this.GF_ON_B.Bounds = new NxtControl.Drawing.RectF(((float)(49)), ((float)(305)), ((float)(147)), ((float)(29)));
			this.GF_ON_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_ON_B.Name = "GF_ON_B";
			this.GF_ON_B.Radius = 20;
			this.GF_ON_B.Text = "Global Fan ON";
			this.GF_ON_B.Click += new System.EventHandler(this.GF_ON_BClick);
			// 
			// GF_OFF_B
			// 
			this.GF_OFF_B.Bounds = new NxtControl.Drawing.RectF(((float)(242)), ((float)(309)), ((float)(147)), ((float)(29)));
			this.GF_OFF_B.Enabled = false;
			this.GF_OFF_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.GF_OFF_B.Name = "GF_OFF_B";
			this.GF_OFF_B.Radius = 20;
			this.GF_OFF_B.Text = "Global Fan OFF";
			this.GF_OFF_B.Click += new System.EventHandler(this.GF_OFF_BClick);
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle3,
									this.rectangle2,
									this.t_RoomTemp,
									this.freeText3,
									this.RoomTemp_Text,
									this.freeText4,
									this.rect_GF,
									this.rect_GFMove,
									this.freeText6,
									this.GF_LightOFF,
									this.GF_ON_OFF,
									this.freeText14,
									this.GF_LightON,
									this.Total_EC,
									this.freeText13,
									this.GF_Total_EC,
									this.freeText16,
									this.Total_EC_All,
									this.freeText17,
									this.Reset,
									this.Report,
									this.freeText18,
									this.rectangle6,
									this.freeText19,
									this.freeText20,
									this.freeText22,
									this.RTMin,
									this.freeText23,
									this.RTMax,
									this.freeText24,
									this.radioButton1,
									this.radioButton2,
									this.radioButton3,
									this.reSetToDefault,
									this.freeText27,
									this.freeText28,
									this.freeText29,
									this.GFSpeed,
									this.Run,
									this.GF_ON_B,
									this.GF_OFF_B});
			this.SymbolSize = new System.Drawing.Size(1005, 714);
		}
		private NxtControl.GuiFramework.DrawnButton reSetToDefault;
		private NxtControl.GuiFramework.DrawnButton GF_OFF_B;
		private NxtControl.GuiFramework.DrawnButton GF_ON_B;
		private NxtControl.GuiFramework.TextBox Total_EC_All;
		private NxtControl.GuiFramework.Rectangle Total_EC;
		private NxtControl.GuiFramework.TextBox GF_Total_EC;
		private NxtControl.GuiFramework.DrawnButton Run;
		private NxtControl.GuiFramework.ComboBox GFSpeed;
		private NxtControl.GuiFramework.TextBox RTMin;
		private NxtControl.GuiFramework.TextBox RTMax;
		private NxtControl.GuiFramework.FreeText freeText29;
		private NxtControl.GuiFramework.FreeText freeText28;
		private NxtControl.GuiFramework.FreeText freeText27;
		private NxtControl.GuiFramework.RadioButton radioButton3;
		private NxtControl.GuiFramework.RadioButton radioButton2;
		private NxtControl.GuiFramework.RadioButton radioButton1;
		private NxtControl.GuiFramework.FreeText freeText24;
		private NxtControl.GuiFramework.FreeText freeText23;
		private NxtControl.GuiFramework.FreeText freeText22;
		private NxtControl.GuiFramework.FreeText freeText20;
		private NxtControl.GuiFramework.FreeText freeText19;
		private NxtControl.GuiFramework.Rectangle rectangle6;
		private NxtControl.GuiFramework.FreeText freeText18;
		private NxtControl.GuiFramework.DrawnButton Report;
		private NxtControl.GuiFramework.DrawnButton Reset;
		private NxtControl.GuiFramework.FreeText freeText17;
		private NxtControl.GuiFramework.FreeText freeText16;
		private NxtControl.GuiFramework.FreeText freeText13;
		private NxtControl.GuiFramework.Ellipse GF_LightON;
		private NxtControl.GuiFramework.Ellipse GF_LightOFF;
		private NxtControl.GuiFramework.Rectangle rectangle3;
		private NxtControl.GuiFramework.FreeText freeText14;
		private NxtControl.GuiFramework.FreeText GF_ON_OFF;
		private NxtControl.GuiFramework.Rectangle rectangle2;
		private NxtControl.GuiFramework.FreeText freeText6;
		private NxtControl.GuiFramework.Rectangle rect_GFMove;
		private NxtControl.GuiFramework.Rectangle rect_GF;
		private NxtControl.GuiFramework.FreeText freeText4;
		private NxtControl.GuiFramework.TextBox RoomTemp_Text;
		private NxtControl.GuiFramework.FreeText freeText3;
		private NxtControl.GuiFramework.Tracker t_RoomTemp;
		#endregion
	}
}
