namespace NxtControl.Drawing {
    
    
    public class ProjectColors {
        
        public static NxtControl.Drawing.Color White;
        
        public static NxtControl.Drawing.Color Black;
        
        public static NxtControl.Drawing.Color Transparent;
        
        public static NxtControl.Drawing.Color Red;
        
        public static NxtControl.Drawing.Color Green;
        
        public static NxtControl.Drawing.Color Blue;
        
        public static NxtControl.Drawing.Color Yellow;
        
        public static NxtControl.Drawing.Color Magenta;
        
        public static NxtControl.Drawing.Color Cyan;
        
        public static NxtControl.Drawing.Color Grey50;
        
        public static NxtControl.Drawing.Color DarkGrey;
        
        public static NxtControl.Drawing.Color LightGrey;
        
        public static NxtControl.Drawing.Color DevPassive;
        
        public static NxtControl.Drawing.Color DevActive1;
        
        public static NxtControl.Drawing.Color DevActive2;
        
        public static NxtControl.Drawing.Color DevAnalogSp;
        
        public static NxtControl.Drawing.Color DevAnalogPv;
        
        public static NxtControl.Drawing.Color DevAnalogOut;
        
        public static NxtControl.Drawing.Color MedAcid;
        
        public static NxtControl.Drawing.Color MedAir;
        
        public static NxtControl.Drawing.Color MedAlcali;
        
        public static NxtControl.Drawing.Color MedCoal;
        
        public static NxtControl.Drawing.Color MedBurnGas;
        
        public static NxtControl.Drawing.Color MedFlueGas;
        
        public static NxtControl.Drawing.Color MedBurnOil;
        
        public static NxtControl.Drawing.Color MedOxygen;
        
        public static NxtControl.Drawing.Color MedOil;
        
        public static NxtControl.Drawing.Color MedSteam;
        
        public static NxtControl.Drawing.Color MedSteamMP;
        
        public static NxtControl.Drawing.Color MedSteamLP;
        
        public static NxtControl.Drawing.Color Water;
        
        public static NxtControl.Drawing.Color CoolWater;
        
        public static NxtControl.Drawing.Color NatWater;
        
        public static NxtControl.Drawing.Color Ele110kV;
        
        public static NxtControl.Drawing.Color Ele30kV;
        
        public static NxtControl.Drawing.Color Ele10_20kV;
        
        public static NxtControl.Drawing.Color Ele1_10kV;
        
        public static NxtControl.Drawing.Color EleNV;
        
        public static NxtControl.Drawing.Color Ele24dc;
        
        public static NxtControl.Drawing.Color EleNVme;
        
        public static NxtControl.Drawing.Color AlarmCame;
        
        public static NxtControl.Drawing.Color WarningCame;
        
        public static NxtControl.Drawing.BlinkColor BlackWhite;
        
        public static NxtControl.Drawing.BlinkColor RedTransparent;
        
        public static NxtControl.Drawing.BlinkColor DevGoToPas;
        
        public static NxtControl.Drawing.BlinkColor DevGoToAct1;
        
        public static NxtControl.Drawing.BlinkColor DevGoToAct2;
        
        public static NxtControl.Drawing.BlinkColor DevError;
        
        public static NxtControl.Drawing.BlinkColor WarningCameNotAcked;
        
        public static NxtControl.Drawing.BlinkColor AlarmGoneNotAcked;
        
        public static NxtControl.Drawing.BlinkColor AlarmCameNotAcked;
        
        public static NxtControl.Drawing.BlinkColor WarningGoneNotAcked;
        
        static ProjectColors() {
            White = NxtControl.Drawing.SystemColors.White;
            Black = NxtControl.Drawing.SystemColors.Black;
            Transparent = NxtControl.Drawing.SystemColors.Transparent;
            Red = NxtControl.Drawing.SystemColors.Red;
            Green = NxtControl.Drawing.SystemColors.Green;
            Blue = NxtControl.Drawing.SystemColors.Blue;
            Yellow = NxtControl.Drawing.SystemColors.Yellow;
            Magenta = NxtControl.Drawing.SystemColors.Magenta;
            Cyan = NxtControl.Drawing.SystemColors.Cyan;
            Grey50 = NxtControl.Drawing.SystemColors.Grey50;
            DarkGrey = NxtControl.Drawing.SystemColors.DarkGrey;
            LightGrey = NxtControl.Drawing.SystemColors.LightGrey;
            DevPassive = NxtControl.Drawing.SystemColors.DevPassive;
            DevActive1 = NxtControl.Drawing.SystemColors.DevActive1;
            DevActive2 = NxtControl.Drawing.SystemColors.DevActive2;
            DevAnalogSp = NxtControl.Drawing.SystemColors.DevAnalogSp;
            DevAnalogPv = NxtControl.Drawing.SystemColors.DevAnalogPv;
            DevAnalogOut = NxtControl.Drawing.SystemColors.DevAnalogOut;
            MedAcid = NxtControl.Drawing.SystemColors.MedAcid;
            MedAir = NxtControl.Drawing.SystemColors.MedAir;
            MedAlcali = NxtControl.Drawing.SystemColors.MedAlcali;
            MedCoal = NxtControl.Drawing.SystemColors.MedCoal;
            MedBurnGas = NxtControl.Drawing.SystemColors.MedBurnGas;
            MedFlueGas = NxtControl.Drawing.SystemColors.MedFlueGas;
            MedBurnOil = NxtControl.Drawing.SystemColors.MedBurnOil;
            MedOxygen = NxtControl.Drawing.SystemColors.MedOxygen;
            MedOil = NxtControl.Drawing.SystemColors.MedOil;
            MedSteam = NxtControl.Drawing.SystemColors.MedSteam;
            MedSteamMP = NxtControl.Drawing.SystemColors.MedSteamMP;
            MedSteamLP = NxtControl.Drawing.SystemColors.MedSteamLP;
            Water = NxtControl.Drawing.SystemColors.Water;
            CoolWater = NxtControl.Drawing.SystemColors.CoolWater;
            NatWater = NxtControl.Drawing.SystemColors.NatWater;
            Ele110kV = NxtControl.Drawing.SystemColors.Ele110kV;
            Ele30kV = NxtControl.Drawing.SystemColors.Ele30kV;
            Ele10_20kV = NxtControl.Drawing.SystemColors.Ele10_20kV;
            Ele1_10kV = NxtControl.Drawing.SystemColors.Ele1_10kV;
            EleNV = NxtControl.Drawing.SystemColors.EleNV;
            Ele24dc = NxtControl.Drawing.SystemColors.Ele24dc;
            EleNVme = NxtControl.Drawing.SystemColors.EleNVme;
            AlarmCame = NxtControl.Drawing.SystemColors.AlarmCame;
            WarningCame = NxtControl.Drawing.SystemColors.WarningCame;
            BlackWhite = NxtControl.Drawing.SystemColors.BlackWhite;
            RedTransparent = NxtControl.Drawing.SystemColors.RedTransparent;
            DevGoToPas = NxtControl.Drawing.SystemColors.DevGoToPas;
            DevGoToAct1 = NxtControl.Drawing.SystemColors.DevGoToAct1;
            DevGoToAct2 = NxtControl.Drawing.SystemColors.DevGoToAct2;
            DevError = NxtControl.Drawing.SystemColors.DevError;
            WarningCameNotAcked = NxtControl.Drawing.SystemColors.WarningCameNotAcked;
            AlarmGoneNotAcked = NxtControl.Drawing.SystemColors.AlarmGoneNotAcked;
            AlarmCameNotAcked = NxtControl.Drawing.SystemColors.AlarmCameNotAcked;
            WarningGoneNotAcked = NxtControl.Drawing.SystemColors.WarningGoneNotAcked;
        }
    }
}
