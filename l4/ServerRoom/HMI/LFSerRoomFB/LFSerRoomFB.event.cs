/*
 * Created by nxtSTUDIO.
 * User: arader
 * Date: 2/6/2014
 * Time: 3:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #LFSerRoomFB_HMI;

namespace HMI.Main.Symbols.LFSerRoomFB
{

  public class InitServerRoomEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public InitServerRoomEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_IP(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String IP
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_PORT(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String PORT
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }


  }

  public class ReceiveEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public ReceiveEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_IP(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String IP
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_PORT(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String PORT
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }


  }

}

namespace HMI.Main.Symbols.LFSerRoomFB
{

  public class InitFinishEventArgs : System.EventArgs
  {
    public InitFinishEventArgs()
    {
    }
    private System.String SocketInit_field = null;
    public System.String SocketInit
    {
       get { return SocketInit_field; }
       set { SocketInit_field = value; }
    }

  }

  public class LF_ONEventArgs : System.EventArgs
  {
    public LF_ONEventArgs()
    {
    }
    private System.String DataOut_field = null;
    public System.String DataOut
    {
       get { return DataOut_field; }
       set { DataOut_field = value; }
    }

  }

  public class LF_OFFEventArgs : System.EventArgs
  {
    public LF_OFFEventArgs()
    {
    }
    private System.String DataOut_field = null;
    public System.String DataOut
    {
       get { return DataOut_field; }
       set { DataOut_field = value; }
    }

  }

  public class GF_ONEventArgs : System.EventArgs
  {
    public GF_ONEventArgs()
    {
    }
    private System.String DataOut_field = null;
    public System.String DataOut
    {
       get { return DataOut_field; }
       set { DataOut_field = value; }
    }

  }

  public class GF_OFFEventArgs : System.EventArgs
  {
    public GF_OFFEventArgs()
    {
    }
    private System.String DataOut_field = null;
    public System.String DataOut
    {
       get { return DataOut_field; }
       set { DataOut_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.LFSerRoomFB
{
  partial class sDefault
  {

    private event EventHandler<HMI.Main.Symbols.LFSerRoomFB.InitServerRoomEventArgs> InitServerRoom_Fired;

    private event EventHandler<HMI.Main.Symbols.LFSerRoomFB.ReceiveEventArgs> Receive_Fired;

    protected override void OnEndInit()
    {
      if (InitServerRoom_Fired != null)
        AttachEventInput(0);
      if (Receive_Fired != null)
        AttachEventInput(1);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (InitServerRoom_Fired != null)
            InitServerRoom_Fired(this, new HMI.Main.Symbols.LFSerRoomFB.InitServerRoomEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (Receive_Fired != null)
            Receive_Fired(this, new HMI.Main.Symbols.LFSerRoomFB.ReceiveEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_InitFinish(System.String SocketInit)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {SocketInit});
    }
    public bool FireEvent_InitFinish(HMI.Main.Symbols.LFSerRoomFB.InitFinishEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.SocketInit != null) _values_[0] = ea.SocketInit;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_InitFinish(System.String SocketInit, bool ignore_SocketInit)
    {
      object[] _values_ = new object[1];
      if (!ignore_SocketInit) _values_[0] = SocketInit;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_LF_ON(System.String DataOut)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {DataOut});
    }
    public bool FireEvent_LF_ON(HMI.Main.Symbols.LFSerRoomFB.LF_ONEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.DataOut != null) _values_[0] = ea.DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_LF_ON(System.String DataOut, bool ignore_DataOut)
    {
      object[] _values_ = new object[1];
      if (!ignore_DataOut) _values_[0] = DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_LF_OFF(System.String DataOut)
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {DataOut});
    }
    public bool FireEvent_LF_OFF(HMI.Main.Symbols.LFSerRoomFB.LF_OFFEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.DataOut != null) _values_[0] = ea.DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_LF_OFF(System.String DataOut, bool ignore_DataOut)
    {
      object[] _values_ = new object[1];
      if (!ignore_DataOut) _values_[0] = DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_GF_ON(System.String DataOut)
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {DataOut});
    }
    public bool FireEvent_GF_ON(HMI.Main.Symbols.LFSerRoomFB.GF_ONEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.DataOut != null) _values_[0] = ea.DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_GF_ON(System.String DataOut, bool ignore_DataOut)
    {
      object[] _values_ = new object[1];
      if (!ignore_DataOut) _values_[0] = DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_GF_OFF(System.String DataOut)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {DataOut});
    }
    public bool FireEvent_GF_OFF(HMI.Main.Symbols.LFSerRoomFB.GF_OFFEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.DataOut != null) _values_[0] = ea.DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_GF_OFF(System.String DataOut, bool ignore_DataOut)
    {
      object[] _values_ = new object[1];
      if (!ignore_DataOut) _values_[0] = DataOut;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}
#endregion #LFSerRoomFB_HMI;

#endregion Definitions;
