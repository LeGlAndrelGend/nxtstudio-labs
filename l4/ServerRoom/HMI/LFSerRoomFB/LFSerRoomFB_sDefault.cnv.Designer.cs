﻿/*
 * Created by nxtSTUDIO.
 * User: arader
 * Date: 2/6/2014
 * Time: 3:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.LFSerRoomFB
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.t_CPULoad = new NxtControl.GuiFramework.Tracker();
			this.CPULoad_Text = new NxtControl.GuiFramework.TextBox();
			this.freeText1 = new NxtControl.GuiFramework.FreeText();
			this.freeText2 = new NxtControl.GuiFramework.FreeText();
			this.rect_LF = new NxtControl.GuiFramework.Rectangle();
			this.rect_LFMove = new NxtControl.GuiFramework.Rectangle();
			this.freeText5 = new NxtControl.GuiFramework.FreeText();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.rect_CPU = new NxtControl.GuiFramework.Rectangle();
			this.freeText8 = new NxtControl.GuiFramework.FreeText();
			this.LF_LightOFF = new NxtControl.GuiFramework.Ellipse();
			this.LF_ON_OFF = new NxtControl.GuiFramework.FreeText();
			this.line1 = new NxtControl.GuiFramework.Line();
			this.freeText9 = new NxtControl.GuiFramework.FreeText();
			this.freeText10 = new NxtControl.GuiFramework.FreeText();
			this.freeText11 = new NxtControl.GuiFramework.FreeText();
			this.freeText12 = new NxtControl.GuiFramework.FreeText();
			this.rectangle3 = new NxtControl.GuiFramework.Rectangle();
			this.LF_LightON = new NxtControl.GuiFramework.Ellipse();
			this.freeText13 = new NxtControl.GuiFramework.FreeText();
			this.LF_Total_EC = new NxtControl.GuiFramework.TextBox();
			this.freeText15 = new NxtControl.GuiFramework.FreeText();
			this.Total_EC_All = new NxtControl.GuiFramework.TextBox();
			this.freeText17 = new NxtControl.GuiFramework.FreeText();
			this.Reset = new NxtControl.GuiFramework.DrawnButton();
			this.Report = new NxtControl.GuiFramework.DrawnButton();
			this.freeText18 = new NxtControl.GuiFramework.FreeText();
			this.rectangle6 = new NxtControl.GuiFramework.Rectangle();
			this.freeText21 = new NxtControl.GuiFramework.FreeText();
			this.freeText22 = new NxtControl.GuiFramework.FreeText();
			this.CPUTMin = new NxtControl.GuiFramework.TextBox();
			this.freeText25 = new NxtControl.GuiFramework.FreeText();
			this.CPUTMax = new NxtControl.GuiFramework.TextBox();
			this.freeText26 = new NxtControl.GuiFramework.FreeText();
			this.radioButton1 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton2 = new NxtControl.GuiFramework.RadioButton();
			this.radioButton3 = new NxtControl.GuiFramework.RadioButton();
			this.reSetToDefault = new NxtControl.GuiFramework.DrawnButton();
			this.freeText27 = new NxtControl.GuiFramework.FreeText();
			this.freeText28 = new NxtControl.GuiFramework.FreeText();
			this.freeText29 = new NxtControl.GuiFramework.FreeText();
			this.Run = new NxtControl.GuiFramework.DrawnButton();
			this.Total_EC = new NxtControl.GuiFramework.Rectangle();
			this.LF_ON_B = new NxtControl.GuiFramework.DrawnButton();
			this.LF_OFF_B = new NxtControl.GuiFramework.DrawnButton();
			this.freeText7 = new NxtControl.GuiFramework.FreeText();
			this.LFSpeed = new NxtControl.GuiFramework.ComboBox();
			this.freeText30 = new NxtControl.GuiFramework.FreeText();
			// 
			// t_CPULoad
			// 
			this.t_CPULoad.BeginInit();
			this.t_CPULoad.Bounds = new NxtControl.Drawing.RectF(((float)(48)), ((float)(388)), ((float)(240)), ((float)(65)));
			this.t_CPULoad.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(240)), ((byte)(240)), ((byte)(240))));
			this.t_CPULoad.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_CPULoad.Maximum = 100;
			this.t_CPULoad.Minimum = 0;
			this.t_CPULoad.Name = "t_CPULoad";
			this.t_CPULoad.Orientation = System.Windows.Forms.Orientation.Horizontal;
			this.t_CPULoad.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.TrackHandleBrush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(139))), new NxtControl.Drawing.GradientFill(NxtControl.Drawing.GradientFillOrientation.Center));
			this.t_CPULoad.TrackHandlePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.TrackLinePen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.t_CPULoad.ValueFont = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.t_CPULoad.ValueChanged += new System.EventHandler(this.T_CPULoadValueChanged);
			this.t_CPULoad.EndInit();
			// 
			// CPULoad_Text
			// 
			this.CPULoad_Text.Location = new System.Drawing.Point(307, 408);
			this.CPULoad_Text.Name = "CPULoad_Text";
			this.CPULoad_Text.Size = new System.Drawing.Size(87, 20);
			this.CPULoad_Text.TabIndex = 0;
			this.CPULoad_Text.Text = "10";
			// 
			// freeText1
			// 
			this.freeText1.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText1.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText1.Location = new NxtControl.Drawing.PointF(51, 364);
			this.freeText1.Name = "freeText1";
			this.freeText1.Text = "Change CPU Temprature";
			// 
			// freeText2
			// 
			this.freeText2.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText2.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText2.Location = new NxtControl.Drawing.PointF(302, 387);
			this.freeText2.Name = "freeText2";
			this.freeText2.Text = "CPU Temp";
			// 
			// rect_LF
			// 
			this.rect_LF.Bounds = new NxtControl.Drawing.RectF(((float)(37)), ((float)(110)), ((float)(111)), ((float)(65)));
			this.rect_LF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_LF.ImageName = "HMI:imagesnew.LF";
			this.rect_LF.Name = "rect_LF";
			// 
			// rect_LFMove
			// 
			this.rect_LFMove.Bounds = new NxtControl.Drawing.RectF(((float)(35)), ((float)(109)), ((float)(115)), ((float)(65)));
			this.rect_LFMove.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_LFMove.ImageName = "HMI:imagesnew.LF-move";
			this.rect_LFMove.Name = "rect_LFMove";
			this.rect_LFMove.Visible = false;
			// 
			// freeText5
			// 
			this.freeText5.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText5.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText5.Location = new NxtControl.Drawing.PointF(47, 86);
			this.freeText5.Name = "freeText5";
			this.freeText5.Text = "Local Fan";
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(35)), ((float)(79)), ((float)(371)), ((float)(160)));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			// 
			// rect_CPU
			// 
			this.rect_CPU.Bounds = new NxtControl.Drawing.RectF(((float)(171)), ((float)(110)), ((float)(224)), ((float)(60)));
			this.rect_CPU.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rect_CPU.ImageName = "HMI:imagesnew.server";
			this.rect_CPU.Name = "rect_CPU";
			// 
			// freeText8
			// 
			this.freeText8.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText8.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText8.Location = new NxtControl.Drawing.PointF(244, 85);
			this.freeText8.Name = "freeText8";
			this.freeText8.Text = "CPU";
			// 
			// LF_LightOFF
			// 
			this.LF_LightOFF.Bounds = new NxtControl.Drawing.RectF(((float)(112)), ((float)(179)), ((float)(22)), ((float)(20)));
			this.LF_LightOFF.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.LF_LightOFF.ImageName = "HMI:imagesnew.light-off";
			this.LF_LightOFF.Name = "LF_LightOFF";
			// 
			// LF_ON_OFF
			// 
			this.LF_ON_OFF.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.LF_ON_OFF.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_ON_OFF.Location = new NxtControl.Drawing.PointF(45, 181);
			this.LF_ON_OFF.Name = "LF_ON_OFF";
			this.LF_ON_OFF.Text = "OFF";
			// 
			// line1
			// 
			this.line1.EndPoint = new NxtControl.Drawing.PointF(150, 237);
			this.line1.Name = "line1";
			this.line1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.line1.StartPoint = new NxtControl.Drawing.PointF(150, 79);
			// 
			// freeText9
			// 
			this.freeText9.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText9.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText9.Location = new NxtControl.Drawing.PointF(46, 203);
			this.freeText9.Name = "freeText9";
			this.freeText9.Text = "Speed:";
			// 
			// freeText10
			// 
			this.freeText10.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText10.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText10.Location = new NxtControl.Drawing.PointF(166, 180);
			this.freeText10.Name = "freeText10";
			this.freeText10.Text = "Load:";
			// 
			// freeText11
			// 
			this.freeText11.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText11.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText11.Location = new NxtControl.Drawing.PointF(166, 199);
			this.freeText11.Name = "freeText11";
			this.freeText11.Text = "Heat:";
			// 
			// freeText12
			// 
			this.freeText12.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText12.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.freeText12.Location = new NxtControl.Drawing.PointF(165, 216);
			this.freeText12.Name = "freeText12";
			this.freeText12.Text = "Temp:";
			// 
			// rectangle3
			// 
			this.rectangle3.Bounds = new NxtControl.Drawing.RectF(((float)(36)), ((float)(246)), ((float)(369)), ((float)(330)));
			this.rectangle3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle3.Name = "rectangle3";
			// 
			// LF_LightON
			// 
			this.LF_LightON.Bounds = new NxtControl.Drawing.RectF(((float)(112)), ((float)(179)), ((float)(22)), ((float)(20)));
			this.LF_LightON.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.LF_LightON.ImageName = "HMI:imagesnew.light-on";
			this.LF_LightON.Name = "LF_LightON";
			this.LF_LightON.Visible = false;
			// 
			// freeText13
			// 
			this.freeText13.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText13.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText13.Location = new NxtControl.Drawing.PointF(693, 391);
			this.freeText13.Name = "freeText13";
			this.freeText13.Text = "Energy Related Info";
			// 
			// LF_Total_EC
			// 
			this.LF_Total_EC.Location = new System.Drawing.Point(670, 450);
			this.LF_Total_EC.Name = "LF_Total_EC";
			this.LF_Total_EC.Size = new System.Drawing.Size(287, 20);
			this.LF_Total_EC.TabIndex = 0;
			// 
			// freeText15
			// 
			this.freeText15.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText15.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText15.Location = new NxtControl.Drawing.PointF(655, 429);
			this.freeText15.Name = "freeText15";
			this.freeText15.Text = "Local Fans\' Total Energy Consumption";
			// 
			// Total_EC_All
			// 
			this.Total_EC_All.Location = new System.Drawing.Point(670, 552);
			this.Total_EC_All.Name = "Total_EC_All";
			this.Total_EC_All.Size = new System.Drawing.Size(287, 20);
			this.Total_EC_All.TabIndex = 0;
			// 
			// freeText17
			// 
			this.freeText17.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText17.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText17.Location = new NxtControl.Drawing.PointF(709, 529);
			this.freeText17.Name = "freeText17";
			this.freeText17.Text = "Total Energy Consumption";
			// 
			// Reset
			// 
			this.Reset.Bounds = new NxtControl.Drawing.RectF(((float)(573)), ((float)(510)), ((float)(69)), ((float)(29)));
			this.Reset.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Reset.Name = "Reset";
			this.Reset.Radius = 20;
			this.Reset.Text = "Reset";
			this.Reset.Click += new System.EventHandler(this.ResetClick);
			// 
			// Report
			// 
			this.Report.Bounds = new NxtControl.Drawing.RectF(((float)(573)), ((float)(467)), ((float)(69)), ((float)(29)));
			this.Report.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Report.Name = "Report";
			this.Report.Radius = 20;
			this.Report.Text = "Report";
			this.Report.Click += new System.EventHandler(this.ReportClick);
			// 
			// freeText18
			// 
			this.freeText18.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText18.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText18.Location = new NxtControl.Drawing.PointF(717, 55);
			this.freeText18.Name = "freeText18";
			this.freeText18.Text = "Control Strategy Setup";
			// 
			// rectangle6
			// 
			this.rectangle6.Bounds = new NxtControl.Drawing.RectF(((float)(626)), ((float)(80)), ((float)(347)), ((float)(304)));
			this.rectangle6.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle6.Name = "rectangle6";
			// 
			// freeText21
			// 
			this.freeText21.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText21.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText21.Location = new NxtControl.Drawing.PointF(639, 179);
			this.freeText21.Name = "freeText21";
			this.freeText21.Text = "4- CPU Temp";
			// 
			// freeText22
			// 
			this.freeText22.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText22.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText22.Location = new NxtControl.Drawing.PointF(639, 204);
			this.freeText22.Name = "freeText22";
			this.freeText22.Text = "5- Controller Type";
			// 
			// CPUTMin
			// 
			this.CPUTMin.Location = new System.Drawing.Point(822, 178);
			this.CPUTMin.Name = "CPUTMin";
			this.CPUTMin.Size = new System.Drawing.Size(41, 20);
			this.CPUTMin.TabIndex = 0;
			this.CPUTMin.Text = "10";
			// 
			// freeText25
			// 
			this.freeText25.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText25.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText25.Location = new NxtControl.Drawing.PointF(783, 179);
			this.freeText25.Name = "freeText25";
			this.freeText25.Text = "Min:";
			// 
			// CPUTMax
			// 
			this.CPUTMax.Location = new System.Drawing.Point(917, 178);
			this.CPUTMax.Name = "CPUTMax";
			this.CPUTMax.Size = new System.Drawing.Size(41, 20);
			this.CPUTMax.TabIndex = 0;
			this.CPUTMax.Text = "50";
			// 
			// freeText26
			// 
			this.freeText26.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText26.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText26.Location = new NxtControl.Drawing.PointF(875, 179);
			this.freeText26.Name = "freeText26";
			this.freeText26.Text = "Max:";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton1.Location = new System.Drawing.Point(669, 232);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(77, 24);
			this.radioButton1.TabIndex = 3;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "PID \r";
			// 
			// radioButton2
			// 
			this.radioButton2.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton2.Location = new System.Drawing.Point(669, 259);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(78, 24);
			this.radioButton2.TabIndex = 3;
			this.radioButton2.Text = "MPC";
			// 
			// radioButton3
			// 
			this.radioButton3.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.radioButton3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(14)))), ((int)(((byte)(18)))));
			this.radioButton3.Location = new System.Drawing.Point(668, 287);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(77, 24);
			this.radioButton3.TabIndex = 3;
			this.radioButton3.Text = "VFD";
			// 
			// reSetToDefault
			// 
			this.reSetToDefault.Bounds = new NxtControl.Drawing.RectF(((float)(733)), ((float)(337)), ((float)(137)), ((float)(29)));
			this.reSetToDefault.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.reSetToDefault.Name = "reSetToDefault";
			this.reSetToDefault.Radius = 20;
			this.reSetToDefault.Text = "RESET To Default";
			this.reSetToDefault.Click += new System.EventHandler(this.ReSetToDefaultClick);
			// 
			// freeText27
			// 
			this.freeText27.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText27.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText27.Location = new NxtControl.Drawing.PointF(764, 236);
			this.freeText27.Name = "freeText27";
			this.freeText27.Text = "(Proportional Integral Derivative)";
			// 
			// freeText28
			// 
			this.freeText28.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText28.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText28.Location = new NxtControl.Drawing.PointF(765, 262);
			this.freeText28.Name = "freeText28";
			this.freeText28.Text = "(Model Predictive Control)";
			// 
			// freeText29
			// 
			this.freeText29.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText29.Font = new NxtControl.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText29.Location = new NxtControl.Drawing.PointF(765, 291);
			this.freeText29.Name = "freeText29";
			this.freeText29.Text = "(Variable Frequency Drive)";
			// 
			// Run
			// 
			this.Run.Bounds = new NxtControl.Drawing.RectF(((float)(306)), ((float)(449)), ((float)(88)), ((float)(38)));
			this.Run.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.Run.Name = "Run";
			this.Run.Radius = 20;
			this.Run.Text = "Run";
			this.Run.Click += new System.EventHandler(this.RunClick);
			// 
			// Total_EC
			// 
			this.Total_EC.Bounds = new NxtControl.Drawing.RectF(((float)(564)), ((float)(420)), ((float)(411)), ((float)(167)));
			this.Total_EC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Total_EC.Name = "Total_EC";
			// 
			// LF_ON_B
			// 
			this.LF_ON_B.Bounds = new NxtControl.Drawing.RectF(((float)(50)), ((float)(257)), ((float)(147)), ((float)(29)));
			this.LF_ON_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_ON_B.Name = "LF_ON_B";
			this.LF_ON_B.Radius = 20;
			this.LF_ON_B.Text = "Local Fan ON";
			this.LF_ON_B.Click += new System.EventHandler(this.LF_ON_BClick);
			// 
			// LF_OFF_B
			// 
			this.LF_OFF_B.Bounds = new NxtControl.Drawing.RectF(((float)(244)), ((float)(256)), ((float)(147)), ((float)(29)));
			this.LF_OFF_B.Enabled = false;
			this.LF_OFF_B.Font = new NxtControl.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.LF_OFF_B.Name = "LF_OFF_B";
			this.LF_OFF_B.Radius = 20;
			this.LF_OFF_B.Text = "Local Fan OFF";
			this.LF_OFF_B.Click += new System.EventHandler(this.LF_OFF_BClick);
			// 
			// freeText7
			// 
			this.freeText7.Color = new NxtControl.Drawing.Color(((byte)(178)), ((byte)(14)), ((byte)(18)));
			this.freeText7.Font = new NxtControl.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
			this.freeText7.Location = new NxtControl.Drawing.PointF(178, 56);
			this.freeText7.Name = "freeText7";
			this.freeText7.Text = "Server#1";
			// 
			// LFSpeed
			// 
			this.LFSpeed.Items.Add("1");
			this.LFSpeed.Items.Add("2");
			this.LFSpeed.Items.Add("3");
			this.LFSpeed.Location = new System.Drawing.Point(820, 120);
			this.LFSpeed.Name = "LFSpeed";
			this.LFSpeed.Size = new System.Drawing.Size(47, 21);
			this.LFSpeed.TabIndex = 4;
			this.LFSpeed.Text = "2";
			// 
			// freeText30
			// 
			this.freeText30.Color = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.freeText30.Font = new NxtControl.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
			this.freeText30.Location = new NxtControl.Drawing.PointF(636, 120);
			this.freeText30.Name = "freeText30";
			this.freeText30.Text = "2- LF Speed";
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle3,
									this.rectangle1,
									this.t_CPULoad,
									this.CPULoad_Text,
									this.freeText1,
									this.freeText2,
									this.rect_LF,
									this.rect_LFMove,
									this.freeText5,
									this.freeText7,
									this.rect_CPU,
									this.freeText8,
									this.LF_LightOFF,
									this.LF_ON_OFF,
									this.line1,
									this.freeText9,
									this.freeText10,
									this.freeText11,
									this.freeText12,
									this.LF_LightON,
									this.Total_EC,
									this.freeText13,
									this.LF_Total_EC,
									this.freeText15,
									this.Total_EC_All,
									this.freeText17,
									this.Reset,
									this.Report,
									this.freeText18,
									this.rectangle6,
									this.freeText21,
									this.freeText22,
									this.CPUTMin,
									this.freeText25,
									this.CPUTMax,
									this.freeText26,
									this.radioButton1,
									this.radioButton2,
									this.radioButton3,
									this.reSetToDefault,
									this.freeText27,
									this.freeText28,
									this.freeText29,
									this.freeText30,
									this.LFSpeed,
									this.Run,
									this.LF_ON_B,
									this.LF_OFF_B});
			this.SymbolSize = new System.Drawing.Size(1005, 714);
		}
		private NxtControl.GuiFramework.DrawnButton reSetToDefault;
		private NxtControl.GuiFramework.DrawnButton LF_OFF_B;
		private NxtControl.GuiFramework.DrawnButton LF_ON_B;
		private NxtControl.GuiFramework.TextBox Total_EC_All;
		private NxtControl.GuiFramework.Rectangle Total_EC;
		private NxtControl.GuiFramework.TextBox LF_Total_EC;
		private NxtControl.GuiFramework.DrawnButton Run;
		private NxtControl.GuiFramework.TextBox CPUTMin;
		private NxtControl.GuiFramework.TextBox CPUTMax;
		private NxtControl.GuiFramework.ComboBox LFSpeed;
		private NxtControl.GuiFramework.FreeText freeText30;
		private NxtControl.GuiFramework.FreeText freeText29;
		private NxtControl.GuiFramework.FreeText freeText28;
		private NxtControl.GuiFramework.FreeText freeText27;
		private NxtControl.GuiFramework.RadioButton radioButton3;
		private NxtControl.GuiFramework.RadioButton radioButton2;
		private NxtControl.GuiFramework.RadioButton radioButton1;
		private NxtControl.GuiFramework.FreeText freeText26;
		private NxtControl.GuiFramework.FreeText freeText25;
		private NxtControl.GuiFramework.FreeText freeText22;
		private NxtControl.GuiFramework.FreeText freeText21;
		private NxtControl.GuiFramework.Rectangle rectangle6;
		private NxtControl.GuiFramework.FreeText freeText18;
		private NxtControl.GuiFramework.DrawnButton Report;
		private NxtControl.GuiFramework.DrawnButton Reset;
		private NxtControl.GuiFramework.FreeText freeText17;
		private NxtControl.GuiFramework.FreeText freeText15;
		private NxtControl.GuiFramework.FreeText freeText13;
		private NxtControl.GuiFramework.Ellipse LF_LightON;
		private NxtControl.GuiFramework.Ellipse LF_LightOFF;
		private NxtControl.GuiFramework.Rectangle rectangle3;
		private NxtControl.GuiFramework.FreeText freeText12;
		private NxtControl.GuiFramework.FreeText freeText11;
		private NxtControl.GuiFramework.FreeText freeText10;
		private NxtControl.GuiFramework.FreeText freeText9;
		private NxtControl.GuiFramework.Line line1;
		private NxtControl.GuiFramework.FreeText LF_ON_OFF;
		private NxtControl.GuiFramework.FreeText freeText8;
		private NxtControl.GuiFramework.Rectangle rect_CPU;
		private NxtControl.GuiFramework.FreeText freeText7;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtControl.GuiFramework.FreeText freeText5;
		private NxtControl.GuiFramework.Rectangle rect_LFMove;
		private NxtControl.GuiFramework.Rectangle rect_LF;
		private NxtControl.GuiFramework.FreeText freeText2;
		private NxtControl.GuiFramework.FreeText freeText1;
		private NxtControl.GuiFramework.Tracker t_CPULoad;
		private NxtControl.GuiFramework.TextBox CPULoad_Text;
		#endregion
	}
}
