﻿/*
 * Created by nxtSTUDIO.
 * User: Alto
 * Date: 09/26/2017
 * Time: 8:15 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace HMI.Main.Symbols.LFSerRoomFB
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
	  /*--declare global variables--*/
	  bool LF_State_ON = false;
	  int LF_EC_Total = 0;
	  int C1 = 3;
		public sDefault()
		{
			// The InitializeComponent() call is required for Windows Forms designer support.
			InitializeComponent();
      this.InitServerRoom_Fired +=new EventHandler <LFSerRoomFB.InitServerRoomEventArgs>(InitServerRoom);
			this.Receive_Fired += new EventHandler <LFSerRoomFB.ReceiveEventArgs> (Receive);
		}
		public void InitServerRoom(object Sender, LFSerRoomFB.InitServerRoomEventArgs sr){}		
    public void Receive(object Sender, LFSerRoomFB.ReceiveEventArgs r){}

    /*--Run button--*/
    void RunClick(object sender, EventArgs e)
		{
		  t_CPULoad.Value = double.Parse (CPULoad_Text.Text);
		}
    /*--Change image static/animated--*/
    /*--Light on/off when fun is turned on/off--*/
    void LF_ON_BClick(object sender, EventArgs e)
		{
      LFTurnOn();
      LF_ON_B.Enabled = false;
      LF_OFF_B.Enabled = true;
		}
    public void LFTurnOn()
    {
      this.FireEvent_LF_ON("LF_ON");  // ??? for what is it?
      LF_State_ON = true;
      rect_LF.Visible = false;
      rect_LFMove.Visible = true;
      LF_ON_OFF.Text = "ON";
      LF_LightON.Visible = true;
      LF_LightOFF.Visible = false;
    }
    
    void LF_OFF_BClick(object sender, EventArgs e)
		{
      LFTurnOff();
      LF_ON_B.Enabled = true;
      LF_OFF_B.Enabled = false;
		}
    public void LFTurnOff()
    {
      this.FireEvent_LF_OFF("LF_OFF");
      LF_State_ON = false;
      rect_LF.Visible = true;
      rect_LFMove.Visible = false;
      LF_ON_OFF.Text = "OFF";
      LF_LightON.Visible = false;
      LF_LightOFF.Visible = true;
    }
  
    /*--Working with Tracker--*/
    /*--Change temperature by using tracker, turn on fan if not default temp range--*/
    /*--Count electricity consumption LF and GF when turn funs on by trackers--*/
    void T_CPULoadValueChanged(object sender, EventArgs e)
		{
      CPULoad_Text.Text = t_CPULoad.Value.ToString();
      
      if (t_CPULoad.Value > double.Parse(CPUTMax.Text) && !LF_State_ON)
      {
        LFTurnOn();
        Thread threadCPU = new Thread( new ThreadStart( moveCPULoad ));
        threadCPU.Start();
        LF_EC_Total += C1;   //increase total EC consume by LF
      } else if (t_CPULoad.Value == double.Parse(CPUTMax.Text) && LF_State_ON)
        {
          LFTurnOff();
        }
		}
    
    public void moveCPULoad()
    {
      moveTrackerDefault(ref t_CPULoad, ref CPULoad_Text, ref CPUTMin);
    }
    
    public void moveTrackerDefault(ref NxtControl.GuiFramework.Tracker tracker, ref NxtControl.GuiFramework.TextBox trackerText, ref NxtControl.GuiFramework.TextBox tempMin)
    {
      try 
      {
        string[] parts = trackerText.Text.Split('.');
        int textValue = int.Parse(parts[0]);
        tracker.Value = Convert.ToDouble(textValue);
        Thread.Sleep(500);
        for (int curVal = textValue; curVal >= Convert.ToInt32(double.Parse(tempMin.Text)); curVal--)
        {
          tracker.Value = Convert.ToDouble(curVal);
          Thread.Sleep(100);
        }
      } catch( Exception ex) {}
    }
    
	  /*--Reset trackers parameters to default--*/
	  void ReSetToDefaultClick(object sender, EventArgs e)
		{
			LFSpeed.Text = "2";
			CPUTMin.Text = "10";
			CPUTMax.Text = "50";
		}

    void ResetClick(object sender, EventArgs e)
		{
		  t_CPULoad.Value = double.Parse (CPUTMin.Text);
		  CPULoad_Text.Text = CPUTMin.Text;
		  LF_EC_Total = 0;
      LF_Total_EC.Clear();
      Total_EC_All.Clear();
		}
    
    /*--Count total electricity consumption LF and GF--*/
    /*--Output info about consumption electricity by click to Report button--*/
    void ReportClick(object sender, EventArgs e)
		{
			LF_Total_EC.Text = LF_EC_Total.ToString();
			Total_EC_All.Text = (LF_EC_Total).ToString();
		}
	}
}
