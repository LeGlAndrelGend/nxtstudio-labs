/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 8/12/2014
 * Time: 7:24 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.PANEL
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btStart = new HMI.Main.Symbols.BUTTON.HMI();
			this.btStop = new HMI.Main.Symbols.BUTTON.HMI();
			this.ledStart = new HMI.Main.Symbols.LED.HMI();
			this.btResume = new HMI.Main.Symbols.BUTTON.HMI();
			this.AutoSwitch = new HMI.Main.Symbols.CHKBOX.HMI();
			this.ledQ1 = new HMI.Main.Symbols.LED.HMI();
			this.ledQ2 = new HMI.Main.Symbols.LED.HMI();
			// 
			// btStart
			// 
			this.btStart.BeginInit();
			this.btStart.AngleIgnore = false;
			this.btStart.DesignTransformation = new NxtControl.Drawing.Matrix(0.64210526315789473, 0, 0, 1, 39, 29);
			this.btStart.Name = "btStart";
			this.btStart.SecurityToken = ((uint)(4294967295u));
			this.btStart.TagName = "btStart";
			this.btStart.EndInit();
			// 
			// btStop
			// 
			this.btStop.BeginInit();
			this.btStop.AngleIgnore = false;
			this.btStop.DesignTransformation = new NxtControl.Drawing.Matrix(0.56842105263157894, 0, 0, 1, 168, 32);
			this.btStop.Name = "btStop";
			this.btStop.SecurityToken = ((uint)(4294967295u));
			this.btStop.TagName = "btStop";
			this.btStop.EndInit();
			// 
			// ledStart
			// 
			this.ledStart.BeginInit();
			this.ledStart.AngleIgnore = false;
			this.ledStart.DesignTransformation = new NxtControl.Drawing.Matrix(0.52272727272727271, 0, 0, 0.48837209302325574, 17, 30);
			this.ledStart.Name = "ledStart";
			this.ledStart.SecurityToken = ((uint)(4294967295u));
			this.ledStart.TagName = "ledStart";
			this.ledStart.EndInit();
			// 
			// btResume
			// 
			this.btResume.BeginInit();
			this.btResume.AngleIgnore = false;
			this.btResume.DesignTransformation = new NxtControl.Drawing.Matrix(0.66315789473684217, 0, 0, 0.975, 39, 79);
			this.btResume.Name = "btResume";
			this.btResume.SecurityToken = ((uint)(4294967295u));
			this.btResume.TagName = "btResume";
			this.btResume.EndInit();
			// 
			// AutoSwitch
			// 
			this.AutoSwitch.BeginInit();
			this.AutoSwitch.AngleIgnore = false;
			this.AutoSwitch.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 172, 85);
			this.AutoSwitch.Name = "AutoSwitch";
			this.AutoSwitch.SecurityToken = ((uint)(4294967295u));
			this.AutoSwitch.TagName = "AutoSwitch";
			this.AutoSwitch.EndInit();
			// 
			// ledQ1
			// 
			this.ledQ1.BeginInit();
			this.ledQ1.AngleIgnore = false;
			this.ledQ1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 44, 155);
			this.ledQ1.Name = "ledQ1";
			this.ledQ1.SecurityToken = ((uint)(4294967295u));
			this.ledQ1.TagName = "ledQ1";
			this.ledQ1.EndInit();
			// 
			// ledQ2
			// 
			this.ledQ2.BeginInit();
			this.ledQ2.AngleIgnore = false;
			this.ledQ2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 171, 155);
			this.ledQ2.Name = "ledQ2";
			this.ledQ2.SecurityToken = ((uint)(4294967295u));
			this.ledQ2.TagName = "ledQ2";
			this.ledQ2.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.btStart,
									this.btStop,
									this.ledStart,
									this.btResume,
									this.AutoSwitch,
									this.ledQ1,
									this.ledQ2});
			this.SymbolSize = new System.Drawing.Size(257, 229);
		}
		private HMI.Main.Symbols.LED.HMI ledQ2;
		private HMI.Main.Symbols.LED.HMI ledQ1;
		private HMI.Main.Symbols.CHKBOX.HMI AutoSwitch;
		private HMI.Main.Symbols.BUTTON.HMI btResume;
		private HMI.Main.Symbols.LED.HMI ledStart;
		private HMI.Main.Symbols.BUTTON.HMI btStop;
		private HMI.Main.Symbols.BUTTON.HMI btStart;
		#endregion
	}
}
