/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 8/13/2014
 * Time: 7:13 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.CHKBOX
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void SwitchCheckedChanged(object sender, EventArgs e)
		{
		  if (Switch.Checked){
		    FireEvent_CNF(true);
		  }
		  else
		    FireEvent_CNF(false);
		}
	}
}
