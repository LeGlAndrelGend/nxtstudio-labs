/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 8/13/2014
 * Time: 7:13 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.CHKBOX
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Switch = new NxtControl.GuiFramework.TwoStateButton();
			// 
			// Switch
			// 
			this.Switch.Bounds = new NxtControl.Drawing.RectF(((float)(11)), ((float)(8)), ((float)(33)), ((float)(30)));
			this.Switch.DisabledTrueImageStream = null;
			this.Switch.DrawStyle = NxtControl.GuiFramework.TwoStateButton.ButtonDrawStyle.CheckBox;
			this.Switch.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Switch.Name = "Switch";
			this.Switch.Radius = 20;
			this.Switch.CheckedChanged += new System.EventHandler(this.SwitchCheckedChanged);
			// 
			// HMI
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.Switch});
			this.SymbolSize = new System.Drawing.Size(52, 45);
		}
		private NxtControl.GuiFramework.TwoStateButton Switch;
		#endregion
	}
}
