/*
 * Created by nxtSTUDIO.
 * User: valvya
 * Date: 8/13/2014
 * Time: 7:13 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #CHKBOX_HMI;

namespace HMI.Main.Symbols.CHKBOX
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_VAL(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.Boolean) var;
      return ret;
    }

    public System.Boolean? VAL
    { get {
      if (accessorService == null)
        return null;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.Boolean) var;
    }  }


  }

}

namespace HMI.Main.Symbols.CHKBOX
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.Boolean? RES_field = null;
    public System.Boolean? RES
    {
       get { return RES_field; }
       set { RES_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.CHKBOX
{
  partial class HMI
  {

    private event EventHandler<Symbols.CHKBOX.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new Symbols.CHKBOX.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean RES)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {RES});
    }
    public bool FireEvent_CNF(Symbols.CHKBOX.CNFEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.RES.HasValue) _values_[0] = ea.RES.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean RES, bool ignore_RES)
    {
      object[] _values_ = new object[1];
      if (!ignore_RES) _values_[0] = RES;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #CHKBOX_HMI;

#endregion Definitions;
