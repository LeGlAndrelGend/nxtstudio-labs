﻿/*
 * Created by nxtSTUDIO.
 * User: cheyan
 * Date: 9/30/2014
 * Time: 5:09 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for PanelCanvas.
	/// </summary>
	partial class PanelCanvas
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.sDefault1 = new HMI.Main.Symbols.HCylinderLS.sDefault();
			this.sDefault2 = new HMI.Main.Symbols.VCylinderLS.sDefault();
			this.sDefault3 = new HMI.Main.Symbols.PANEL.sDefault();
			// 
			// sDefault1
			// 
			this.sDefault1.BeginInit();
			this.sDefault1.AngleIgnore = false;
			this.sDefault1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 257, 26);
			this.sDefault1.Name = "sDefault1";
			this.sDefault1.SecurityToken = ((uint)(4294967295u));
			this.sDefault1.TagName = "F8601AA3EECC6B1A";
			this.sDefault1.EndInit();
			// 
			// sDefault2
			// 
			this.sDefault2.BeginInit();
			this.sDefault2.AngleIgnore = false;
			this.sDefault2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 11, 29);
			this.sDefault2.Name = "sDefault2";
			this.sDefault2.SecurityToken = ((uint)(4294967295u));
			this.sDefault2.TagName = "E7AAC0C5996B018B";
			this.sDefault2.EndInit();
			// 
			// sDefault3
			// 
			this.sDefault3.BeginInit();
			this.sDefault3.AngleIgnore = false;
			this.sDefault3.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 281, 232);
			this.sDefault3.Name = "sDefault3";
			this.sDefault3.SecurityToken = ((uint)(4294967295u));
			this.sDefault3.TagName = "919601DD00F60EA9";
			this.sDefault3.EndInit();
			// 
			// PanelCanvas
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(600)), ((float)(450)));
			this.Name = "PanelCanvas";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.sDefault1,
									this.sDefault2,
									this.sDefault3});
			this.Size = new System.Drawing.Size(600, 450);
		}
		private HMI.Main.Symbols.PANEL.sDefault sDefault3;
		private HMI.Main.Symbols.VCylinderLS.sDefault sDefault2;
		private HMI.Main.Symbols.HCylinderLS.sDefault sDefault1;
		#endregion
	}
}
