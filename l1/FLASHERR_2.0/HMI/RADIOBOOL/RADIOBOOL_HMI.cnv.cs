﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 24/03/2012
 * Time: 7:55 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.RADIOBOOL
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			//btStart.Text=LABEL1.Text;
			//btStop.Text =LABEL0.Text;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void BtStartCheckedChanged(object sender, EventArgs e)
		{
		  if (btStart.Checked) {
		    FireEvent_IND(true);
		  }
		}
		
		void BtStopCheckedChanged(object sender, EventArgs e)
		{
			if (btStop.Checked) {
		    FireEvent_IND(false);
		  }
		}
		
		void LABEL0ValueChanged(object sender, ValueChangedEventArgs e)
		{
		  btStop.Text =(string)LABEL0.Value;
		}
		
		void LABEL1ValueChanged(object sender, ValueChangedEventArgs e)
		{
		  btStart.Text=(string)LABEL1.Value;
		}
		
		void INVValueChanged(object sender, ValueChangedEventArgs e)
		{
		  btStart.Checked=((bool)INV.Value);
		  btStop.Checked=!btStart.Checked;
		}
	}
}
