/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 24/03/2012
 * Time: 7:55 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #RADIOBOOL_HMI;

namespace NxtStudio.Symbols.RADIOBOOL
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_LABEL1(ref System.String value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref value);
    }
    public bool Get_LABEL0(ref System.String value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref value);
    }
    public bool Get_INV(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetBoolValue(channelId, cookie, eventIndex, true,2, ref value);
    }

  }

}

namespace NxtStudio.Symbols.RADIOBOOL
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.RADIOBOOL.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new NxtStudio.Symbols.RADIOBOOL.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_IND(System.Boolean OUT)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {OUT});
    }
    public bool FireEvent_IND(System.Boolean OUT, bool ignore_OUT)
    {
      object[] _values_ = new object[1];
      if (!ignore_OUT) _values_[0] = OUT;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }

  }
}
#endregion #RADIOBOOL_HMI;

#endregion Definitions;
