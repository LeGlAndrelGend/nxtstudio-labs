/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 16/11/2012
 * Time: 12:05 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace NxtStudio.Canvases
{
	/// <summary>
	/// Summary description for LIGHTS.
	/// </summary>
	partial class LIGHTS
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LEDS = new NxtStudio.Symbols.LEDHMI.HMI();
			// 
			// LEDS
			// 
			this.LEDS.BeginInit();
			this.LEDS.AngleIgnore = false;
			this.LEDS.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 19, 78);
			this.LEDS.Name = "LEDS";
			this.LEDS.SecurityToken = ((uint)(4294967295u));
			this.LEDS.TagName = "4E4BD22FC0F80372";
			this.LEDS.EndInit();
			// 
			// LIGHTS
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(192)), ((float)(134)));
			this.Name = "LIGHTS";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.LEDS});
			this.Size = new System.Drawing.Size(192, 134);
		}
		private NxtStudio.Symbols.LEDHMI.HMI LEDS;
		#endregion
	}
}
