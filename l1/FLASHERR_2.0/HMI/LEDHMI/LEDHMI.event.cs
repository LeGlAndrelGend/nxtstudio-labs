/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 9:09 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #LEDHMI_HMI;

namespace NxtStudio.Symbols.LEDHMI
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_LED3(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetBoolValue(channelId, cookie, eventIndex, true,0, ref value);
    }
    public bool Get_LED2(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetBoolValue(channelId, cookie, eventIndex, true,1, ref value);
    }
    public bool Get_LED1(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetBoolValue(channelId, cookie, eventIndex, true,2, ref value);
    }
    public bool Get_LED0(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetBoolValue(channelId, cookie, eventIndex, true,3, ref value);
    }

  }

}

namespace NxtStudio.Symbols.LEDHMI
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.LEDHMI.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new NxtStudio.Symbols.LEDHMI.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }

  }
}
#endregion #LEDHMI_HMI;

#endregion Definitions;
