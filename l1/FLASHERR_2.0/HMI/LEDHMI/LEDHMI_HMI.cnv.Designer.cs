﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 9:09 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.LEDHMI
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LED3 = new NxtStudio.Symbols.Led();
			this.LED2 = new NxtStudio.Symbols.Led();
			this.led1 = new NxtStudio.Symbols.Led();
			this.LED0 = new NxtStudio.Symbols.Led();
			// 
			// LED3
			// 
			this.LED3.BeginInit();
			this.LED3.AngleIgnore = false;
			this.LED3.ColorFrame = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.LED3.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.LED3.ColorOn = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(255)), ((byte)(0)));
			this.LED3.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 5, 5);
			this.LED3.Frame = 2;
			this.LED3.Name = "LED3";
			this.LED3.TagName = "LED3";
			this.LED3.EndInit();
			// 
			// LED2
			// 
			this.LED2.BeginInit();
			this.LED2.AngleIgnore = false;
			this.LED2.ColorFrame = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.LED2.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.LED2.ColorOn = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(255)), ((byte)(0)));
			this.LED2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 43, 5);
			this.LED2.Frame = 2;
			this.LED2.Name = "LED2";
			this.LED2.TagName = "LED2";
			this.LED2.EndInit();
			// 
			// led1
			// 
			this.led1.BeginInit();
			this.led1.AngleIgnore = false;
			this.led1.ColorFrame = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.led1.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.led1.ColorOn = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(255)), ((byte)(0)));
			this.led1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 81, 5);
			this.led1.Frame = 2;
			this.led1.Name = "led1";
			this.led1.TagName = "LED1";
			this.led1.EndInit();
			// 
			// LED0
			// 
			this.LED0.BeginInit();
			this.LED0.AngleIgnore = false;
			this.LED0.ColorFrame = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0)));
			this.LED0.ColorOff = new NxtControl.Drawing.Color(((byte)(90)), ((byte)(90)), ((byte)(90)));
			this.LED0.ColorOn = new NxtControl.Drawing.Color(((byte)(0)), ((byte)(255)), ((byte)(0)));
			this.LED0.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 118, 5);
			this.LED0.Frame = 2;
			this.LED0.Name = "LED0";
			this.LED0.TagName = "LED0";
			this.LED0.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.LED3,
									this.LED2,
									this.led1,
									this.LED0});
			this.SymbolSize = new System.Drawing.Size(150, 36);
		}
		private NxtStudio.Symbols.Led LED0;
		private NxtStudio.Symbols.Led led1;
		private NxtStudio.Symbols.Led LED2;
		private NxtStudio.Symbols.Led LED3;
		#endregion
	}
}
