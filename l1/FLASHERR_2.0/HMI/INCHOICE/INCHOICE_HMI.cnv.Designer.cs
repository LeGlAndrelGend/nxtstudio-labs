﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 1:08 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INCHOICE
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.I = new NxtStudio.Symbols.Execute<ushort>();
			this.CHOICES = new NxtStudio.Symbols.Execute<string>();
			this.comboBox1 = new NxtControl.GuiFramework.ComboBox();
			// 
			// I
			// 
			this.I.BeginInit();
			this.I.AngleIgnore = false;
			this.I.DesignTransformation = new NxtControl.Drawing.Matrix(0.31, 0, 0, 1, 117, 56);
			this.I.Name = "I";
			this.I.TagName = "I";
			this.I.Value = ((ushort)(0));
			this.I.EndInit();
			// 
			// CHOICES
			// 
			this.CHOICES.BeginInit();
			this.CHOICES.AngleIgnore = false;
			this.CHOICES.DesignTransformation = new NxtControl.Drawing.Matrix(0.61, 0, 0, 1, 20, 57);
			this.CHOICES.IsOnlyInput = true;
			this.CHOICES.Name = "CHOICES";
			this.CHOICES.TagName = "CHOICES";
			this.CHOICES.Value = null;
			this.CHOICES.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.CHOICESValueChanged);
			this.CHOICES.EndInit();
			// 
			// comboBox1
			// 
			this.comboBox1.Items.Add("FLASH_ALL");
			this.comboBox1.Items.Add("COUNT_UP");
			this.comboBox1.Items.Add("COUNT_DOWN");
			this.comboBox1.Items.Add("CHASE_UP");
			this.comboBox1.Items.Add("CHASE_DOWN");
			this.comboBox1.Location = new System.Drawing.Point(7, 6);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(121, 21);
			this.comboBox1.TabIndex = 0;
			this.comboBox1.Text = "comboBox1";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.I,
									this.CHOICES,
									this.comboBox1});
			this.SymbolSize = new System.Drawing.Size(167, 79);
		}
		private NxtControl.GuiFramework.ComboBox comboBox1;
		private NxtStudio.Symbols.Execute<ushort> I;
		private NxtStudio.Symbols.Execute<string> CHOICES;
		#endregion
	}
}
