/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 1:08 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #INCHOICE_HMI;

namespace NxtStudio.Symbols.INCHOICE
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_CHOICES(ref System.String value)
    {
      if (accessorService == null)
        return false;
      return accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref value);
    }

  }

}

namespace NxtStudio.Symbols.INCHOICE
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.INCHOICE.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new NxtStudio.Symbols.INCHOICE.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.UInt16 I)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {I});
    }
    public bool FireEvent_CNF(System.UInt16 I, bool ignore_I)
    {
      object[] _values_ = new object[1];
      if (!ignore_I) _values_[0] = I;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CHQ()
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {});
    }

  }
}
#endregion #INCHOICE_HMI;

#endregion Definitions;
