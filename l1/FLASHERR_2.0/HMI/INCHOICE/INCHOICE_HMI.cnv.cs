﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 1:08 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INCHOICE
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		void CHOICESValueChanged(object sender, ValueChangedEventArgs e)
		{
		  comboBox1.SelectedIndex=0;
		  I.Value = 0;
		  FireEvent_CHQ();
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
		  //I.Value = (uint)comboBox1.SelectedIndex;
		  //FireEvent_CNF((ushort)I.Value);
		  FireEvent_CNF((ushort)comboBox1.SelectedIndex);
		}
	}
}
