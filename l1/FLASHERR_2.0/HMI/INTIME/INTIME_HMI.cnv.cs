﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 10:01 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtStudio.Symbols;
using NxtControl.Services;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INTIME
{
	/// <summary>
	/// Description of HMI.
	/// </summary>
	public partial class HMI : NxtControl.GuiFramework.HMISymbol
	{
		public HMI()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		void MINMSValueChanged(object sender, ValueChangedEventArgs e)
		{
		  //OUT.Value=(uint)MINMS.Value;
		  //FireEvent_CNF((int)OUT.Value);
		  FireEvent_CNF((int)200);
		}
		
		void MAXMSValueChanged(object sender, ValueChangedEventArgs e)
		{
		  //OUT.Value=(uint)MINMS.Value;
		  //FireEvent_CNF((int)OUT.Value);
		  FireEvent_CNF((int)200);
  	}
	}
}
