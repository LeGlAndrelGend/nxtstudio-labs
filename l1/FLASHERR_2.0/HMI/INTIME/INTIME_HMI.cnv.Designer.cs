﻿/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 10:01 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtStudio.Symbols;
using NxtControl.GuiFramework;

namespace NxtStudio.Symbols.INTIME
{
	/// <summary>
	/// Summary description for HMI.
	/// </summary>
	partial class HMI
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.OUT = new NxtStudio.Symbols.RoundKnob<int>();
			this.MINMS = new NxtStudio.Symbols.Execute<int>();
			this.MAXMS = new NxtStudio.Symbols.Execute<int>();
			// 
			// OUT
			// 
			this.OUT.BeginInit();
			this.OUT.AngleIgnore = false;
			this.OUT.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))));
			this.OUT.DesignTransformation = new NxtControl.Drawing.Matrix(0.625, 0, 0, 0.62500000000000011, 34, 54.000000000000007);
			this.OUT.KnobColor = System.Drawing.Color.Red;
			this.OUT.LabelFormatString = "{0,0:F2}";
			this.OUT.Maximum = 100;
			this.OUT.MaximumTag = new NxtControl.GuiFramework.AuxHMIAccessor("MAXMS", this.OUT);
			this.OUT.Minimum = 0;
			this.OUT.MinimumTag = new NxtControl.GuiFramework.AuxHMIAccessor("MINMS", this.OUT);
			this.OUT.Name = "OUT";
			this.OUT.NeedleColor = System.Drawing.Color.Yellow;
			this.OUT.ScaleColor = System.Drawing.Color.Black;
			this.OUT.ScaleDivisions = 10;
			this.OUT.ScaleSubDivisions = 10;
			this.OUT.setValue = 0;
			this.OUT.SnapDivisions = 0;
			this.OUT.TagName = "OUT";
			this.OUT.Value = 0;
			this.OUT.ValueChange = false;
			this.OUT.ViewGlass = false;
			this.OUT.EndInit();
			// 
			// MINMS
			// 
			this.MINMS.BeginInit();
			this.MINMS.AngleIgnore = false;
			this.MINMS.DesignTransformation = new NxtControl.Drawing.Matrix(0.59, 0, 0, 1, 31, 185);
			this.MINMS.IsOnlyInput = true;
			this.MINMS.Name = "MINMS";
			this.MINMS.TagName = "MINMS";
			this.MINMS.Value = 0;
			this.MINMS.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.MINMSValueChanged);
			this.MINMS.EndInit();
			// 
			// MAXMS
			// 
			this.MAXMS.BeginInit();
			this.MAXMS.AngleIgnore = false;
			this.MAXMS.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 31, 201);
			this.MAXMS.IsOnlyInput = true;
			this.MAXMS.Name = "MAXMS";
			this.MAXMS.TagName = "MAXMS";
			this.MAXMS.Value = 0;
			this.MAXMS.ValueChanged += new System.EventHandler<NxtControl.GuiFramework.ValueChangedEventArgs>(this.MAXMSValueChanged);
			this.MAXMS.EndInit();
			// 
			// HMI
			// 
			this.Name = "HMI";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.OUT,
									this.MINMS,
									this.MAXMS});
			this.SymbolSize = new System.Drawing.Size(197, 227);
		}
		private NxtStudio.Symbols.Execute<int> MAXMS;
		private NxtStudio.Symbols.Execute<int> MINMS;
		private NxtStudio.Symbols.RoundKnob<int> OUT;
		#endregion
	}
}
