/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 25/03/2012
 * Time: 10:01 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #INTIME_HMI;

namespace NxtStudio.Symbols.INTIME
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_MINMS(ref System.Int32 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 b = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,0, ref b);
      if (ret) value = (System.Int32) b;
      return ret;
    }
    public bool Get_MAXMS(ref System.Int32 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 b = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,1, ref b);
      if (ret) value = (System.Int32) b;
      return ret;
    }

  }

}

namespace NxtStudio.Symbols.INTIME
{
  partial class HMI
  {

    private event EventHandler<NxtStudio.Symbols.INTIME.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new NxtStudio.Symbols.INTIME.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Int32 OUT)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {OUT});
    }
    public bool FireEvent_CNF(System.Int32 OUT, bool ignore_OUT)
    {
      object[] _values_ = new object[1];
      if (!ignore_OUT) _values_[0] = OUT;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #INTIME_HMI;

#endregion Definitions;
