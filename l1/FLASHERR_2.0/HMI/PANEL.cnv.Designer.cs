/*
 * Created by nxtSTUDIO.
 * User: vvya002
 * Date: 24/03/2012
 * Time: 8:07 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace NxtStudio.Canvases
{
	/// <summary>
	/// Summary description for FLASHERTEST.
	/// </summary>
	partial class PANEL
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ST = new NxtStudio.Symbols.RADIOBOOL.HMI();
			this.DT = new NxtStudio.Symbols.INTIME.HMI();
			this.MODE = new NxtStudio.Symbols.INCHOICE.HMI();
			// 
			// ST
			// 
			this.ST.BeginInit();
			this.ST.AngleIgnore = false;
			this.ST.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 13, 61);
			this.ST.Name = "ST";
			this.ST.SecurityToken = ((uint)(4294967295u));
			this.ST.TagName = "747B91B4E38D4B30";
			this.ST.EndInit();
			// 
			// DT
			// 
			this.DT.BeginInit();
			this.DT.AngleIgnore = false;
			this.DT.DesignTransformation = new NxtControl.Drawing.Matrix(0.96, 0, 0, 1.008, 159, 34);
			this.DT.Name = "DT";
			this.DT.SecurityToken = ((uint)(4294967295u));
			this.DT.TagName = "C9BF7C1A496D1962";
			this.DT.EndInit();
			// 
			// MODE
			// 
			this.MODE.BeginInit();
			this.MODE.AngleIgnore = false;
			this.MODE.DesignTransformation = new NxtControl.Drawing.Matrix(0.75206611570247939, 0, 0, 1, 327, 57);
			this.MODE.Name = "MODE";
			this.MODE.SecurityToken = ((uint)(4294967295u));
			this.MODE.TagName = "91FC17E80871BD76";
			this.MODE.EndInit();
			// 
			// PANEL
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(431)), ((float)(178)));
			this.Name = "FLASHERTEST";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.ST,
									this.DT,
									this.MODE});
			this.Size = new System.Drawing.Size(431, 178);
		}
		private NxtStudio.Symbols.INCHOICE.HMI MODE;
		private NxtStudio.Symbols.INTIME.HMI DT;
		private NxtStudio.Symbols.RADIOBOOL.HMI ST;
		#endregion
	}
}
