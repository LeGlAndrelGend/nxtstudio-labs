<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE SubAppType SYSTEM "../LibraryElement.dtd">
<SubAppType Name="Flasher" Comment="Subapplication " Namespace="Main">
  <Identification Standard="61499-2" />
  <VersionInfo Organization="nxtControl GmbH" Version="0.0" Author="vvya002" Date="13/11/2012" Remarks="template" />
  <SubAppInterfaceList>
    <SubAppEventInputs>
      <SubAppEvent Name="INIT" Comment="Initialization Request" />
      <SubAppEvent Name="REQ" Comment="Normal Execution Request" />
    </SubAppEventInputs>
    <SubAppEventOutputs>
      <SubAppEvent Name="INITO" Comment="Initialization Confirm" />
      <SubAppEvent Name="CNF" Comment="Execution Confirmation" />
    </SubAppEventOutputs>
    <InputVars>
      <VarDeclaration Name="QI" Type="BOOL" Comment="Input event qualifier" />
    </InputVars>
    <OutputVars>
      <VarDeclaration Name="QO" Type="BOOL" Comment="Output event qualifier" />
    </OutputVars>
  </SubAppInterfaceList>
  <SubAppNetwork>
    <FB Name="STARTSTOP" Type="RADIOBOOL" x="560" y="860" Namespace="Main">
      <Parameter Name="QI" Value="TRUE" />
      <Parameter Name="LABEL1" Value="Start" />
      <Parameter Name="LABEL0" Value="Stop" />
      <Parameter Name="INV" Value="FALSE" />
    </FB>
    <FB Name="LEDS" Type="LEDHMI" x="3360" y="980" Namespace="Main">
      <Parameter Name="QI" Value="TRUE" />
    </FB>
    <FB Name="PERIODIC" Type="E_CYCLE" x="1980" y="1160" Namespace="IEC61499.Standard" />
    <FB Name="RS_GATE" Type="E_SWITCH" x="1440" y="980" Namespace="IEC61499.Standard" />
    <FB Name="FLASHGEN" Type="FLASHER4" x="2720" y="1040" Namespace="Main" />
    <FB Name="DT" Type="INTIME" x="880" y="1720" Namespace="Main">
      <Parameter Name="MINMS" Value="200" />
      <Parameter Name="MAXMS" Value="1000" />
      <Parameter Name="QI" Value="TRUE" />
    </FB>
    <FB Name="MODE" Type="INCHOICE" x="2000" y="1720" Namespace="Main">
      <Parameter Name="QI" Value="TRUE" />
      <Parameter Name="CHOICES" Value="ALL" />
    </FB>
    <EventConnections>
      <Connection Source="STARTSTOP.IND" Destination="RS_GATE.EI">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="RS_GATE.EO0" Destination="PERIODIC.STOP" dx1="119.7295">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="FLASHGEN.CNF" Destination="LEDS.REQ" />
      <Connection Source="STARTSTOP.INITO" Destination="FLASHGEN.INIT" dx1="969.6667">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="PERIODIC.EO" Destination="FLASHGEN.REQ" dx1="48.5" />
      <Connection Source="STARTSTOP.INITO" Destination="LEDS.INIT" dx1="2249.667">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="RS_GATE.EO1" Destination="PERIODIC.START" dx1="49.72949" />
      <Connection Source="LEDS.INITO" Destination="DT.INIT" dx1="49.5" dx2="80" dy="602">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="DT.CNF" Destination="RS_GATE.EI" dx1="79.77075" />
      <Connection Source="DT.INITO" Destination="MODE.INIT" />
      <Connection Source="MODE.CNF" Destination="FLASHGEN.REQ" dx1="50" />
    </EventConnections>
    <DataConnections>
      <Connection Source="STARTSTOP.OUT" Destination="RS_GATE.G">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="FLASHGEN.LED3" Destination="LEDS.LED3" />
      <Connection Source="FLASHGEN.LED2" Destination="LEDS.LED2" />
      <Connection Source="FLASHGEN.LED1" Destination="LEDS.LED1" />
      <Connection Source="FLASHGEN.LED0" Destination="LEDS.LED0" />
      <Connection Source="DT.OUT" Destination="PERIODIC.DT" dx1="309.7708">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
      <Connection Source="MODE.I" Destination="FLASHGEN.MODE" dx1="171.833">
        <AvoidsNodes>false</AvoidsNodes>
      </Connection>
    </DataConnections>
    <Input Name="INIT">
      <Position>
        <X>0</X>
        <Y>87.96354</Y>
      </Position>
      <IsType>Event</IsType>
    </Input>
    <Input Name="REQ">
      <Position>
        <X>0</X>
        <Y>132.963547</Y>
      </Position>
      <IsType>Event</IsType>
    </Input>
    <Input Name="QI">
      <Position>
        <X>0</X>
        <Y>172.963547</Y>
      </Position>
      <IsType>Data</IsType>
    </Input>
    <Output Name="INITO">
      <Position>
        <X>1230</X>
        <Y>177.963547</Y>
      </Position>
      <IsType>Event</IsType>
    </Output>
    <Output Name="CNF">
      <Position>
        <X>1235</X>
        <Y>192.963547</Y>
      </Position>
      <IsType>Event</IsType>
    </Output>
    <Output Name="QO">
      <Position>
        <X>1205</X>
        <Y>207.963547</Y>
      </Position>
      <IsType>Data</IsType>
    </Output>
  </SubAppNetwork>
</SubAppType>